<?php ($model->page_title) ? $this->setPageTitle($model->page_title) : $this->setPageTitle($model->name.' | '.Yii::app()->name);?>
<?php $this->setKeywords($model->meta_keywords); ?>
<?php $this->setDescription($model->meta_description); ?>

<div class="box-single-service">
    <div class="content"><?php echo $model->content; ?></div>
</div>