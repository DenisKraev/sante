<div class="container inner-content">

    <div class="box-videos">

        <div class="title-page title-photo-video">
            <a href="/photoalbum/" >Фотоальбомы</a>
            <span class="separator">/</span>
            <span class="active">Видеоальбом</span>
        </div>

        <div class="box-video-list">
            <?php foreach($model as $modelItem){
                $this->renderPartial('_view', array('model' => $modelItem));
            } ?>
        </div>

        <?php  $this->widget('LinkPagerWidget', array(
            'pages' => $pages,
        )); ?>

    </div>

</div>