<div class="review-item">
    <div class="name"><?php echo CHtml::encode($data->name); ?><span class="date"><?php echo Yii::app()->dateFormatter->format('dd.MM.yyyy', $data->create_date); ?></span></div>
    <div class="text text-main"><?php echo nl2br(CHtml::encode($data->review)); ?></div>
</div>