<div class="page-review">
    <?php
    if (Yii::app()->user->hasFlash('reviewAdd')) {
      $this->widget('AlertWidget', array(
        'title' => 'Отзывы',
        'message' => Yii::app()->user->getFlash('reviewAdd'),
      ));
    }
    ?>
    <h2 class="title-main">Отзывы</h2>
    <div class="a-main btn-write-review ib">Написать отзыв</div>
    <div class="box-review-form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'htmlOptions' => array(
                'class' => 'ask-form',
                //'autocomplete' => 'off',
            ),
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => false,
            ),
            //'focus' => array($model, 'name')
          ));
        ?>
        <div class="form-row">
            <?php echo $form->textField($model, 'name', array('class' => 'style-form-text', 'placeholder' => $model->getAttributeLabel('name'), 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
<!--        <div class="form-row">-->
<!--            --><?php //echo $form->textField($model, 'contact', array('class' => 'style-form-text', 'placeholder' => $model->getAttributeLabel('contact'), 'size'=>60,'maxlength'=>255)); ?>
<!--            --><?php //echo $form->error($model, 'contact'); ?>
<!--        </div>-->
        <div class="form-row">
            <?php echo $form->textArea($model, 'review', array('class' => 'style-form-textarea', 'placeholder' => $model->getAttributeLabel('review'), 'rows'=>6, 'cols'=>50)); ?>
            <?php echo $form->error($model, 'review'); ?>
        </div>
        <?php if(extension_loaded('gd')): ?>
            <div class="form-row">
                <?php $this->widget('CCaptcha', array('clickableImage'=>true, 'showRefreshButton'=>false)); ?>
                <div class="captcha ib">
                    <?php echo CHtml::activeTextField($model, 'verifyCode', array('autocomplete' => 'off', 'class' => 'style-form-text', 'placeholder' => 'Код проверки')); ?>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-actions">
            <?php echo CHtml::submitButton('Отправить', array('class' => 'btn-main')); ?>
        </div>
        <?php $this->endWidget('CActiveForm'); ?>

    </div>

    <div class="list-review">
        <?php $this->widget('zii.widgets.CListView', array(
            'itemView' => 'webroot.themes.application.views.review._item',
            'dataProvider' => $dataProvider,
            'emptyText' => '',
            'pager' => array('class' => 'LinkPagerWidget'),
            'pagerCssClass' => 'dummyClass', //Если класс оставить пустым то происходит ошибка в плагине yiiListView
            'summaryText' => '',
        )); ?>
    </div>
</div>