<div class="box-single-item">
    <div class="cf">
        <?php if ($model->getImagePreview('_list')) { ?>
            <a href="<?php echo $model->getUrl(); ?>" class="box-img">
                <img src="<?php echo $model->getImagePreview('_list')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($model->title); ?>" />
            </a>
        <?php } ?>
        <div class="data">
            <h3 class="name"><a href="<?php echo $model->getUrl(); ?>" title="<?php echo $model->title; ?>"><?php echo $model->title; ?></a></h3>
            <div class="description text-main"><?php echo $model->short; ?></div>
        </div>
    </div>
    <div class="more">
        <a class="btn-main" href="<?php echo $model->getUrl(); ?>">Подробнее</a>
    </div>
</div>