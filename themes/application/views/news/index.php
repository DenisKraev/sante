<h2 class="title-main">Новости</h2>
<div class="box-list-items">
    <?php foreach ($news as $model): ?>
        <?php $this->renderPartial('/_list_item', array('model' => $model)); ?>
    <?php endforeach; ?>
</div>

<?php  $this->widget('LinkPagerWidget', array(
  'pages' => $pages,
)); ?>