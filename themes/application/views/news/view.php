<?php ($model->page_title) ? $this->setPageTitle($model->page_title) : $this->setPageTitle($model->title.' | '.Yii::app()->name);?>
<?php $this->setKeywords($model->meta_keywords); ?>
<?php $this->setDescription($model->meta_description); ?>

<div class="box-news-single">
    <h1 class="title-main"><?php echo $model->title; ?></h1>
    <div class="text"><?php echo $model->content; ?></div>
    <div class="archive"><?php echo CHtml::link('«&nbsp; К списку новостей', array('index'), array('class' => 'btn-main'))?></div>
</div>