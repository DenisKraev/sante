<?php $this->widget('application.widgets.SliderWidget'); ?>
<?php $this->widget('application.widgets.ServicesWidget'); ?>
<div class="box-about cf">
    <div class="box-text">
        <?php echo Yii::app()->Block->getBlockContent('about_text'); ?>
    </div>
    <div class="box-img" style="background: url('/themes/application/images/img-7.jpg') 0px 0px no-repeat; background-size: cover; -o-background-size: cover; -webkit-background-size: cover"></div>
</div>
<?php $this->widget('application.widgets.SpecialistWidget'); ?>