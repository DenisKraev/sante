<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-language" content="ru" >

    <?php if (YII_DEBUG) {
        Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
    } ?>

    <link rel="apple-touch-icon" href="/content/touch-icon-ipad-retina.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/content/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/content/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/content/touch-icon-ipad-retina.png" />
    <meta property="og:image" content="/content/touch-icon-ipad-retina.png">
    <link rel="image_src" href="/content/touch-icon-ipad-retina.png">

    <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
        $libDir = "/themes/application/js/lib/";
        Yii::app()->clientScript->packages['jsLib'] = array('baseUrl'=>$libDir,'js'=>AppHelper::getFilesDir($libDir));
        Yii::app()->clientScript->registerPackage('jsLib');
        Yii::app()->clientScript->registerScriptFile('/themes/application/js/app.js', CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerCssFile('/themes/application/css/application.css');
    ?>
    <?php if (Yii::app()->controller->id == 'contact') { ?>
        <script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <?php } ?>
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body class="<?php echo 'controller_'.Yii::app()->controller->id.' '.'action_'.Yii::app()->controller->action->id; ?>">

    <div id="wrapper" class="site-content">
        <?php $this->renderPartial('partials.LeftColumn'); ?>

        <div class="body-content">
            <?php if(Yii::app()->controller->id != 'home') { ?><div class="inner-content"><?php } ?>
                <?php echo $content; ?>
            <?php if(Yii::app()->controller->id != 'home') { ?></div><?php } ?>
        </div>
    </div>
    <?php $this->widget('application.widgets.FormCallbackWidget'); ?>
    <?php //$this->renderPartial('partials.BottomScripts'); ?>

</body>
</html>