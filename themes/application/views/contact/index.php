<?php
Yii::app()->clientScript->registerScript(
    'yamap',
    "var myMap;

    ymaps.ready(init);

    function init () {

        myMap = new ymaps.Map('box-map', {
            center: [58.60786183, 49.66270986],
            zoom: 16

        }),
            myPlacemark = new ymaps.Placemark(myMap.getCenter(),
                {
                    hintContent: 'Медицинский центр Сантэ',
                    iconContent: 'Медицинский центр Сантэ'
                },
                {preset: 'islands#darkBlueStretchyIcon'});
        myMap.behaviors.disable('scrollZoom');
        myMap.geoObjects.add(myPlacemark);

        document.getElementById('destroyButton').onclick = function () {
            myMap.destroy();
        };

    }",
    CClientScript::POS_READY
);
?>

<div id="box-map" class="box-map"></div>
<div class="page-contact-inner">
    <div class="box-contact">
        <?php echo $model[0]->content; ?>
    </div>

    <div class="box-form-contact">
        <h3 class="title-main">Обратная свзяь</h3>
        <?php $this->widget('application.widgets.FormFeedbackWidget'); ?>
    </div>
</div>