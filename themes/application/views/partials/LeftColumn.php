<div class="left-column">
    <a class="logo" href="/"><img src="/themes/application/images/app/logo.png"></a>
    <div class="box-flash-contacts">
        <div class="phone">+7 (8332) 35-46-44</div>
        <a href="#box-form-callback" class="btn-callback ib js-main-popup"><span>Обратный звонок</span></a>
    </div>
    <div class="main-menu">
        <?php $this->widget('application.modules.menu.widgets.MenuWidget', array(
            'rootItem' => Yii::app()->menu->all,
            'typeMenu' => 'type_main_menu',
            'addItems' => array(
                array(
                    'sequence' => 1,
                    'menu' => Yii::app()->AddMenuItems->getAddMenuServices($visibleChildLevels = true)
                )
            ),
            'htmlOptions' => array('class' => 'main-menu-ul'),
            'activeCssClass' => 'active',
            'activateParents' => 'true',
            'maxChildLevel' => 2,
            'encodeLabel' => false,
        )); ?>
    </div>
    <div class="footer">
        <div class="copy">
            <?php echo AppHelper::copyDate('2014'); ?> &copy; <br>Медицинский центр &laquo;Сантэ&raquo;
        </div>
        <div class="dev">
            <span>Сделано в</span>
            <img src="/themes/application/images/app/artnet-logo.png" alt="Разработка и дизайн сайтов Artnet Studio" title="Разработка и дизайн сайтов Artnet Studio">
            <a href="http://artnetdesign.ru/" target="_blank">Artnet Studio</a>
        </div>
    </div>
</div>