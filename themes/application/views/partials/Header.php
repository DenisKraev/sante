<?php if($this->beginCache('Header', array('duration'=>3600))) { ?>

<div id="header" class="site-content">
    <div class="main-menu">
        <?php $this->widget('application.modules.menu.widgets.MenuWidget', array(
            'rootItem' => Yii::app()->menu->all,
            'typeMenu' => 'type_main_menu',
            'htmlOptions' => array('class' => 'main-menu-ul'),
            'activeCssClass' => 'active',
            'activateParents' => 'true',
            'maxChildLevel' => 0,
            'encodeLabel' => false,
        )); ?>
    </div>
</div>

<?php $this->endCache(); } ?>