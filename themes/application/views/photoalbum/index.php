<div class="container inner-content">

    <div class="box-albums">

        <div class="title-page title-photo-video">
            <span class="active">Фотоальбомы</span>
            <span class="separator">/</span>
            <a href="/videoalbum/">Видеоальбом</a>
        </div>

        <div class="box-album-list">
            <?php foreach($model as $modelItem){
                $this->renderPartial('_view', array('model' => $modelItem, 'limitPhoto' => 8));
            } ?>
        </div>

        <?php  $this->widget('LinkPagerWidget', array(
            'pages' => $pages,
        )); ?>

    </div>

</div>