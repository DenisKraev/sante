<?php if ($model->photosList) { ?>
    <div class="box-album">

        <h4 class="title-album">
            <a href="<?php echo $model->alias ?>"><?php echo CHtml::encode($model->name); ?></a>
        </h4>
        <div class="box-album-images cf">
            <?php foreach ($model->photosList(array('limit' => $limitPhoto)) as $i=>$photo) { ?>

                <?php
                    $smallImage = $photo->image->getPreview($w = 220, $h = 150, $postfix = '_gal', $cropType = 'center', $quality = 90, $resize = false);
                    $full = $photo->image->getPreview($w = 1200, '', $postfix = '_full', $cropType = null, $quality = 90, $resize = false);
                ?>
                <div class="box-img">
                    <a class="js-popup-img" href="<?php echo $full->getUrlPath() ?>">
                        <img src="<?php echo $smallImage->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($photo->getName()); ?>">
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php if ($model->countPhoto > $limitPhoto) { ?>
            <div class="box-all-images-btn">
                <a href="<?php echo $model->alias ?>"><?php echo Yii::t('main-ui', 'Все картинки'); ?></a>
            </div>
        <?php } ?>

    </div>
<?php } ?>