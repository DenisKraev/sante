<?php $this->pageTitle = Yii::t('main-ui', 'Фотоальбом').' - '.$model->name.' | '.Yii::app()->name;  ?>

<div class="box-single-album">

    <h1 class="title-page"><?php echo Yii::t('main-ui', 'Альбом') ?>: <?php echo $model->name; ?></h1>

    <div class="box-album-images cf">
        <?php foreach ($model->photosList as $i=>$photo) { ?>

            <?php
                $smallImage = $photo->image->getPreview($w = 220, $h = 150, $postfix = '_gal', $cropType = 'center', $quality = 90, $resize = false);
                $full = $photo->image->getPreview($w = 1200, '', $postfix = '_full', $cropType = null, $quality = 90, $resize = false);
            ?>

            <div class="box-img">
                <a class="js-popup-img" href="<?php echo $full->getUrlPath() ?>"><img src="<?php echo $smallImage->getUrlPath(); ?>" alt="<?php echo AppHelper::cutQuotes($photo->getName()); ?>"></a>
            </div>

        <?php } ?>
    </div>

    <div class="archive"><?php echo CHtml::link('«&nbsp;'.Yii::t('main-ui', 'К списку альбомов'), array('index'), array('class' => 'brown-btn'))?></div>

</div>