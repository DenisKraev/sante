<?php ($model->page_title) ? $this->setPageTitle($model->page_title) : $this->setPageTitle($model->job_title.'. '.$model->name.' | '.Yii::app()->name);?>
<?php $this->setKeywords($model->meta_keywords); ?>
<?php $this->setDescription($model->meta_description); ?>

<h3 class="title-main">Специалисты</h3>

<div class="specialist-single cf">
    <div class="box-img">
        <?php if($model->getImagePreview('_avatar_gs')) { ?>
            <img src="<?php echo $model->getImagePreview('_avatar_gs')->getUrlPath(); ?>" alt="<?php echo AppHelper::cutQuotes($model->name); ?>">
        <?php } else { ?>
            <img src="/content/no-image.jpg">
        <?php } ?>
    </div>
    <div class="box-info">
        <div class="name"><?php echo $model->name; ?></div>
        <div class="job_title"><?php echo $model->job_title; ?></div>
        <?php if($model->description) { ?>
            <div class="description text-main"><?php echo $model->description; ?></div>
        <?php } ?>
        <?php if($model->experience) { ?>
            <div class="experience">Опыт работы: <?php echo $model->experience; ?></div>
        <?php } ?>
    </div>
</div>
<?php $this->widget('application.widgets.FormQuestionWidget', array('selectedId' => $model->id_app_specialist, 'nameSpecialist' => $model->name)); ?>
<?php $this->widget('application.widgets.AnswerWidget'); ?>