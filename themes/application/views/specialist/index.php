<div class="page-specialist">

    <h3 class="title-main">Специалисты</h3>

    <?php if($model) { ?>
        <div class="specialist-list">
            <?php foreach($model as $modelItem){ ?>
                <div class="specialist-item cf">
                    <a class="box-img" href="/specialist/<?php echo $modelItem->id_app_specialist; ?>">
                        <?php if($modelItem->getImagePreview('_avatar_gs')) { ?>
                            <img src="<?php echo $modelItem->getImagePreview('_avatar_gs')->getUrlPath(); ?>" alt="<?php echo AppHelper::cutQuotes($modelItem->name); ?>">
                        <?php } else { ?>
                            <img src="/content/no-image.jpg">
                        <?php } ?>
                    </a>
                    <div class="box-info">
                        <a href="/specialist/<?php echo $modelItem->id_app_specialist; ?>" class="name ib"><?php echo $modelItem->name; ?></a>
                        <div class="job_title"><?php echo $modelItem->job_title; ?></div>
                        <a class="btn-main" href="/specialist/<?php echo $modelItem->id_app_specialist; ?>">Задать вопрос</a>
                        <?php if($modelItem->description) { ?>
                            <div class="description text-main"><?php echo $modelItem->description; ?></div>
                        <?php } ?>
                        <?php if($modelItem->experience) { ?>
                            <div class="experience">Опыт работы: <?php echo $modelItem->experience; ?></div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { echo 'Пустой раздел';} ?>

</div>