$(document).ready(function(){

    $('input, textarea').placeholder();

    //$('.style-scrollbar').scrollbar();

    $(".js-main-popup").fancybox({
        padding: '0',
        width : 'auto',
        height : 'auto',
        maxWidth: "90%",
        fitToView: false,
        scrolling: 'no',
        helpers: {  overlay: { locked: false } }
    });
    $('.popup-cancel').click(function(){
        $.fancybox.close();
    });

    $('.slider').lightSlider({
        item: 1,
        //auto: true,
        //speed: 1500,
        pause: 10000,
        adaptiveHeight: true,
        slideMargin: 0,
        autoWidth: false,
        galleryMargin: 0,
        thumbMargin: 0,
        mode: 'fade',
        controls: false,
        enableTouch: false,
        enableDrag: false,
        freeMove: false,
        loop: true,
        onSliderLoad: function() {
            $('.slider').removeClass('gallery-hidden');
        }
    });

    // прижимаем копирайты в левой колонке к низу экрана
    var responsiveLeftColumn = function(){
        if($(".left-column").outerHeight() < $(document).outerHeight()) {
            $(".left-column").height(0).css({minHeight: '100%', height: $(document).outerHeight()+'px'});
        }
        //$(".left-column .footer").css({bottom: 0+'px', visibility: 'visible'});
    }

    responsiveLeftColumn();
    $(window).resize(function(){
        $(".left-column").css({height: 'auto'});
        responsiveLeftColumn();
    });

    $('.widget-specialist .specialist-item').hover(function(){
       $(this).find('.overlay').fadeToggle();
    });

    $('.btn-write-review').click(function(){
       $(this).toggleClass('active');
       $('.box-review-form').slideToggle();
    });

    var boxen = [];
    $("a[class^=js-popup-img], a[class^=js-popup-img-overlay]").each(function() {
        if ($.inArray($(this).attr('class'),boxen)) boxen.push($(this).attr('class'));
    });
    $(boxen).each(function(i,val) {
        $('a[class='+val+']').attr('rel',val).fancybox({
            fitToView		: false,
            padding		: '0',
            helpers : {overlay: {locked: false}}
        });
    });

    $('.js-popup-img, .js-popup-img-overlay').fancybox({
        openEffect : 'fade',
        openSpeed  : 150,
        closeEffect : 'fade',
        closeSpeed  : 150,
        closeBtn  : true,
        arrows    : true,
        nextClick : true,
        helpers : {
            buttons	: {},
            thumbs : {
                width  : 90,
                height : 50
            },
            beforeShow : function() {
                var s='';
                var alt = this.element.find('img').attr('alt');
                var title = this.element.find('img').attr('title');
                var ze = (title == undefined || title=='') ? true : false;
                var te = (alt == undefined || alt=='') ? true : false;
                if (ze) {title = this.title;}
                if (alt == title) {title='';}
                var ze= (title == undefined || title=='') ? true : false;
                if (!ze && !te) s=alt + '<br>' + title;
                if (ze && !te) s=alt;
                if (!ze && te) s=title;
                this.title = s;
            },
            overlay: {
                locked: false
            }
        },
        afterLoad : function(){if (this.group.length>1){this.title = 'Изображение ' + (this.index + 1) + ' из ' + this.group.length+(this.title ? ' - ' + this.title : '');}
        else{this.helpers.buttons = false;return;}}
    });

    $(".js-popup-video").fancybox({
        type: 'iframe'
    });

});