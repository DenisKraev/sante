<?php
class ImagesEquipmentBehavior extends CActiveRecordBehavior {

  public $idObject;

  public function attach($owner) {
    parent::attach($owner);
    if (empty($this->idObject)) {
      if ($owner instanceOf DaActiveRecord) {
        $this->idObject = $owner->getIdObject();
      }
    }
    //exit(print_r($idObject));
    $this->getOwner()->getMetaData()->addRelation(
      'photosList',
      array(CActiveRecord::HAS_MANY, 'ImagesEquipment', 'id_images_equipment_instance',
        'select' => 'id_images_equipment, name',
        'with' => 'imageFile',
        'on' => 'photosList.id_images_equipment_object = :PHOTOGALLERY_OBJECT_ID',
        'joinType' => 'JOIN',
        'params' => array(':PHOTOGALLERY_OBJECT_ID' => 'project-oborudovanie'),
    ));

  }

}