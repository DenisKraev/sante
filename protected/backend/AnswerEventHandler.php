<?php
class AnswerEventHandler extends BackendEventHandler {
    public function onPostForm(PostFormEvent $event) {
        $model = $event->model;
        $send = $model->send;
        $email = $model->email;
        $answer = $model->answer;
        $idEventTypeAnswer = 54;
        $idEventSubscriberAnswer = 3;

        if ($send == 1 && $model->validate() && $answer != null) {
            Yii::app()->notifier->addNewEvent(
                $idEventTypeAnswer,
                $event->sender->renderPartial('webroot.themes.application.views.questionanswer.answer_notification_email_body', array('data' => $model), true),
                $idEventSubscriberAnswer,
                array($email)
            )->sendNowLastAdded();
            $model->send = 0;
        }
    }
}