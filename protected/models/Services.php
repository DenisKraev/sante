<?php

/**
 * Модель для таблицы "app_services".
 *
 * The followings are the available columns in table 'app_services':
 * @property integer $id_app_services
 * @property string $name
 * @property string $alias
 * @property integer $style
 * @property string $content
 * @property integer $on_main
 * @property integer $visible
 * @property integer $sequence
 * @property integer $image
 *
 * The followings are the available model relations:
 * @property File $imageFile
 */
class Services extends DaActiveRecord {

  const ID_OBJECT = 'project-uslugi';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Services the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_services';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, alias, style, content, image', 'required'),
      array('style, on_main, visible, sequence, image', 'numerical', 'integerOnly'=>true),
      array('name, alias, page_title', 'length', 'max'=>255),
      array('meta_keywords, meta_description', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'imageFile' => array(self::BELONGS_TO, 'File', 'image'),
    );
  }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'imageFile',
                'formats' => array(
                    '_widget' => array(
                        'width' => '480',
                        'height' => '480',
                        'crop' => 'center',
                        'grayscale' => true
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_services' => 'ID',
      'name' => 'Название',
      'alias' => 'В адресной строке',
      'style' => 'Внешний вид блока услуги',
      'content' => 'Контент',
      'on_main' => 'Отображать на главной странице',
      'visible' => 'Видимость',
      'sequence' => 'п/п',
      'image' => 'Картинка на фон блока',
    );
  }

    protected function beforeSave() {
        $text = trim($this->alias);
        $this->alias = HText::translit($text, '_', false);
        return parent::beforeSave();
    }

}