<?php

/**
 * Модель для таблицы "app_main_slider".
 *
 * The followings are the available columns in table 'app_main_slider':
 * @property integer $id_app_main_slider
 * @property string $title
 * @property string $subtitle
 * @property integer $image
 * @property string $text
 * @property string $url
 * @property integer $sequence
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property File $imageFile
 */
class MainSlider extends DaActiveRecord {

  const ID_OBJECT = 'project-slaider';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return MainSlider the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_main_slider';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('title, image', 'required'),
      array('image, sequence, visible', 'numerical', 'integerOnly'=>true),
      array('title, subtitle, url', 'length', 'max'=>255),
      array('text', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'imageFile' => array(self::BELONGS_TO, 'File', 'image'),
    );
  }

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'imageFile',
                'formats' => array(
                    '_slider' => array(
                        'width' => '1700',
                        'height' => '700',
                        'crop' => 'top',
                        'grayscale' => true
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_main_slider' => 'ID',
      'title' => 'Заголовок',
      'subtitle' => 'Подзаголовок',
      'image' => 'Картинка',
      'text' => 'Текст (описание)',
      'url' => 'Ссылка',
      'sequence' => 'п/п',
      'visible' => 'Видимость',
    );
  }

}