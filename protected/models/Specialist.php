<?php

/**
 * Модель для таблицы "app_specialist".
 *
 * The followings are the available columns in table 'app_specialist':
 * @property integer $id_app_specialist
 * @property string $name
 * @property string $job_title
 * @property string $experience
 * @property integer $visible
 * @property integer $avatar
 * @property string $description
 * @property integer $sequence
 * @property string $page_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * The followings are the available model relations:
 * @property File $avatarFile
 */
class Specialist extends DaActiveRecord {

  const ID_OBJECT = 'project-spezialisty';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Specialist the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_specialist';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, job_title', 'required'),
      array('visible, avatar, sequence', 'numerical', 'integerOnly'=>true),
      array('name, job_title, experience, page_title', 'length', 'max'=>255),
      array('description, meta_keywords, meta_description', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'avatarFile' => array(self::BELONGS_TO, 'File', 'avatar'),
    );
  }
    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'avatarFile',
                'formats' => array(
                    '_avatar' => array(
                        'width' => '270',
                        'height' => '360',
                        'crop' => 'top',
                        //'grayscale' => true
                    ),
                    '_avatar_gs' => array(
                        'width' => '270',
                        'height' => '360',
                        'crop' => 'top',
                        'grayscale' => true
                    ),
                    '_avatar_widget' => array(
                        'width' => '370',
                        'height' => '480',
                        'crop' => 'top',
                        'grayscale' => true
                    ),
                ),
            ),
        );
        return $behaviors;
    }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_specialist' => 'ID',
      'name' => 'Имя (ФИО)',
      'job_title' => 'Должность',
      'experience' => 'Опыт работы',
      'visible' => 'Видимость',
      'avatar' => 'Аватар',
      'description' => 'Описание',
      'sequence' => 'п/п',
      'page_title' => 'Заголовок <title>',
      'meta_keywords' => 'Ключевые слова <meta name="keywords">',
      'meta_description' => 'Описание <meta name="description">',
    );
  }

}