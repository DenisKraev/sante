<?php

/**
 * Модель для таблицы "app_block_content".
 *
 * The followings are the available columns in table 'app_block_content':
 * @property integer $id_app_block_content
 * @property string $name
 * @property string $explanation
 * @property integer $state
 * @property string $text_ru
 * @property string $text_en
 * @property string $html_ru
 * @property string $html_en
 */
class BlockContent extends DaActiveRecord {

  const ID_OBJECT = 'project-bloki-kontenta';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return BlockContent the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_block_content';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name', 'required'),
      array('state', 'numerical', 'integerOnly'=>true),
      array('name, explanation', 'length', 'max'=>255),
      array('text_ru, text_en, html_ru, html_en', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_block_content' => 'ID',
      'name' => 'Название',
      'explanation' => 'Объяснение',
      'state' => 'Видимость',
      'text_ru' => 'Обычный текст по русски',
      'text_en' => 'Обычный текст по английски',
      'html_ru' => 'html контент по русски',
      'html_en' => 'html контент по английски',
    );
  }

}