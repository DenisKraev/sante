<?php

/**
 * Модель для таблицы "app_form_callback".
 *
 * The followings are the available columns in table 'app_form_callback':
 * @property integer $id_app_form_callback
 * @property string $name
 * @property string $phone
 * @property string $datetime
 */
class FormCallback extends DaActiveRecord {

  const ID_OBJECT = 'project-forma-zakaza-zvonka';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return FormCallback the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_form_callback';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, phone', 'required'),
      array('name, phone', 'length', 'max'=>50),
      array('datetime', 'length', 'max'=>10),
      array('name, phone', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_form_callback' => 'ID',
      'name' => 'Ваше имя',
      'phone' => 'Ваш телефон',
      'datetime' => 'Дата отправки',
    );
  }

}