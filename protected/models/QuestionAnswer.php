<?php

/**
 * Модель для таблицы "app_question_answer".
 *
 * The followings are the available columns in table 'app_question_answer':
 * @property integer $id_app_question_answer
 * @property integer $id_app_specialist
 * @property string $immya
 * @property string $email
 * @property string $theme
 * @property string $question
 * @property string $answer
 * @property string $datetime
 * @property integer $send
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property AppSpecialist $appSpecialist
 */
class QuestionAnswer extends DaActiveRecord {

  const ID_OBJECT = 'project-vopros-otvet';

  protected $idObject = self::ID_OBJECT;

  public $name;
  public $name_specialist;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return QuestionAnswer the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function init() {
    parent::init();
    $this->datetime = time();
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_question_answer';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_app_specialist, immya, email, theme, question', 'required'),
      array('id_app_specialist, send, visible', 'numerical', 'integerOnly'=>true),
      array('immya, email, theme, name, name_specialist', 'length', 'max'=>255),
      array('question', 'length', 'max'=>2000),
      array('datetime', 'length', 'max'=>10),
      array('answer', 'safe'),
      array('email', 'email'),
      array('name', 'simulationCaptcha'),
      array('immya, email, question, theme', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'Specialist' => array(self::BELONGS_TO, 'Specialist', 'id_app_specialist'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_question_answer' => 'ID',
      'id_app_specialist' => 'Специалист',
      'immya' => 'Отправитель',
      'email' => 'Email',
      'theme' => 'Тема вопроса',
      'question' => 'Вопрос',
      'answer' => 'Ответ',
      'datetime' => 'Дата отправки',
      'send' => 'Отправить на email спрашивающего',
      'visible' => 'Видимость',
    );
  }

  public function simulationCaptcha($attribute) {
    if ($this->name != '')
        $this->addError($attribute, 'нет');
  }

  public function getBackendEventHandler() {
    return array(
        'class' => 'application.backend.AnswerEventHandler'
    );
  }

  protected function beforeValidate() {
      if ($this->send == 1 && $this->answer == null) {
          $this->addError('answer', 'Чтобы отправить ответ заполните его.');
          return false;
      }
      return parent::beforeSave();
  }

}