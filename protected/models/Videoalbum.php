<?php

/**
 * Модель для таблицы "app_videoalbum".
 *
 * The followings are the available columns in table 'app_videoalbum':
 * @property integer $id_app_videoalbum
 * @property string $url
 * @property string $name_ru
 * @property string $name_en
 * @property integer $visible
 * @property integer $sequence
 */
class Videoalbum extends DaActiveRecord {

  const ID_OBJECT = 'project-vidoalbom';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Videoalbum the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_videoalbum';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('url, name_ru', 'required'),
      array('visible, sequence', 'numerical', 'integerOnly'=>true),
      array('url, name_ru, name_en', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_videoalbum' => 'ID',
      'url' => 'Ссылка на видео',
      'name_ru' => 'Название',
      'name_en' => 'Название (по английски)',
      'visible' => 'Видимость',
      'sequence' => 'п/п',
    );
  }

    public function getYoutubeParams($video) {
        $temp = explode('src="', $video);
        $temp = $temp[1];
        $temp = explode('"', $temp);
        $src = $temp[0];
        $arSrc = parse_url($src);
        $parsedSrc = $arSrc['scheme'] . $arSrc['host'] . $arSrc['path'];
        $temp = explode('/', $parsedSrc);
        $code = end($temp);
        return array(
            'src'  => $src,
            'code' => $code
        );
    }
    public function getParseYoutubeUrl($url) {
        $parsed_url = parse_url($url);
        parse_str($parsed_url['query'], $parsed_query);
        $video = '<iframe src="http://www.youtube.com/embed/' . $parsed_query['v'] . '" type="text/html" width="400" height="300" frameborder="0"></iframe>';
        $arVid = $this->getYoutubeParams($video);
        return $arVid;
    }


}