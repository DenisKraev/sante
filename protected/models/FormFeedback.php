<?php

/**
 * Модель для таблицы "app_form_feedback".
 *
 * The followings are the available columns in table 'app_form_feedback':
 * @property integer $id_app_form_feedback
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $text
 * @property string $datetime
 */
class FormFeedback extends DaActiveRecord {

  const ID_OBJECT = 'project-forma-obratnoi-svyazi';

  protected $idObject = self::ID_OBJECT;

  // Проверочный код
  public $verifyCode;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return FormFeedback the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

    public function init() {
        parent::init();
        $this->datetime = time();
    }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'app_form_feedback';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, email, text', 'required'),
      array('name, phone, email', 'length', 'max'=>255),
      array('datetime', 'length', 'max'=>10),
      array('text', 'length', 'max'=>1000),
      array('verifyCode', 'required'),
      array('verifyCode', 'captcha'),
      array('name, email, text, phone', 'filter', 'filter' => array('CHtml', 'encode')),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_app_form_feedback' => 'ID',
      'name' => 'Ваше имя',
      'phone' => 'Ваш телефон',
      'email' => 'Ваш e-mail',
      'text' => 'Текст сообщения',
      'datetime' => 'Дата отправки',
      'verifyCode' => 'Код проверки',
    );
  }

}