<?php

class QuestionanswerController extends Controller
{
    const EVENT_TYPE_NEW_FORM_QUESTION = 53;

    protected $urlAlias = "questionanswer";

    // форма связи
    public function actionSendQuestion() {

        $model = BaseActiveRecord::newModel('QuestionAnswer');
        $modelClass = get_class($model);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-question-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST[$modelClass])) {
            $model->attributes=$_POST[$modelClass];
            $model->onAfterSave = array($this, 'sendQuestion');
            if ($model->save()) {
                Yii::app()->user->setFlash('form-question-success', 'Спасибо за обращение. Ваш вопрос успешно отправлен. Ожидайте ответ на указанный email.');
            } else {
                Yii::app()->user->setFlash('form-question-success', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);

    }

    // отправка почты с формы задать вопрос
    public function sendQuestion(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_QUESTION,
            $this->renderPartial('question_email_body', array('data' => $event->sender), true)
        )->sendNowLastAdded();
    }

    // отправка почты с попапа заказа звонка
    public function sendMessageCallback(CEvent $event) {
        Yii::app()->notifier->addNewEvent(
            self::EVENT_TYPE_NEW_FORM_CALLBACK,
            $this->renderPartial('callback_email_body', array('data' => $event->sender), true)
//            4, // создать пустого подписчика для отправки на указанные вручную адреса
//            array('agr@sfdg.er')
        )->sendNowLastAdded();
    }
}