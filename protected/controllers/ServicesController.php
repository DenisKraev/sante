<?php

class ServicesController extends Controller
{

    public $urlAlias  = 'services';

	public function actionIndex()
	{
        $criteria=new CDbCriteria;
        $criteria->addInCondition('alias', array($_GET['alias']));
        $criteria->addCondition('visible = 1');

        $model = Services::model()->findAll($criteria);

        if($model == null)
            throw new CHttpException(404, 'Запрашиваемая страница не найдена.');

        $this->render('show',array(
            'model'=>$model,
        ));
    }

}