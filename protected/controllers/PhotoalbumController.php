<?php

class PhotoalbumController extends Controller
{

    public $urlAlias  = 'photoalbum';

	public function actionIndex()
	{
        $criteria = new CDbCriteria();

        $criteria->condition = 'visible=:visible';
        $criteria->params = array(':visible' => 1);
        $criteria->order = 'sequence ASC';

        $count=Photoalbum::model()->count($criteria);

        $pages=new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $model=Photoalbum::model()->with('countPhoto')->findAll($criteria);

        $this->render('index',array(
            'model'=>$model,
            'pages' => $pages
        ));
	}

    public function actionShow()
    {
        $criteria=new CDbCriteria;
        $criteria->condition = 'alias=:alias';
        $criteria->params = array(':alias' => $_GET['id']);

        $model=Photoalbum::model()->findAll($criteria);

        $this->render('show',array(
            'model'=>$model,
        ));
    }

}