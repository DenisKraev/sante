<?php

class SpecialistController extends Controller
{

    public $urlAlias  = 'specialist';

	public function actionIndex()
	{
        $criteria = new CDbCriteria();

        $criteria->addCondition('visible = 1');
        $criteria->order = 'sequence ASC';

        $model = Specialist::model()->findAll($criteria);

        $this->render('index',array(
            'model'=>$model
        ));
	}

    public function actionShow()
    {
        $criteria=new CDbCriteria;
        $criteria->addInCondition('id_app_specialist', array($_GET['id']));
        $criteria->addCondition('visible = 1');

        $model = Specialist::model()->findAll($criteria);

        if($model == null)
            throw new CHttpException(404, 'Запрашиваемая страница не найдена.');

        $this->render('show',array(
            'model'=>$model,
        ));
    }

}