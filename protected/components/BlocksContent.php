<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BlocksContent extends CWidget {

    public function getBlockContent($name) {

        $criteria=new CDbCriteria;
        $criteria->condition = 'name=:name';
        $criteria->params = array(':name'=>$name);

        $model=BlockContent::model()->findAll($criteria);

//        $text = 'text_'.Yii::app()->language;
//        $html = 'html_'.Yii::app()->language;

        $text = 'text';
        $html = 'html';

        if ($model[0]->state == 1) {
            if ($model[0]->$html !== '' && $model[0]->$text == '') {
                $result = $model[0]->$html;
            }
            else if ($model[0]->$text !== '' && $model[0]->$html == '') {
                $result = $model[0]->$text;
            }
            else if ($model[0]->$text !== '' && $model[0]->$html !== '') {
                $result = null;
            }
            return $result;
        }
        else {
            return null;
        }

    }

}