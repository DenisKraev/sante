<?php

class AddMenuItems extends CWidget {

    public function getAddMenuServices($visibleChildLevels = true) {

        $criteria = new CDbCriteria();
        $criteria->addCondition('visible = 1');
        $criteria->order = 'sequence ASC';

        $model = Services::model()->findAll($criteria);

        $arraySubItem = array();
        $arraySubItems = array();
        //exit();
        !empty($_GET['alias']) ? $alias = $_GET['alias'] : $alias = '';
        if(count($model) > 0 && $visibleChildLevels) {
            foreach($model as $k => $item) {
                $arraySubItem['label'] = $item->name;
                $arraySubItem['url'] = '/services/'.$item->alias;
                $arraySubItem['active'] = $item->alias == $alias ? 1 : '';
                $arraySubItems[$k] = $arraySubItem;
            }
            $array = array(
                'active' => Yii::app()->controller->id == 'services' ? 1 : '',
                'label' =>  'Услуги',
                'url' => '#',
                'items' => $arraySubItems,
                'itemOptions' => array('class' => 'services'),
                //'linkOptions' => array('class' => '2')
            );
        }
        else if(count($model) > 0 && !$visibleChildLevels) {
            $array = array(
                'active' => Yii::app()->controller->id == 'services' ? 1 : '',
                'label' => 'Услуги',
                'url' => '/services/',
            );
        }
        else if(count($model) == 0) {
            $array = null;
        }
        return $array;

    }
}