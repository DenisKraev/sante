<?php
class AppHelper {

    public function Truncate($text, $characters, $end = '...'){
        $text=strip_tags($text);
        if(mb_strlen($text) > $characters) {
            $text = mb_substr($text, 0, $characters);
            $text = rtrim($text, ":!,.-");
            $text = mb_substr($text, 0, mb_strrpos($text, ' '));
            return $text.$end;
        }
        else {
            return $text;
        }
    }

    public function cutQuotes($text){
        return str_replace(array("'","\""), "", $text);
    }

    function getFilesDir($dir) {
        $lib = opendir($_SERVER['DOCUMENT_ROOT'].$dir);
        $arrFiles = array();
        while (($file = readdir($lib)) !== false) {
            if($file == '.' or $file == '..') continue;
            $arrFiles[] = $file;
        }
        closedir($lib);
        return $arrFiles;
    }

    public function copyDate($dateStart){
        if($dateStart == date('Y')) {
            $copyDate = $dateStart;
        } else {
            $copyDate = $dateStart.' — '. date('Y');
        }
        return $copyDate;
    }

}