<?php

class AppNews extends News {

    public function behaviors() {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'image',
                'formats' => array(
                    '_list' => array(
                        'width' => '299',
                        'height' => '218',
                        'crop' => 'center',
                    ),
                    '_list_small' => array(
                        'width' => '79',
                        'height' => '58',
                        'crop' => 'center',
                    ),
                ),
            ),
        );
        return $behaviors;
    }

}