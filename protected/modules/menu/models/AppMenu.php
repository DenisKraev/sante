<?php

class AppMenu extends Menu  {

    private function getStaticUrl() {
        $parentMenu = $this->getParent();
        $url = '';
        if ($parentMenu !== null && $parentMenu->isStaticMenu()) {
            $url = $parentMenu->getStaticUrl();
        }

        if (!empty($url)) {
//      $url .= self::SEPARATOR.$this->alias;
            $url = $this->alias; // убрал точки из урла
        } else {
            $url = $this->alias;
        }
        return $url;
    }

    public function getUrl() {

        // ссылка на другую страницу
        if (!empty($this->external_link)) {
            if (!empty(Yii::app()->urlManager->languages)) {
//            count(Yii::app()->urlManager->languages);
                $url = '/'.Yii::app()->language.'/'.$this->external_link;
                $url = str_replace("//", "/", $url);
                return $url;
            }
            else {
                return $this->external_link;
            }
        }

        //Динамический раздел
        if (!empty($this->uri)) {
            return $this->uri;
        }

        if ($this->go_to_type == Menu::GO_TO_FILE) {
            $file = $this->firstFile;
            if ($file != null) {
                $res = $file->getUrlPath();
                return $res;
            }
        }

        //Статический раздел
        $url = $this->getStaticUrl();
        if ($url == ''){$url = '/';}
        if ($url == '/') {
            if (!empty(Yii::app()->urlManager->languages)) {
//            count(Yii::app()->urlManager->languages);
                return $url = '/'.Yii::app()->language.'/';
            }
            else {
                return $url;
            }
        }
        $result = Yii::app()->createUrl(MenuModule::ROUTE_STATIC_MENU, array(MenuModule::ROUTE_STATIC_MENU_PARAM => $url));
        return $result;
    }

}