<div class="box-flash-photo-gallery">
    <h3 class="title-main"><a href="/photoalbum/">Фотогалерея</a></h3>
    <div class="row photos-list">
        <?php foreach($image as $i) { ?>

            <div class=" col-md-6 photo-item ib">
                <a href="<?php echo $i['full']->getUrlPath(); ?>" class="js-popup-img">
                    <img src="<?php echo $i['rand_small']->getUrlPath(); ?>" alt="<?php echo AppHelper::cutQuotes($i['name']); ?>" />
                </a>
            </div>

        <?php } ?>
    </div>
</div>