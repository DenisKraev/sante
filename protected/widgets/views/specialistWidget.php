<div class="widget-specialist">
    <div class="specialist-list cf">
        <?php foreach($model as $modelItem){ ?>
            <?php if($modelItem->getImagePreview('_avatar_widget')) { ?>
                <div class="specialist-item" style="background: url('<?php echo $modelItem->getImagePreview('_avatar_widget')->getUrlPath(); ?>') 50% 0% no-repeat; background-size: cover; -o-background-size: cover; -webkit-background-size: cover">
            <?php } else { ?>
                <div class="specialist-item" style="background: url('/content/no-image.jpg') 50% 50% no-repeat; background-size: cover; -o-background-size: cover; -webkit-background-size: cover">
            <?php } ?>
                <div class="overlay">
                    <div class="cell">
                        <a href="/specialist/<?php echo $modelItem->id_app_specialist; ?>" class="name ib"><?php echo $modelItem->name; ?></a>
                        <div class="job-title"><?php echo $modelItem->job_title; ?></div>
                        <a href="/specialist/<?php echo $modelItem->id_app_specialist; ?>"  class="btn-main">Задать вопрос</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <a class="more" href="/specialist/">
            <div class="cell">
                <div class="title">Высший медицинский персонал</div>
            </div>
        </a>
    </div>
</div>