<div class="box-flash-news">
    <h3 class="title-main"><a href="/news/">Новости</a></h3>
    <ul class="news-list">
        <?php foreach ($this->getNews() as $model) { ?>
            <li>
                <div class="media">
                    <div class="pull-left">
                        <?php if ($model->getImagePreview('_list_small')) { ?>
                            <a href="<?php echo $model->getUrl(); ?>">
                                <img src="<?php echo $model->getImagePreview('_list_small')->getUrlPath() ?>" alt="<?php echo AppHelper::cutQuotes($model->title); ?>" />
                            </a>
                        <?php } ?>
                    </div>
                    <div class="media-body">
                        <div class="text">
                            <a href="<?php echo $model->getUrl(); ?>"><?php echo AppHelper::Truncate($model->short, 80); ?></a>
                        </div>
                        <div class="date"><?php echo Yii::app()->dateFormatter->formatDateTime($model->date, 'long', null); ?></div>
                    </div>
                </div>
            </li>
        <?php } ?>
    </ul>
    <div class="all-news">
        <a href="/news/">Все новости</a>
    </div>
</div>