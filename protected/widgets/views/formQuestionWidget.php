<?php
    if (Yii::app()->user->hasFlash('form-question-success')) {
        $this->widget('AlertWidget', array(
            'title' => 'Задать вопрос',
            'message' => Yii::app()->user->getFlash('form-question-success'),
    ));
}
?>

<div class="box-form-question">

    <h3 class="title-main white">Задать вопрос</h3>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'form-question-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'action' => '/questionanswer/sendquestion/',
    ));
    ?>

    <div class="form-row specialist">
        <?php echo $form->textField($model,'id_app_specialist',array('class'=> 'style-form-text', 'value' => $selectedId, 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'id_app_specialist'); ?>
    </div>

    <div class="form-row name-specialist">
        <?php echo $form->textField($model,'name_specialist',array('class'=> 'style-form-text', 'value' => $nameSpecialist, 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name_specialist'); ?>
    </div>

    <div class="form-row name">
        <?php echo $form->textField($model,'name',array('class'=> 'style-form-text', 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="cf">
        <div class="form-row immya">
            <?php echo $form->textField($model,'immya',array('class'=> 'style-form-text', 'placeholder' => 'Ваше имя', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'immya'); ?>
        </div>


        <div class="form-row email">
            <?php echo $form->emailField($model,'email', array('class'=> 'style-form-text', 'placeholder' => 'Ваш Email', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>

    <div class="form-row">
        <?php echo $form->textField($model,'theme', array('class'=> 'style-form-text', 'placeholder' => $model->getAttributeLabel('theme'), 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'theme'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->textArea($model,'question', array('class'=> 'style-form-textarea', 'placeholder' => $model->getAttributeLabel('question'), 'rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'question'); ?>
    </div>

    <div class="form-actions">
        <?php echo CHtml::submitButton('Написать', array('class' => 'btn-main')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>