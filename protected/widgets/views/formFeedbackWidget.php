<?php
    if (Yii::app()->user->hasFlash('form-feedback-success')) {
        $this->widget('AlertWidget', array(
            'title' => 'Написать нам',
            'message' => Yii::app()->user->getFlash('form-feedback-success'),
    ));
}
?>

<div id="box-form-feedback" class="box-form-feedback">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'form-feedback-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'action' => '/contact/sendfeedback/',
    ));
    ?>

    <div class="top-part cf">
        <div class="form-row name">
            <?php echo $form->textField($model,'name',array('class'=> 'style-form-text gray', 'placeholder' => $model->getAttributeLabel('name'), 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div>

        <div class="form-row phone">
            <?php echo $form->textField($model,'phone',array('class'=> 'style-form-text gray', 'placeholder' => $model->getAttributeLabel('phone'), 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'phone'); ?>
        </div>

        <div class="form-row email">
            <?php echo $form->emailField($model,'email',array('class'=> 'style-form-text gray', 'placeholder' => $model->getAttributeLabel('email'), 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>
    <div class="form-row">
        <?php echo $form->textArea($model,'text',array('class'=> 'style-form-textarea gray', 'placeholder' => $model->getAttributeLabel('text'), 'rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'text'); ?>
    </div>
    <?php if(extension_loaded('gd')): ?>
    <div class="form-row">
        <?php $this->widget('CCaptcha', array('clickableImage'=>true, 'showRefreshButton'=>false)); ?>
        <div class="captcha ib">
            <?php echo CHtml::activeTextField($model, 'verifyCode', array('autocomplete' => 'off', 'class' => 'style-form-text gray', 'placeholder' => 'Код проверки')); ?>
            <?php echo $form->error($model, 'verifyCode'); ?>
        </div>
    </div>
    <?php endif; ?>

    <div class="form-actions">
        <?php echo CHtml::submitButton('Отправить', array('class' => 'btn-main')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>