<div class="box-services">
    <div class="services-list">
        <?php foreach($model as $modelItem){ ?>
            <?php
                switch ($modelItem->style) {
                    case 1:
                        $style = 'blue';
                        break;
                    case 2:
                        $style = 'green';
                        break;
                    case 3:
                        $style = 'white';
                        break;
                }
            ?>
            <a href="/services/<?php echo $modelItem->alias; ?>" class="services-item ib col-3 <?php echo $style; ?>">
                <div class="bg" style="background: url('<?php echo $modelItem->getImagePreview('_widget')->getUrlPath() ?>') 50% 50% no-repeat; background-size: cover; -o-background-size: cover; -webkit-background-size: cover;"></div>
                <div class="table">
                    <div class="cell">
                        <h2 class="name"><?php echo $modelItem->name; ?></h2>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</div>