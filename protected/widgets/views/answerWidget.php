<div class="widget-answer">
    <div class="answer-list">
        <?php foreach($model as $modelItem) { ?>
            <div class="answer-item">
                <div class="name"><?php echo $modelItem->immya; ?> <span class="date">(<?php echo Yii::app()->dateFormatter->format('dd.M.yyyy в HH:mm', $modelItem->datetime); ?>)</span></div>
                <h3 class="theme"><?php echo $modelItem->theme; ?></h3>
                <div class="question text-main"><?php echo $modelItem->question; ?></div>
                <?php if($modelItem->answer) { ?>
                    <div class="answer"><?php echo $modelItem->answer; ?></div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>