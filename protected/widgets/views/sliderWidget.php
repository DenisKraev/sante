<?php if($model) { ?>
    <div class="box-slider">
        <div class="slider gallery-hidden">
            <?php foreach($model as $i=>$item) { ?>
                <div class="slide" style="background: url('<?php echo $item->getImagePreview('_slider')->getUrlPath() ?>') 100% 50% no-repeat; background-size: cover; -o-background-size: cover; -webkit-background-size: cover;">
                    <div class="text ib">
                        <div class="cell">
                            <h2><?php echo $item->title; ?></h2>
                            <?php if($item->subtitle) { ?>
                                <h3><span><?php echo $item->subtitle; ?></span></h3>
                            <?php } ?>
                            <?php if($item->text) { ?>
                                <p><?php echo $item->text; ?></p>
                            <?php } ?>
                            <?php if($item->url) { ?>
                                <a class="more ib" href="<?php echo $item->url; ?>">Подробнее</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>