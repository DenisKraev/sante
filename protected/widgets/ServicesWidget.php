<?php

class ServicesWidget extends DaWidget {
    public function run() {

        $criteria=new CDbCriteria;
        $criteria->addCondition('visible = 1');
        $criteria->addCondition('on_main = 1');
        $criteria->limit = 3;
        $criteria->order = 'sequence ASC';

        $model = Services::model()->findAll($criteria);

        $this->render('servicesWidget',array(
            'model'=>$model,
        ));

    }
}