<?php

class FormQuestionWidget extends DaWidget {

    public $selectedId;
    public $nameSpecialist;

    public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $model = BaseActiveRecord::newModel('QuestionAnswer');

        $this->render('formQuestionWidget',array(
            'model' => $model,
            'selectedId' => $this->selectedId,
            'nameSpecialist' => $this->nameSpecialist,
        ));
    }

}