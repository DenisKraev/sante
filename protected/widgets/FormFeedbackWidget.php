<?php

class FormFeedbackWidget extends DaWidget {

    public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $model = BaseActiveRecord::newModel('FormFeedback');
        $this->render('formFeedbackWidget', array('model' => $model));
    }

}