<?php

class FlashRandomPhotoWidget extends DaWidget  implements IParametersConfig {
    /**
     * @var int Ширина px
     */
    public $width = 100;
    /**
     * @var int Высота px
     */
    public $height = 100;
    /**
     * @var string Постфикс
     */
    public $postfix = '_rand_small';
    /**
     * @var int Качество
     */
    public $quality = 80;
    /**
     * @var mixed Тип кадрирования
     */
    public $cropType = null;
    /**
     * @var mixed Масштабирование
     */
    public $resize = false;

    public $countPhoto = 2;

    public static function getParametersConfig() {
        return array(
            'width' => array(
                'type' => DataType::INT,
                'default' => 283,
                'label' => 'Ширина изображения (px)',
                'required' => true,
            ),
            'height' => array(
                'type' => DataType::INT,
                'default' => 205,
                'label' => 'Высота изображения (px)',
                'required' => true,
            ),
            'postfix' => array(
                'type' => DataType::VARCHAR,
                'default' => '_rand',
                'label' => 'Постфикс имен файлов',
                'required' => false,
            ),
            'quality' => array(
                'type' => DataType::INT,
                'default' => 80,
                'label' => 'Степень сжатия (%)',
                'required' => false,
            ),
            'cropType' => array(
                'type' => DataType::EVAL_EXPRESSION,
                'default' => 'null',
                'label' => 'Тип кадрирования',
                'required' => false,
            ),
            'resize' => array(
                'type' => DataType::EVAL_EXPRESSION,
                'default' => 'false',
                'label' => 'Масштабировать',
                'required' => false,
            ),
        );
    }

    public function run() {

        $criteria = new CDbCriteria();
        $criteria->condition = 'id_photogallery_object=:id_photogallery_object';
        $criteria->params = array(':id_photogallery_object' => 'project-fotoalbomy');
        $criteria->addCondition('visible = 1');

        $count = PhotogalleryPhoto::model()->count($criteria);

        $countPhotoVisible = $count < $this->countPhoto ? $count : $this->countPhoto;

        $numbers = array();
        for ($i = 1; $i <= $countPhotoVisible; $i++) {
            do {
                $_num = mt_rand(0, $count-1);
            } while(in_array($_num, $numbers));
            $numbers[$i] = $_num;
        }

        $image = array();
        foreach ($numbers as $i => $offset) {

            $criteria2 = new CDbCriteria();
            $criteria2->condition = 'id_photogallery_object=:id_photogallery_object';
            $criteria2->params = array(':id_photogallery_object' => 'project-fotoalbomy');
            $criteria2->addCondition('visible = 1');
            $criteria2->offset = $offset;

            $model = PhotogalleryPhoto::model()->find($criteria2);
            if ($model == null) {
                return;
            }

            if ($model->image) {
                $image[$i]['rand_small'] = $model->image->getPreview(
                    $this->width,
                    $this->height,
                    $this->postfix,
                    $this->cropType,
                    $this->quality,
                    $this->resize
                );
                $image[$i]['full'] = $model->image->getPreview($w = 1200, '', $postfix = '_full', $cropType = null, $quality = 90, $resize = false);
                $image[$i]['name'] = $model->name;
            }
        }

        $this->render('flashRandomPhotoWidget', array(
            'image' => $image,
        ));
    }
}