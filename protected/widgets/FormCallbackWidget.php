<?php

class formCallbackWidget extends DaWidget {
    public function run() {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        $model = BaseActiveRecord::newModel('FormCallback');
        $this->render('formCallbackWidget', array('model' => $model));
    }
}