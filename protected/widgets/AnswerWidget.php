<?php

class AnswerWidget extends DaWidget {
    public function run() {

        $criteria = new CDbCriteria;

        $criteria->with = array('Specialist');
        $criteria->addCondition('t.visible = 1');
        $criteria->addInCondition('Specialist.id_app_specialist', array($_GET["id"]));
        $criteria->order = 't.datetime DESC';

        $model = QuestionAnswer::model()->findAll($criteria);

        $this->render('answerWidget',array(
            'model'=>$model,
        ));

    }
}