<?php

class SliderWidget extends DaWidget {
    public function run() {

        $criteria=new CDbCriteria;
        $criteria->condition = 'visible=:visible';
        $criteria->params = array(':visible' => 1);
        $criteria->order = 'sequence ASC';

        $model = MainSlider::model()->findAll($criteria);

        $this->render('sliderWidget',array(
            'model'=>$model,
        ));

    }
}