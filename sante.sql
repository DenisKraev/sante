-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 29 2014 г., 11:42
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sante`
--

-- --------------------------------------------------------

--
-- Структура таблицы `app_block_content`
--

CREATE TABLE IF NOT EXISTS `app_block_content` (
  `id_app_block_content` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `explanation` varchar(255) DEFAULT NULL COMMENT 'Объяснение',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Видимость',
  `text` longtext COMMENT 'Обычный текст по русски',
  `html` longtext COMMENT 'html контент по русски',
  PRIMARY KEY (`id_app_block_content`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Блоки контента' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `app_block_content`
--

INSERT INTO `app_block_content` (`id_app_block_content`, `name`, `explanation`, `state`, `text`, `html`) VALUES
(3, 'about_text', 'Текст о компании на главной', 1, '', '<h2 class="title-main">Центр семейной медицины &laquo;САНТЭ&raquo;</h2>\r\n<p class="text-main">Современные медицинские центры, как правило, предлагают своим пациентам все способы оздоровления и лечения. Клиники совмещают диагностику с назначением и проведением терапевтического, а далее реабилитационного курса. Это очень удобно, так как в значительной степени экономит полезное время человека, позволяя, во-первых, не выпадать надолго из привычного ритма жизни, а, во-вторых, не дать перейти заболеванию в хроническую форму.</p>\r\n<a class="a-main" href="/page/o_centre/">СЕРТИФИКАТЫ</a>');

-- --------------------------------------------------------

--
-- Структура таблицы `app_contact`
--

CREATE TABLE IF NOT EXISTS `app_contact` (
  `id_app_contact` int(8) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL COMMENT 'Содержание',
  PRIMARY KEY (`id_app_contact`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Контакты' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `app_contact`
--

INSERT INTO `app_contact` (`id_app_contact`, `content`) VALUES
(1, '<h2 class="title-main">Контакты</h2>\r\n<p class="text-main">Мы всегда рады видеть Вас в нашем медицинском цетре. Мы находимся по адресу Труда 62.</p>\r\n<p class="text-main">Запись по телефонам: 35-46-44 или 45-46-44</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `app_form_callback`
--

CREATE TABLE IF NOT EXISTS `app_form_callback` (
  `id_app_form_callback` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Ваше имя',
  `phone` varchar(255) NOT NULL COMMENT 'Ваш телефон',
  `datetime` int(10) unsigned DEFAULT NULL COMMENT 'Дата отправки',
  PRIMARY KEY (`id_app_form_callback`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Форма заказа звонка' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `app_form_feedback`
--

CREATE TABLE IF NOT EXISTS `app_form_feedback` (
  `id_app_form_feedback` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Ваше имя',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Ваш телефон',
  `email` varchar(255) NOT NULL COMMENT 'Ваш e-mail',
  `text` longtext NOT NULL COMMENT 'Текст сообщения',
  `datetime` int(10) unsigned DEFAULT NULL COMMENT 'Дата отправки',
  PRIMARY KEY (`id_app_form_feedback`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Форма обратной связи' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `app_form_feedback`
--

INSERT INTO `app_form_feedback` (`id_app_form_feedback`, `name`, `phone`, `email`, `text`, `datetime`) VALUES
(1, 'sd', 'erg', 'esrg@tg.ee', 'hsrthdrth', 1419515048);

-- --------------------------------------------------------

--
-- Структура таблицы `app_main_slider`
--

CREATE TABLE IF NOT EXISTS `app_main_slider` (
  `id_app_main_slider` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `subtitle` varchar(255) DEFAULT NULL COMMENT 'Подзаголовок',
  `image` int(8) NOT NULL COMMENT 'Картинка',
  `text` longtext COMMENT 'Текст (описание)',
  `url` varchar(255) DEFAULT NULL COMMENT 'Ссылка',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_app_main_slider`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Слайдер' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_main_slider`
--

INSERT INTO `app_main_slider` (`id_app_main_slider`, `title`, `subtitle`, `image`, `text`, `url`, `sequence`, `visible`) VALUES
(1, 'Лучшая диагностика', NULL, 1, 'Описание', '#', 1, 1),
(2, 'Лучшая диагностика', 'На лучшем оборудовании', 11, 'УЗИ-диагностика занимает одно из самых ведущих мест в нашей современной клинической медицине.', '#', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_photoalbum`
--

CREATE TABLE IF NOT EXISTS `app_photoalbum` (
  `id_app_photoalbum` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название альбома',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `alias` varchar(255) NOT NULL COMMENT 'В адресной строке',
  PRIMARY KEY (`id_app_photoalbum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Фотоальбомы' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `app_question_answer`
--

CREATE TABLE IF NOT EXISTS `app_question_answer` (
  `id_app_question_answer` int(8) NOT NULL AUTO_INCREMENT,
  `id_app_specialist` int(8) NOT NULL COMMENT 'Специалист',
  `immya` varchar(255) NOT NULL COMMENT 'Отправитель',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `theme` varchar(255) NOT NULL COMMENT 'Тема вопроса',
  `question` longtext NOT NULL COMMENT 'Вопрос',
  `answer` longtext COMMENT 'Ответ',
  `datetime` int(10) unsigned DEFAULT NULL COMMENT 'Дата отправки',
  `send` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отправить на email спрашивающего',
  `visible` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Видимость',
  PRIMARY KEY (`id_app_question_answer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Вопрос-ответ' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `app_question_answer`
--

INSERT INTO `app_question_answer` (`id_app_question_answer`, `id_app_specialist`, `immya`, `email`, `theme`, `question`, `answer`, `datetime`, `send`, `visible`) VALUES
(2, 1, 'Денис', 'aff@ferg.er', 'Тема', 'Здравствуйте! Подскажите пожалуйста. У меня уже третий месяц растет живот, я беременна или у меня просто газы? Можно ли записаться к вам на узи', 'Здравствуйте. Запись по телефону 35-46-44', 1419407078, 0, 1),
(3, 2, 'Ден', 'er@er.er', 'ntvf', 'argaerg', NULL, 1419416965, 0, 1),
(4, 1, 'Денис', 'qw@qw.qw', 'Тема вопроса', 'Здравствуйте! Подскажите пожалуйста. У меня уже третий месяц растет живот, я беременна или у меня просто газы? Можно ли записаться к вам на узи', NULL, 1419422744, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_services`
--

CREATE TABLE IF NOT EXISTS `app_services` (
  `id_app_services` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `alias` varchar(255) NOT NULL COMMENT 'В адресной строке',
  `style` int(8) NOT NULL COMMENT 'Внешний вид блока услуги',
  `content` longtext NOT NULL COMMENT 'Контент',
  `on_main` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Отображать на главной странице',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `image` int(8) NOT NULL COMMENT 'Картинка на фон блока',
  `page_title` varchar(255) DEFAULT NULL COMMENT 'Заголовок <title>',
  `meta_keywords` longtext COMMENT 'Ключевые слова <meta name="keywords">',
  `meta_description` longtext COMMENT 'Описание <meta name="description">',
  PRIMARY KEY (`id_app_services`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Услуги' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `app_services`
--

INSERT INTO `app_services` (`id_app_services`, `name`, `alias`, `style`, `content`, `on_main`, `visible`, `sequence`, `image`, `page_title`, `meta_keywords`, `meta_description`) VALUES
(1, 'Узи-диагностика', 'uzi', 1, '<h2 class="title-main">Узи-диагностика</h2>\r\n<p class="text-main">Ультразвуковым исследованием (УЗИ-диагностика) называется неинвазивное обследование внутренних органов пациента, а также протекающих в них специальных процессов при помощи ультразвуковых волн. УЗИ-диагностика занимает одно из самых ведущих мест в нашей современной клинической медицине.</p>\r\n<h2 class="title-main">Почему УЗИ-диагностика является такой популярной?</h2>\r\n<p class="text-main">Ультразвуковым исследованием (УЗИ-диагностика) называется неинвазивное обследование внутренних органов пациента, а также протекающих в них специальных процессов при помощи ультразвуковых волн. УЗИ-диагностика занимает одно из самых ведущих мест в нашей современной клинической медицине.</p>\r\n<h2 class="title-main">Цены</h2>\r\n<table border="0" class="table-main">\r\n<tbody>\r\n<tr>\r\n<td>Брюшная полость</td>\r\n<td>233 руб</td>\r\n</tr>\r\n<tr>\r\n<td>Брюшная полость</td>\r\n<td>234 руб</td>\r\n</tr>\r\n<tr>\r\n<td>Предстательная железа, почки, мочевой пузырь фцафцуа фкпфукп уцкпцыукп укпыукпыук фкупыукпы ыфукпфыукпыфук ыукпыфукп</td>\r\n<td>421 руб</td>\r\n</tr>\r\n<tr>\r\n<td>Брюшная полость</td>\r\n<td>112234 руб</td>\r\n</tr>\r\n<tr>\r\n<td>Брюшная полость</td>\r\n<td>23 руб</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, 1, 1, 16, NULL, '', ''),
(2, 'Гинекология', 'ginekologiya', 2, 'фцпафкуц', 1, 1, 2, 20, NULL, '', ''),
(3, 'Внутривенные капельницы', 'vnutrivennye_kapelnitsy', 3, 'кпфук', 1, 1, 3, 22, NULL, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `app_specialist`
--

CREATE TABLE IF NOT EXISTS `app_specialist` (
  `id_app_specialist` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Имя (ФИО)',
  `job_title` varchar(255) NOT NULL COMMENT 'Должность',
  `experience` varchar(255) DEFAULT NULL COMMENT 'Опыт работы',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `avatar` int(8) DEFAULT NULL COMMENT 'Аватар',
  `description` longtext COMMENT 'Описание',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `page_title` varchar(255) DEFAULT NULL COMMENT 'Заголовок <title>',
  `meta_keywords` longtext COMMENT 'Ключевые слова <meta name="keywords">',
  `meta_description` longtext COMMENT 'Описание <meta name="description">',
  `on_main` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отображать на главной странице',
  PRIMARY KEY (`id_app_specialist`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Специалисты' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_specialist`
--

INSERT INTO `app_specialist` (`id_app_specialist`, `name`, `job_title`, `experience`, `visible`, `avatar`, `description`, `sequence`, `page_title`, `meta_keywords`, `meta_description`, `on_main`) VALUES
(1, 'Данилова Любовь Валерьевна', 'Невролог', '5 лет', 1, 29, 'Образование: Пермский государственный медицинский институт окончил в 1993г. Диплом ШВ №037949, дата выдачи 25.06.1993 г., специальность – лечебное дело. Кировская государственная мед.академия прошел профессиональную переподготовку с 18.09.07 г по 28.12.07 г. Диплом № 040609, дата выдачи 28.12.07 г., специальность – ультразвуковая диагностика. Ординатура в Кировском государственном медицинском институте с 01.09.93 г. по 31.08.95 г. по акушерству и гинекологии. Удостоверение ШВ №037949 Кировская государственная мед.академия. Сертификат А №2997621, дата выдачи 28.12.2007 г. Действителен 5 лет. Специальность – ультразвуковая диагностика.', 1, NULL, '', '', 1),
(2, 'Данилова Любовь Валерьевна', 'Гинеколог', '7 лет', 1, NULL, 'Образование: Пермский государственный медицинский институт окончил в 1993г. Диплом ШВ №037949, дата выдачи 25.06.1993 г., специальность – лечебное дело. Кировская государственная мед.академия прошел профессиональную переподготовку с 18.09.07 г по 28.12.07 г. Диплом № 040609, дата выдачи 28.12.07 г., специальность – ультразвуковая диагностика. Ординатура в Кировском государственном медицинском институте с 01.09.93 г. по 31.08.95 г. по акушерству и гинекологии. Удостоверение ШВ №037949 Кировская государственная мед.академия. Сертификат А №2997621, дата выдачи 28.12.2007 г. Действителен 5 лет. Специальность – ультразвуковая диагностика.', 2, NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_videoalbum`
--

CREATE TABLE IF NOT EXISTS `app_videoalbum` (
  `id_app_videoalbum` int(8) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL COMMENT 'Ссылка на видео',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_app_videoalbum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Видоальбом' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `da_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `da_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_auth_assignment`
--

INSERT INTO `da_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('dev', '106', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `da_auth_item`
--

CREATE TABLE IF NOT EXISTS `da_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_auth_item`
--

INSERT INTO `da_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('create_object_101', 0, 'Операция создание экземпляра объекта Наборы модулей', NULL, 'N;'),
('create_object_103', 0, 'Операция создание экземпляра объекта Модули сайта', NULL, 'N;'),
('create_object_105', 0, 'Операция создание экземпляра объекта Голосование', NULL, 'N;'),
('create_object_106', 0, 'Операция создание экземпляра объекта Ответы на голосование', NULL, 'N;'),
('create_object_20', 0, 'Операция создание экземпляра объекта Объекты', NULL, 'N;'),
('create_object_21', 0, 'Операция создание экземпляра объекта Свойства объекта', NULL, 'N;'),
('create_object_22', 0, 'Операция создания для объекта Типы данных', NULL, 'N;'),
('create_object_23', 0, 'Операция создание экземпляра объекта Группы пользователей', NULL, 'N;'),
('create_object_24', 0, 'Операция создание экземпляра объекта Пользователи', NULL, 'N;'),
('create_object_250', 0, 'Операция создание экземпляра объекта Комментарии', NULL, 'N;'),
('create_object_26', 0, 'Операция создание экземпляра объекта Права доступа', NULL, 'N;'),
('create_object_260', 0, 'Операция создание экземпляра объекта Баннеры', NULL, 'N;'),
('create_object_261', 0, 'Операция создание экземпляра объекта Баннерные места', NULL, 'N;'),
('create_object_27', 0, 'Операция создание экземпляра объекта Справочники', NULL, 'N;'),
('create_object_28', 0, 'Операция создание экземпляра объекта Значения справочника', NULL, 'N;'),
('create_object_30', 0, 'Операция создание экземпляра объекта Настройки сайта', NULL, 'N;'),
('create_object_31', 0, 'Операция создание экземпляра объекта Домены сайта', NULL, 'N;'),
('create_object_33', 0, 'Операция создание экземпляра объекта Формат сообщения', NULL, 'N;'),
('create_object_34', 0, 'Операция создание экземпляра объекта Подписчики на события', NULL, 'N;'),
('create_object_35', 0, 'Операция создание экземпляра объекта Тип события', NULL, 'N;'),
('create_object_47', 0, 'Операция создание экземпляра объекта Права пользователей', NULL, 'N;'),
('create_object_50', 0, 'Операция создание экземпляра объекта Почтовые аккаунты', NULL, 'N;'),
('create_object_500', 0, 'Операция создание экземпляра объекта Фотогалереи', NULL, 'N;'),
('create_object_501', 0, 'Операция создание экземпляра объекта Фотографии', NULL, 'N;'),
('create_object_502', 0, 'Операция создание экземпляра объекта Новости', NULL, 'N;'),
('create_object_503', 0, 'Операция создание экземпляра объекта Категории новостей', NULL, 'N;'),
('create_object_505', 0, 'Операция создание экземпляра объекта Вопрос', NULL, 'N;'),
('create_object_506', 0, 'Операция создание экземпляра объекта Ответ', NULL, 'N;'),
('create_object_507', 0, 'Операция создание экземпляра объекта Отвечающий', NULL, 'N;'),
('create_object_508', 0, 'Операция создание экземпляра объекта Специализация отвечающего', NULL, 'N;'),
('create_object_509', 0, 'Операция создание экземпляра объекта Категории продукции', NULL, 'N;'),
('create_object_51', 0, 'Операция создание экземпляра объекта Планировщик', NULL, 'N;'),
('create_object_511', 0, 'Операция создание экземпляра объекта Продукция', NULL, 'N;'),
('create_object_512', 0, 'Операция создание экземпляра объекта Вопрос-ответ', NULL, 'N;'),
('create_object_513', 0, 'Операция создание экземпляра объекта OpenID провайдер', NULL, 'N;'),
('create_object_514', 0, 'Операция создание экземпляра объекта OpenId аккаунты', NULL, 'N;'),
('create_object_517', 0, 'Операция создание экземпляра объекта Обратная связь', NULL, 'N;'),
('create_object_520', 0, 'Операция создание экземпляра объекта Витрина', NULL, 'N;'),
('create_object_521', 0, 'Операция создание экземпляра объекта Викторины', NULL, 'N;'),
('create_object_522', 0, 'Операция создание экземпляра объекта Вопросы викторины', NULL, 'N;'),
('create_object_523', 0, 'Операция создание экземпляра объекта Варианты ответов', NULL, 'N;'),
('create_object_524', 0, 'Операция создание экземпляра объекта Ответ пользователя', NULL, 'N;'),
('create_object_525', 0, 'Операция создание экземпляра объекта Брэнды', NULL, 'N;'),
('create_object_529', 0, 'Операция создание экземпляра объекта Статусы остатка продукции', NULL, 'N;'),
('create_object_530', 0, 'Операция создание экземпляра объекта Отзывы клиентов', NULL, 'N;'),
('create_object_54', 0, 'Операция создание экземпляра объекта Доступные локализации', NULL, 'N;'),
('create_object_61', 0, 'Операция создание экземпляра объекта Инструкции', NULL, 'N;'),
('create_object_63', 0, 'Операция создание экземпляра объекта Представление', NULL, 'N;'),
('create_object_66', 0, 'Операция создание экземпляра объекта Колонка представления', NULL, 'N;'),
('create_object_80', 0, 'Операция создание экземпляра объекта php-скрипты', NULL, 'N;'),
('create_object_86', 0, 'Операция создание экземпляра объекта Интерфейс php-скрипта', NULL, 'N;'),
('create_object_project-bloki-kontenta', 0, 'Операция создания для объекта Блоки контента', NULL, 'N;'),
('create_object_project-forma-obratnoi-svyazi', 0, 'Операция создания для объекта Форма обратной связи', NULL, 'N;'),
('create_object_project-forma-zakaza-zvonka', 0, 'Операция создания для объекта Форма заказа звонка', NULL, 'N;'),
('create_object_project-fotoalbomy', 0, 'Операция создания для объекта Фотоальбомы', NULL, 'N;'),
('create_object_project-kontakty', 0, 'Операция создания для объекта Контакты', NULL, 'N;'),
('create_object_project-slaider', 0, 'Операция создания для объекта Слайдер', NULL, 'N;'),
('create_object_project-spezialisty', 0, 'Операция создания для объекта Специалисты', NULL, 'N;'),
('create_object_project-uslugi', 0, 'Операция создания для объекта Услуги', NULL, 'N;'),
('create_object_project-vidoalbom', 0, 'Операция создания для объекта Видоальбом', NULL, 'N;'),
('create_object_project-vopros-otvet', 0, 'Операция создания для объекта Вопрос-ответ', NULL, 'N;'),
('create_object_ygin-menu', 0, 'Операция создания для объекта Меню', NULL, 'N;'),
('create_object_ygin-views-generator', 0, 'Операция создания для объекта Генерация вьюхи', NULL, 'N;'),
('delete_object_101', 0, 'Операция удаления экземпляра объекта Наборы модулей', NULL, 'N;'),
('delete_object_103', 0, 'Операция удаления экземпляра объекта Модули сайта', NULL, 'N;'),
('delete_object_105', 0, 'Операция удаления экземпляра объекта Голосование', NULL, 'N;'),
('delete_object_106', 0, 'Операция удаления экземпляра объекта Ответы на голосование', NULL, 'N;'),
('delete_object_20', 0, 'Операция удаления экземпляра объекта Объекты', NULL, 'N;'),
('delete_object_21', 0, 'Операция удаления экземпляра объекта Свойства объекта', NULL, 'N;'),
('delete_object_22', 0, 'Операция удаления для объекта Типы данных', NULL, 'N;'),
('delete_object_23', 0, 'Операция удаления экземпляра объекта Группы пользователей', NULL, 'N;'),
('delete_object_24', 0, 'Операция удаления экземпляра объекта Пользователи', NULL, 'N;'),
('delete_object_250', 0, 'Операция удаления экземпляра объекта Комментарии', NULL, 'N;'),
('delete_object_26', 0, 'Операция удаления экземпляра объекта Права доступа', NULL, 'N;'),
('delete_object_260', 0, 'Операция удаления экземпляра объекта Баннеры', NULL, 'N;'),
('delete_object_261', 0, 'Операция удаления экземпляра объекта Баннерные места', NULL, 'N;'),
('delete_object_27', 0, 'Операция удаления экземпляра объекта Справочники', NULL, 'N;'),
('delete_object_28', 0, 'Операция удаления экземпляра объекта Значения справочника', NULL, 'N;'),
('delete_object_30', 0, 'Операция удаления экземпляра объекта Настройки сайта', NULL, 'N;'),
('delete_object_33', 0, 'Операция удаления экземпляра объекта Формат сообщения', NULL, 'N;'),
('delete_object_34', 0, 'Операция удаления экземпляра объекта Подписчики на события', NULL, 'N;'),
('delete_object_35', 0, 'Операция удаления экземпляра объекта Тип события', NULL, 'N;'),
('delete_object_47', 0, 'Операция удаления экземпляра объекта Права пользователей', NULL, 'N;'),
('delete_object_50', 0, 'Операция удаления экземпляра объекта Почтовые аккаунты', NULL, 'N;'),
('delete_object_500', 0, 'Операция удаления экземпляра объекта Фотогалереи', NULL, 'N;'),
('delete_object_501', 0, 'Операция удаления экземпляра объекта Фотографии', NULL, 'N;'),
('delete_object_502', 0, 'Операция удаления экземпляра объекта Новости', NULL, 'N;'),
('delete_object_503', 0, 'Операция удаления экземпляра объекта Категории новостей', NULL, 'N;'),
('delete_object_505', 0, 'Операция удаления экземпляра объекта Вопрос', NULL, 'N;'),
('delete_object_506', 0, 'Операция удаления экземпляра объекта Ответ', NULL, 'N;'),
('delete_object_507', 0, 'Операция удаления экземпляра объекта Отвечающий', NULL, 'N;'),
('delete_object_508', 0, 'Операция удаления экземпляра объекта Специализация отвечающего', NULL, 'N;'),
('delete_object_509', 0, 'Операция удаления экземпляра объекта Категории продукции', NULL, 'N;'),
('delete_object_51', 0, 'Операция удаления экземпляра объекта Планировщик', NULL, 'N;'),
('delete_object_511', 0, 'Операция удаления экземпляра объекта Продукция', NULL, 'N;'),
('delete_object_512', 0, 'Операция удаления экземпляра объекта Вопрос-ответ', NULL, 'N;'),
('delete_object_513', 0, 'Операция удаления экземпляра объекта OpenID провайдер', NULL, 'N;'),
('delete_object_514', 0, 'Операция удаления экземпляра объекта OpenId аккаунты', NULL, 'N;'),
('delete_object_517', 0, 'Операция удаления экземпляра объекта Обратная связь', NULL, 'N;'),
('delete_object_519', 0, 'Операция удаления экземпляра объекта Заказы пользователей', NULL, 'N;'),
('delete_object_520', 0, 'Операция удаления экземпляра объекта Витрина', NULL, 'N;'),
('delete_object_521', 0, 'Операция удаления экземпляра объекта Викторины', NULL, 'N;'),
('delete_object_522', 0, 'Операция удаления экземпляра объекта Вопросы викторины', NULL, 'N;'),
('delete_object_523', 0, 'Операция удаления экземпляра объекта Варианты ответов', NULL, 'N;'),
('delete_object_524', 0, 'Операция удаления экземпляра объекта Ответ пользователя', NULL, 'N;'),
('delete_object_525', 0, 'Операция удаления экземпляра объекта Брэнды', NULL, 'N;'),
('delete_object_529', 0, 'Операция удаления экземпляра объекта Статусы остатка продукции', NULL, 'N;'),
('delete_object_530', 0, 'Операция удаления экземпляра объекта Отзывы клиентов', NULL, 'N;'),
('delete_object_531', 0, 'Операция удаления экземпляра объекта Уведомления', NULL, 'N;'),
('delete_object_54', 0, 'Операция удаления экземпляра объекта Доступные локализации', NULL, 'N;'),
('delete_object_61', 0, 'Операция удаления экземпляра объекта Инструкции', NULL, 'N;'),
('delete_object_63', 0, 'Операция удаления экземпляра объекта Представление', NULL, 'N;'),
('delete_object_66', 0, 'Операция удаления экземпляра объекта Колонка представления', NULL, 'N;'),
('delete_object_80', 0, 'Операция удаления экземпляра объекта php-скрипты', NULL, 'N;'),
('delete_object_86', 0, 'Операция удаления экземпляра объекта Интерфейс php-скрипта', NULL, 'N;'),
('delete_object_project-bloki-kontenta', 0, 'Операция удаления для объекта Блоки контента', NULL, 'N;'),
('delete_object_project-forma-obratnoi-svyazi', 0, 'Операция удаления для объекта Форма обратной связи', NULL, 'N;'),
('delete_object_project-forma-zakaza-zvonka', 0, 'Операция удаления для объекта Форма заказа звонка', NULL, 'N;'),
('delete_object_project-fotoalbomy', 0, 'Операция удаления для объекта Фотоальбомы', NULL, 'N;'),
('delete_object_project-kontakty', 0, 'Операция удаления для объекта Контакты', NULL, 'N;'),
('delete_object_project-slaider', 0, 'Операция удаления для объекта Слайдер', NULL, 'N;'),
('delete_object_project-spezialisty', 0, 'Операция удаления для объекта Специалисты', NULL, 'N;'),
('delete_object_project-uslugi', 0, 'Операция удаления для объекта Услуги', NULL, 'N;'),
('delete_object_project-vidoalbom', 0, 'Операция удаления для объекта Видоальбом', NULL, 'N;'),
('delete_object_project-vopros-otvet', 0, 'Операция удаления для объекта Вопрос-ответ', NULL, 'N;'),
('delete_object_ygin-invoice', 0, 'Операция удаления для объекта Счета', NULL, 'N;'),
('delete_object_ygin-menu', 0, 'Операция удаления для объекта Меню', NULL, 'N;'),
('delete_object_ygin-views-generator', 0, 'Операция удаления для объекта Генерация вьюхи', NULL, 'N;'),
('dev', 2, 'Разработчик', NULL, 'N;'),
('editor', 2, 'Редактор', NULL, 'N;'),
('edit_object_101', 0, 'Операция изменения экземпляра объекта Наборы модулей', NULL, 'N;'),
('edit_object_103', 0, 'Операция изменения экземпляра объекта Модули сайта', NULL, 'N;'),
('edit_object_105', 0, 'Операция изменения экземпляра объекта Голосование', NULL, 'N;'),
('edit_object_106', 0, 'Операция изменения экземпляра объекта Ответы на голосование', NULL, 'N;'),
('edit_object_20', 0, 'Операция изменения экземпляра объекта Объекты', NULL, 'N;'),
('edit_object_21', 0, 'Операция изменения экземпляра объекта Свойства объекта', NULL, 'N;'),
('edit_object_22', 0, 'Операция изменения для объекта Типы данных', NULL, 'N;'),
('edit_object_23', 0, 'Операция изменения экземпляра объекта Группы пользователей', NULL, 'N;'),
('edit_object_24', 0, 'Операция изменения экземпляра объекта Пользователи', NULL, 'N;'),
('edit_object_250', 0, 'Операция изменения экземпляра объекта Комментарии', NULL, 'N;'),
('edit_object_26', 0, 'Операция изменения экземпляра объекта Права доступа', NULL, 'N;'),
('edit_object_260', 0, 'Операция изменения экземпляра объекта Баннеры', NULL, 'N;'),
('edit_object_261', 0, 'Операция изменения экземпляра объекта Баннерные места', NULL, 'N;'),
('edit_object_27', 0, 'Операция изменения экземпляра объекта Справочники', NULL, 'N;'),
('edit_object_28', 0, 'Операция изменения экземпляра объекта Значения справочника', NULL, 'N;'),
('edit_object_29', 0, 'Операция изменения экземпляра объекта Группы системных параметров', NULL, 'N;'),
('edit_object_30', 0, 'Операция изменения экземпляра объекта Настройки сайта', NULL, 'N;'),
('edit_object_31', 0, 'Операция изменения экземпляра объекта Домены сайта', NULL, 'N;'),
('edit_object_33', 0, 'Операция изменения экземпляра объекта Формат сообщения', NULL, 'N;'),
('edit_object_34', 0, 'Операция изменения экземпляра объекта Подписчики на события', NULL, 'N;'),
('edit_object_35', 0, 'Операция изменения экземпляра объекта Тип события', NULL, 'N;'),
('edit_object_47', 0, 'Операция изменения экземпляра объекта Права пользователей', NULL, 'N;'),
('edit_object_50', 0, 'Операция изменения экземпляра объекта Почтовые аккаунты', NULL, 'N;'),
('edit_object_500', 0, 'Операция изменения экземпляра объекта Фотогалереи', NULL, 'N;'),
('edit_object_501', 0, 'Операция изменения экземпляра объекта Фотографии', NULL, 'N;'),
('edit_object_502', 0, 'Операция изменения экземпляра объекта Новости', NULL, 'N;'),
('edit_object_503', 0, 'Операция изменения экземпляра объекта Категории новостей', NULL, 'N;'),
('edit_object_505', 0, 'Операция изменения экземпляра объекта Вопрос', NULL, 'N;'),
('edit_object_506', 0, 'Операция изменения экземпляра объекта Ответ', NULL, 'N;'),
('edit_object_507', 0, 'Операция изменения экземпляра объекта Отвечающий', NULL, 'N;'),
('edit_object_508', 0, 'Операция изменения экземпляра объекта Специализация отвечающего', NULL, 'N;'),
('edit_object_509', 0, 'Операция изменения экземпляра объекта Категории продукции', NULL, 'N;'),
('edit_object_51', 0, 'Операция изменения экземпляра объекта Планировщик', NULL, 'N;'),
('edit_object_511', 0, 'Операция изменения экземпляра объекта Продукция', NULL, 'N;'),
('edit_object_512', 0, 'Операция изменения экземпляра объекта Вопрос-ответ', NULL, 'N;'),
('edit_object_513', 0, 'Операция изменения экземпляра объекта OpenID провайдер', NULL, 'N;'),
('edit_object_514', 0, 'Операция изменения экземпляра объекта OpenId аккаунты', NULL, 'N;'),
('edit_object_517', 0, 'Операция изменения экземпляра объекта Обратная связь', NULL, 'N;'),
('edit_object_519', 0, 'Операция изменения экземпляра объекта Заказы пользователей', NULL, 'N;'),
('edit_object_520', 0, 'Операция изменения экземпляра объекта Витрина', NULL, 'N;'),
('edit_object_521', 0, 'Операция изменения экземпляра объекта Викторины', NULL, 'N;'),
('edit_object_522', 0, 'Операция изменения экземпляра объекта Вопросы викторины', NULL, 'N;'),
('edit_object_523', 0, 'Операция изменения экземпляра объекта Варианты ответов', NULL, 'N;'),
('edit_object_524', 0, 'Операция изменения экземпляра объекта Ответ пользователя', NULL, 'N;'),
('edit_object_525', 0, 'Операция изменения экземпляра объекта Брэнды', NULL, 'N;'),
('edit_object_529', 0, 'Операция изменения экземпляра объекта Статусы остатка продукции', NULL, 'N;'),
('edit_object_530', 0, 'Операция изменения экземпляра объекта Отзывы клиентов', NULL, 'N;'),
('edit_object_531', 0, 'Операция изменения экземпляра объекта Уведомления', NULL, 'N;'),
('edit_object_54', 0, 'Операция изменения экземпляра объекта Доступные локализации', NULL, 'N;'),
('edit_object_61', 0, 'Операция изменения экземпляра объекта Инструкции', NULL, 'N;'),
('edit_object_63', 0, 'Операция изменения экземпляра объекта Представление', NULL, 'N;'),
('edit_object_66', 0, 'Операция изменения экземпляра объекта Колонка представления', NULL, 'N;'),
('edit_object_80', 0, 'Операция изменения экземпляра объекта php-скрипты', NULL, 'N;'),
('edit_object_86', 0, 'Операция изменения экземпляра объекта Интерфейс php-скрипта', NULL, 'N;'),
('edit_object_project-bloki-kontenta', 0, 'Операция изменения для объекта Блоки контента', NULL, 'N;'),
('edit_object_project-forma-obratnoi-svyazi', 0, 'Операция изменения для объекта Форма обратной связи', NULL, 'N;'),
('edit_object_project-forma-zakaza-zvonka', 0, 'Операция изменения для объекта Форма заказа звонка', NULL, 'N;'),
('edit_object_project-fotoalbomy', 0, 'Операция изменения для объекта Фотоальбомы', NULL, 'N;'),
('edit_object_project-kontakty', 0, 'Операция изменения для объекта Контакты', NULL, 'N;'),
('edit_object_project-slaider', 0, 'Операция изменения для объекта Слайдер', NULL, 'N;'),
('edit_object_project-spezialisty', 0, 'Операция изменения для объекта Специалисты', NULL, 'N;'),
('edit_object_project-uslugi', 0, 'Операция изменения для объекта Услуги', NULL, 'N;'),
('edit_object_project-vidoalbom', 0, 'Операция изменения для объекта Видоальбом', NULL, 'N;'),
('edit_object_project-vopros-otvet', 0, 'Операция изменения для объекта Вопрос-ответ', NULL, 'N;'),
('edit_object_ygin-invoice', 0, 'Операция изменения для объекта Счета', NULL, 'N;'),
('edit_object_ygin-menu', 0, 'Операция изменения для объекта Меню', NULL, 'N;'),
('edit_object_ygin-views-generator', 0, 'Операция изменения для объекта Генерация вьюхи', NULL, 'N;'),
('list_object_101', 0, 'Просмотр списка данных объекта Наборы модулей', NULL, 'N;'),
('list_object_103', 0, 'Просмотр списка данных объекта Модули сайта', NULL, 'N;'),
('list_object_20', 0, 'Просмотр списка данных объекта Объекты', NULL, 'N;'),
('list_object_21', 0, 'Просмотр списка данных объекта Свойства объекта', NULL, 'N;'),
('list_object_22', 0, 'Просмотр списка данных объекта Типы данных', NULL, 'N;'),
('list_object_23', 0, 'Просмотр списка данных объекта Группы пользователей', NULL, 'N;'),
('list_object_24', 0, 'Просмотр списка данных объекта Пользователи', NULL, 'N;'),
('list_object_250', 0, 'Просмотр списка данных объекта Комментарии', NULL, 'N;'),
('list_object_26', 0, 'Просмотр списка данных объекта Права доступа', NULL, 'N;'),
('list_object_27', 0, 'Просмотр списка данных объекта Справочники', NULL, 'N;'),
('list_object_28', 0, 'Просмотр списка данных объекта Значения справочника', NULL, 'N;'),
('list_object_29', 0, 'Просмотр списка данных объекта Группы системных параметров', NULL, 'N;'),
('list_object_30', 0, 'Просмотр списка данных объекта Настройки сайта', NULL, 'N;'),
('list_object_31', 0, 'Просмотр списка данных объекта Домены сайта', NULL, 'N;'),
('list_object_32', 0, 'Просмотр списка данных объекта Настройка прав', NULL, 'N;'),
('list_object_33', 0, 'Просмотр списка данных объекта Формат сообщения', NULL, 'N;'),
('list_object_34', 0, 'Просмотр списка данных объекта Подписчики на события', NULL, 'N;'),
('list_object_35', 0, 'Просмотр списка данных объекта Тип события', NULL, 'N;'),
('list_object_43', 0, 'Просмотр списка данных объекта SQL', NULL, 'N;'),
('list_object_47', 0, 'Просмотр списка данных объекта Права пользователей', NULL, 'N;'),
('list_object_50', 0, 'Просмотр списка данных объекта Почтовые аккаунты', NULL, 'N;'),
('list_object_500', 0, 'Просмотр списка данных объекта Галерея', NULL, 'N;'),
('list_object_501', 0, 'Просмотр списка данных объекта Фотографии', NULL, 'N;'),
('list_object_502', 0, 'Просмотр списка данных объекта Новости', NULL, 'N;'),
('list_object_505', 0, 'Просмотр списка данных объекта Вопрос', NULL, 'N;'),
('list_object_506', 0, 'Просмотр списка данных объекта Ответ', NULL, 'N;'),
('list_object_507', 0, 'Просмотр списка данных объекта Отвечающий', NULL, 'N;'),
('list_object_508', 0, 'Просмотр списка данных объекта Специализация отвечающего', NULL, 'N;'),
('list_object_51', 0, 'Просмотр списка данных объекта Планировщик', NULL, 'N;'),
('list_object_513', 0, 'Просмотр списка данных объекта OpenID провайдер', NULL, 'N;'),
('list_object_514', 0, 'Просмотр списка данных объекта OpenId аккаунты', NULL, 'N;'),
('list_object_521', 0, 'Просмотр списка данных объекта Викторины', NULL, 'N;'),
('list_object_522', 0, 'Просмотр списка данных объекта Вопросы викторины', NULL, 'N;'),
('list_object_523', 0, 'Просмотр списка данных объекта Варианты ответов', NULL, 'N;'),
('list_object_524', 0, 'Просмотр списка данных объекта Ответ пользователя', NULL, 'N;'),
('list_object_528', 0, 'Управление объектом Плагины системы', NULL, 'N;'),
('list_object_530', 0, 'Просмотр списка данных объекта Отзывы', NULL, 'N;'),
('list_object_531', 0, 'Просмотр списка данных объекта ид=531', NULL, 'N;'),
('list_object_54', 0, 'Просмотр списка данных объекта Доступные локализации', NULL, 'N;'),
('list_object_60', 0, 'Просмотр списка данных объекта Проверка файлов', NULL, 'N;'),
('list_object_61', 0, 'Просмотр списка данных объекта Инструкции', NULL, 'N;'),
('list_object_63', 0, 'Просмотр списка данных объекта Представление', NULL, 'N;'),
('list_object_66', 0, 'Просмотр списка данных объекта Колонка представления', NULL, 'N;'),
('list_object_80', 0, 'Просмотр списка данных объекта php-скрипты', NULL, 'N;'),
('list_object_86', 0, 'Просмотр списка данных объекта Интерфейс php-скрипта', NULL, 'N;'),
('list_object_89', 0, 'Просмотр списка данных объекта Очистить кэш', NULL, 'N;'),
('list_object_91', 0, 'Просмотр списка данных объекта Поисковый индекс', NULL, 'N;'),
('list_object_94', 0, 'Просмотр списка данных объекта Логи', NULL, 'N;'),
('list_object_a', 0, 'Просмотр списка данных объекта Фотографии', NULL, 'N;'),
('list_object_project-bloki-kontenta', 0, 'Просмотр списка данных объекта Блоки контента', NULL, 'N;'),
('list_object_project-forma-obratnoi-svyazi', 0, 'Просмотр списка данных объекта Форма обратной связи', NULL, 'N;'),
('list_object_project-forma-zakaza-zvonka', 0, 'Просмотр списка данных объекта Форма заказа звонка', NULL, 'N;'),
('list_object_project-fotoalbomy', 0, 'Просмотр списка данных объекта Фотоальбомы', NULL, 'N;'),
('list_object_project-kontakty', 0, 'Просмотр списка данных объекта Контакты', NULL, 'N;'),
('list_object_project-slaider', 0, 'Просмотр списка данных объекта Слайдер', NULL, 'N;'),
('list_object_project-spezialisty', 0, 'Просмотр списка данных объекта Специалисты', NULL, 'N;'),
('list_object_project-uslugi', 0, 'Просмотр списка данных объекта Услуги', NULL, 'N;'),
('list_object_project-vidoalbom', 0, 'Просмотр списка данных объекта Видоальбом', NULL, 'N;'),
('list_object_project-vopros-otvet', 0, 'Просмотр списка данных объекта Вопрос-ответ', NULL, 'N;'),
('list_object_ygin-gii', 0, 'Просмотр списка данных объекта gii (debug=true)', NULL, 'N;'),
('list_object_ygin-invoice', 0, 'Просмотр списка данных объекта Счета', NULL, 'N;'),
('list_object_ygin-menu', 0, 'Просмотр списка данных объекта Меню', NULL, 'N;'),
('list_object_ygin-override', 0, 'Просмотр списка данных объекта Переопределение представлений (debug=true)', NULL, 'N;'),
('list_object_ygin-views-generator', 0, 'Просмотр списка данных объекта Генерация вьюхи', NULL, 'N;'),
('showAdminPanel', 0, 'доступ к админке', NULL, 'N;'),
('view_object_250', 0, 'Операция просмотра для объекта Комментарии', NULL, 'N;'),
('view_object_26', 0, 'Операция просмотра экземпляра объекта Права доступа', NULL, 'N;'),
('view_object_30', 0, 'Операция просмотра экземпляра объекта Настройки сайта', NULL, 'N;'),
('view_object_32', 0, 'Операция просмотра экземпляра объекта Настройка прав', NULL, 'N;'),
('view_object_43', 0, 'Операция просмотра экземпляра объекта SQL', NULL, 'N;'),
('view_object_529', 0, 'Операция просмотра экземпляра объекта Статусы остатка продукции', NULL, 'N;'),
('view_object_531', 0, 'Операция просмотра экземпляра объекта Уведомления', NULL, 'N;'),
('view_object_60', 0, 'Операция просмотра экземпляра объекта Проверка файлов', NULL, 'N;'),
('view_object_89', 0, 'Операция просмотра экземпляра объекта Очистить кэш', NULL, 'N;'),
('view_object_91', 0, 'Операция просмотра для объекта Поисковый индекс', NULL, 'N;'),
('view_object_94', 0, 'Операция просмотра экземпляра объекта Логи', NULL, 'N;'),
('view_object_project-bloki-kontenta', 0, 'Операция просмотра для объекта Блоки контента', NULL, 'N;'),
('view_object_project-forma-obratnoi-svyazi', 0, 'Операция просмотра для объекта Форма обратной связи', NULL, 'N;'),
('view_object_project-forma-zakaza-zvonka', 0, 'Операция просмотра для объекта Форма заказа звонка', NULL, 'N;'),
('view_object_project-fotoalbomy', 0, 'Операция просмотра для объекта Фотоальбомы', NULL, 'N;'),
('view_object_project-kontakty', 0, 'Операция просмотра для объекта Контакты', NULL, 'N;'),
('view_object_project-slaider', 0, 'Операция просмотра для объекта Слайдер', NULL, 'N;'),
('view_object_project-spezialisty', 0, 'Операция просмотра для объекта Специалисты', NULL, 'N;'),
('view_object_project-uslugi', 0, 'Операция просмотра для объекта Услуги', NULL, 'N;'),
('view_object_project-vidoalbom', 0, 'Операция просмотра для объекта Видоальбом', NULL, 'N;'),
('view_object_project-vopros-otvet', 0, 'Операция просмотра для объекта Вопрос-ответ', NULL, 'N;'),
('view_object_ygin-gii', 0, 'Операция просмотра для объекта gii (debug=true)', NULL, 'N;'),
('view_object_ygin-override', 0, 'Операция просмотра для объекта Переопределение представлений (debug=true)', NULL, 'N;'),
('view_object_ygin-views-generator', 0, 'Операция просмотра для объекта Генерация вьюхи', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `da_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `da_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_auth_item_child`
--

INSERT INTO `da_auth_item_child` (`parent`, `child`) VALUES
('dev', 'create_object_105'),
('editor', 'create_object_105'),
('dev', 'create_object_106'),
('dev', 'create_object_20'),
('dev', 'create_object_21'),
('dev', 'create_object_22'),
('dev', 'create_object_23'),
('dev', 'create_object_24'),
('dev', 'create_object_26'),
('dev', 'create_object_260'),
('editor', 'create_object_260'),
('dev', 'create_object_261'),
('dev', 'create_object_27'),
('dev', 'create_object_28'),
('dev', 'create_object_30'),
('dev', 'create_object_31'),
('dev', 'create_object_33'),
('dev', 'create_object_34'),
('dev', 'create_object_35'),
('dev', 'create_object_47'),
('dev', 'create_object_50'),
('dev', 'create_object_500'),
('editor', 'create_object_500'),
('dev', 'create_object_501'),
('editor', 'create_object_501'),
('dev', 'create_object_502'),
('editor', 'create_object_502'),
('dev', 'create_object_503'),
('editor', 'create_object_503'),
('dev', 'create_object_505'),
('editor', 'create_object_505'),
('dev', 'create_object_506'),
('editor', 'create_object_506'),
('dev', 'create_object_507'),
('editor', 'create_object_507'),
('dev', 'create_object_508'),
('editor', 'create_object_508'),
('dev', 'create_object_509'),
('editor', 'create_object_509'),
('dev', 'create_object_51'),
('dev', 'create_object_511'),
('editor', 'create_object_511'),
('dev', 'create_object_512'),
('editor', 'create_object_512'),
('dev', 'create_object_513'),
('dev', 'create_object_514'),
('editor', 'create_object_514'),
('dev', 'create_object_517'),
('editor', 'create_object_517'),
('dev', 'create_object_520'),
('editor', 'create_object_520'),
('dev', 'create_object_521'),
('dev', 'create_object_522'),
('dev', 'create_object_523'),
('dev', 'create_object_524'),
('dev', 'create_object_525'),
('editor', 'create_object_525'),
('dev', 'create_object_529'),
('editor', 'create_object_529'),
('dev', 'create_object_530'),
('editor', 'create_object_530'),
('dev', 'create_object_61'),
('dev', 'create_object_63'),
('dev', 'create_object_66'),
('dev', 'create_object_80'),
('dev', 'create_object_86'),
('dev', 'create_object_project-bloki-kontenta'),
('editor', 'create_object_project-bloki-kontenta'),
('dev', 'create_object_project-forma-obratnoi-svyazi'),
('editor', 'create_object_project-forma-obratnoi-svyazi'),
('dev', 'create_object_project-forma-zakaza-zvonka'),
('editor', 'create_object_project-forma-zakaza-zvonka'),
('dev', 'create_object_project-fotoalbomy'),
('editor', 'create_object_project-fotoalbomy'),
('dev', 'create_object_project-kontakty'),
('dev', 'create_object_project-slaider'),
('editor', 'create_object_project-slaider'),
('dev', 'create_object_project-spezialisty'),
('editor', 'create_object_project-spezialisty'),
('dev', 'create_object_project-uslugi'),
('editor', 'create_object_project-uslugi'),
('dev', 'create_object_project-vidoalbom'),
('editor', 'create_object_project-vidoalbom'),
('dev', 'create_object_project-vopros-otvet'),
('editor', 'create_object_project-vopros-otvet'),
('dev', 'create_object_ygin-menu'),
('editor', 'create_object_ygin-menu'),
('dev', 'create_object_ygin-views-generator'),
('editor', 'create_object_ygin-views-generator'),
('dev', 'delete_object_105'),
('editor', 'delete_object_105'),
('dev', 'delete_object_106'),
('dev', 'delete_object_20'),
('dev', 'delete_object_21'),
('dev', 'delete_object_22'),
('dev', 'delete_object_23'),
('dev', 'delete_object_24'),
('dev', 'delete_object_26'),
('dev', 'delete_object_260'),
('editor', 'delete_object_260'),
('dev', 'delete_object_261'),
('dev', 'delete_object_27'),
('dev', 'delete_object_28'),
('dev', 'delete_object_30'),
('dev', 'delete_object_33'),
('dev', 'delete_object_34'),
('dev', 'delete_object_35'),
('dev', 'delete_object_47'),
('dev', 'delete_object_50'),
('dev', 'delete_object_500'),
('editor', 'delete_object_500'),
('dev', 'delete_object_501'),
('editor', 'delete_object_501'),
('dev', 'delete_object_502'),
('editor', 'delete_object_502'),
('dev', 'delete_object_503'),
('editor', 'delete_object_503'),
('dev', 'delete_object_505'),
('editor', 'delete_object_505'),
('dev', 'delete_object_506'),
('editor', 'delete_object_506'),
('dev', 'delete_object_507'),
('editor', 'delete_object_507'),
('dev', 'delete_object_508'),
('editor', 'delete_object_508'),
('dev', 'delete_object_509'),
('editor', 'delete_object_509'),
('dev', 'delete_object_51'),
('dev', 'delete_object_511'),
('editor', 'delete_object_511'),
('dev', 'delete_object_512'),
('editor', 'delete_object_512'),
('dev', 'delete_object_513'),
('dev', 'delete_object_514'),
('editor', 'delete_object_514'),
('dev', 'delete_object_517'),
('editor', 'delete_object_517'),
('dev', 'delete_object_519'),
('dev', 'delete_object_520'),
('editor', 'delete_object_520'),
('dev', 'delete_object_521'),
('dev', 'delete_object_522'),
('dev', 'delete_object_523'),
('dev', 'delete_object_524'),
('dev', 'delete_object_525'),
('editor', 'delete_object_525'),
('dev', 'delete_object_529'),
('editor', 'delete_object_529'),
('dev', 'delete_object_530'),
('editor', 'delete_object_530'),
('dev', 'delete_object_531'),
('dev', 'delete_object_61'),
('dev', 'delete_object_63'),
('dev', 'delete_object_66'),
('dev', 'delete_object_80'),
('dev', 'delete_object_86'),
('dev', 'delete_object_project-bloki-kontenta'),
('editor', 'delete_object_project-bloki-kontenta'),
('dev', 'delete_object_project-forma-obratnoi-svyazi'),
('editor', 'delete_object_project-forma-obratnoi-svyazi'),
('dev', 'delete_object_project-forma-zakaza-zvonka'),
('editor', 'delete_object_project-forma-zakaza-zvonka'),
('dev', 'delete_object_project-fotoalbomy'),
('editor', 'delete_object_project-fotoalbomy'),
('dev', 'delete_object_project-kontakty'),
('dev', 'delete_object_project-slaider'),
('editor', 'delete_object_project-slaider'),
('dev', 'delete_object_project-spezialisty'),
('editor', 'delete_object_project-spezialisty'),
('dev', 'delete_object_project-uslugi'),
('editor', 'delete_object_project-uslugi'),
('dev', 'delete_object_project-vidoalbom'),
('editor', 'delete_object_project-vidoalbom'),
('dev', 'delete_object_project-vopros-otvet'),
('editor', 'delete_object_project-vopros-otvet'),
('dev', 'delete_object_ygin-menu'),
('editor', 'delete_object_ygin-menu'),
('dev', 'delete_object_ygin-views-generator'),
('editor', 'delete_object_ygin-views-generator'),
('dev', 'editor'),
('dev', 'edit_object_105'),
('editor', 'edit_object_105'),
('dev', 'edit_object_106'),
('dev', 'edit_object_20'),
('dev', 'edit_object_21'),
('dev', 'edit_object_22'),
('dev', 'edit_object_23'),
('dev', 'edit_object_24'),
('dev', 'edit_object_26'),
('dev', 'edit_object_260'),
('editor', 'edit_object_260'),
('dev', 'edit_object_261'),
('editor', 'edit_object_261'),
('dev', 'edit_object_27'),
('dev', 'edit_object_28'),
('dev', 'edit_object_30'),
('editor', 'edit_object_30'),
('dev', 'edit_object_31'),
('dev', 'edit_object_33'),
('dev', 'edit_object_34'),
('dev', 'edit_object_35'),
('dev', 'edit_object_47'),
('dev', 'edit_object_50'),
('dev', 'edit_object_500'),
('editor', 'edit_object_500'),
('dev', 'edit_object_501'),
('editor', 'edit_object_501'),
('dev', 'edit_object_502'),
('editor', 'edit_object_502'),
('dev', 'edit_object_503'),
('editor', 'edit_object_503'),
('dev', 'edit_object_505'),
('editor', 'edit_object_505'),
('dev', 'edit_object_506'),
('editor', 'edit_object_506'),
('dev', 'edit_object_507'),
('editor', 'edit_object_507'),
('dev', 'edit_object_508'),
('editor', 'edit_object_508'),
('dev', 'edit_object_509'),
('editor', 'edit_object_509'),
('dev', 'edit_object_51'),
('dev', 'edit_object_511'),
('editor', 'edit_object_511'),
('dev', 'edit_object_512'),
('editor', 'edit_object_512'),
('dev', 'edit_object_513'),
('dev', 'edit_object_514'),
('editor', 'edit_object_514'),
('dev', 'edit_object_517'),
('editor', 'edit_object_517'),
('dev', 'edit_object_519'),
('dev', 'edit_object_520'),
('editor', 'edit_object_520'),
('dev', 'edit_object_521'),
('dev', 'edit_object_522'),
('dev', 'edit_object_523'),
('dev', 'edit_object_524'),
('dev', 'edit_object_525'),
('editor', 'edit_object_525'),
('dev', 'edit_object_529'),
('editor', 'edit_object_529'),
('dev', 'edit_object_530'),
('editor', 'edit_object_530'),
('dev', 'edit_object_531'),
('dev', 'edit_object_61'),
('dev', 'edit_object_63'),
('dev', 'edit_object_66'),
('dev', 'edit_object_80'),
('dev', 'edit_object_86'),
('dev', 'edit_object_project-bloki-kontenta'),
('editor', 'edit_object_project-bloki-kontenta'),
('dev', 'edit_object_project-forma-obratnoi-svyazi'),
('dev', 'edit_object_project-forma-zakaza-zvonka'),
('dev', 'edit_object_project-fotoalbomy'),
('editor', 'edit_object_project-fotoalbomy'),
('dev', 'edit_object_project-kontakty'),
('editor', 'edit_object_project-kontakty'),
('dev', 'edit_object_project-slaider'),
('editor', 'edit_object_project-slaider'),
('dev', 'edit_object_project-spezialisty'),
('editor', 'edit_object_project-spezialisty'),
('dev', 'edit_object_project-uslugi'),
('editor', 'edit_object_project-uslugi'),
('dev', 'edit_object_project-vidoalbom'),
('editor', 'edit_object_project-vidoalbom'),
('dev', 'edit_object_project-vopros-otvet'),
('editor', 'edit_object_project-vopros-otvet'),
('dev', 'edit_object_ygin-menu'),
('editor', 'edit_object_ygin-menu'),
('dev', 'edit_object_ygin-views-generator'),
('editor', 'edit_object_ygin-views-generator'),
('dev', 'list_object_20'),
('dev', 'list_object_21'),
('dev', 'list_object_22'),
('dev', 'list_object_23'),
('dev', 'list_object_24'),
('dev', 'list_object_26'),
('dev', 'list_object_27'),
('dev', 'list_object_28'),
('dev', 'list_object_30'),
('editor', 'list_object_30'),
('dev', 'list_object_31'),
('dev', 'list_object_32'),
('dev', 'list_object_33'),
('dev', 'list_object_34'),
('dev', 'list_object_35'),
('dev', 'list_object_43'),
('dev', 'list_object_47'),
('dev', 'list_object_50'),
('editor', 'list_object_500'),
('dev', 'list_object_501'),
('editor', 'list_object_501'),
('editor', 'list_object_502'),
('editor', 'list_object_505'),
('editor', 'list_object_506'),
('editor', 'list_object_507'),
('editor', 'list_object_508'),
('dev', 'list_object_51'),
('dev', 'list_object_513'),
('dev', 'list_object_514'),
('editor', 'list_object_514'),
('dev', 'list_object_522'),
('dev', 'list_object_523'),
('dev', 'list_object_524'),
('dev', 'list_object_528'),
('editor', 'list_object_530'),
('dev', 'list_object_531'),
('dev', 'list_object_60'),
('dev', 'list_object_61'),
('dev', 'list_object_63'),
('dev', 'list_object_66'),
('dev', 'list_object_80'),
('dev', 'list_object_86'),
('dev', 'list_object_89'),
('dev', 'list_object_91'),
('editor', 'list_object_a'),
('dev', 'list_object_project-bloki-kontenta'),
('editor', 'list_object_project-bloki-kontenta'),
('dev', 'list_object_project-forma-obratnoi-svyazi'),
('editor', 'list_object_project-forma-obratnoi-svyazi'),
('dev', 'list_object_project-forma-zakaza-zvonka'),
('editor', 'list_object_project-forma-zakaza-zvonka'),
('dev', 'list_object_project-fotoalbomy'),
('editor', 'list_object_project-fotoalbomy'),
('dev', 'list_object_project-kontakty'),
('editor', 'list_object_project-kontakty'),
('dev', 'list_object_project-slaider'),
('editor', 'list_object_project-slaider'),
('dev', 'list_object_project-spezialisty'),
('editor', 'list_object_project-spezialisty'),
('dev', 'list_object_project-uslugi'),
('editor', 'list_object_project-uslugi'),
('dev', 'list_object_project-vidoalbom'),
('editor', 'list_object_project-vidoalbom'),
('dev', 'list_object_project-vopros-otvet'),
('editor', 'list_object_project-vopros-otvet'),
('dev', 'list_object_ygin-gii'),
('dev', 'list_object_ygin-menu'),
('editor', 'list_object_ygin-menu'),
('dev', 'list_object_ygin-override'),
('dev', 'list_object_ygin-views-generator'),
('editor', 'list_object_ygin-views-generator'),
('editor', 'showAdminPanel'),
('dev', 'view_object_26'),
('editor', 'view_object_30'),
('dev', 'view_object_32'),
('dev', 'view_object_43'),
('dev', 'view_object_529'),
('editor', 'view_object_529'),
('dev', 'view_object_531'),
('dev', 'view_object_60'),
('dev', 'view_object_89'),
('dev', 'view_object_91'),
('dev', 'view_object_project-bloki-kontenta'),
('editor', 'view_object_project-bloki-kontenta'),
('dev', 'view_object_project-forma-obratnoi-svyazi'),
('editor', 'view_object_project-forma-obratnoi-svyazi'),
('dev', 'view_object_project-forma-zakaza-zvonka'),
('editor', 'view_object_project-forma-zakaza-zvonka'),
('dev', 'view_object_project-fotoalbomy'),
('editor', 'view_object_project-fotoalbomy'),
('dev', 'view_object_project-kontakty'),
('editor', 'view_object_project-kontakty'),
('dev', 'view_object_project-slaider'),
('editor', 'view_object_project-slaider'),
('dev', 'view_object_project-spezialisty'),
('editor', 'view_object_project-spezialisty'),
('dev', 'view_object_project-uslugi'),
('editor', 'view_object_project-uslugi'),
('dev', 'view_object_project-vidoalbom'),
('editor', 'view_object_project-vidoalbom'),
('dev', 'view_object_project-vopros-otvet'),
('editor', 'view_object_project-vopros-otvet'),
('dev', 'view_object_ygin-gii'),
('dev', 'view_object_ygin-override'),
('dev', 'view_object_ygin-views-generator'),
('editor', 'view_object_ygin-views-generator');

-- --------------------------------------------------------

--
-- Структура таблицы `da_domain`
--

CREATE TABLE IF NOT EXISTS `da_domain` (
  `id_domain` int(8) NOT NULL COMMENT 'id',
  `domain_path` varchar(255) DEFAULT NULL COMMENT 'Путь к содержимому домена',
  `name` varchar(255) NOT NULL COMMENT 'Доменное имя',
  `id_default_page` int(8) NOT NULL COMMENT 'ID страницы по умолчанию',
  `description` varchar(255) DEFAULT NULL COMMENT 'Описание',
  `path2data_http` varchar(255) DEFAULT NULL COMMENT 'Путь к данным по http',
  `settings` text COMMENT 'Настройки',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'Ключевые слова',
  `image_src` int(11) DEFAULT NULL COMMENT 'Картинка для сохранения в закладках',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активен',
  PRIMARY KEY (`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Домены сайта';

--
-- Дамп данных таблицы `da_domain`
--

INSERT INTO `da_domain` (`id_domain`, `domain_path`, `name`, `id_default_page`, `description`, `path2data_http`, `settings`, `keywords`, `image_src`, `active`) VALUES
(1, NULL, 'test', 100, 'Медицинский центр Сантэ', NULL, '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_domain_localization`
--

CREATE TABLE IF NOT EXISTS `da_domain_localization` (
  `id_domain` int(8) NOT NULL,
  `id_localization` int(8) NOT NULL,
  PRIMARY KEY (`id_domain`,`id_localization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_domain_localization`
--

INSERT INTO `da_domain_localization` (`id_domain`, `id_localization`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_event`
--

CREATE TABLE IF NOT EXISTS `da_event` (
  `id_instance` int(8) DEFAULT NULL COMMENT 'ИД экземпляра',
  `id_event` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_event_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип события',
  `event_message` longtext COMMENT 'Содержимое',
  `event_create` int(8) NOT NULL DEFAULT '0' COMMENT 'Дата создания события',
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='События' AUTO_INCREMENT=43 ;

--
-- Дамп данных таблицы `da_event`
--

INSERT INTO `da_event` (`id_instance`, `id_event`, `id_event_type`, `event_message`, `event_create`) VALUES
(NULL, 1, 51, '<strong>Имя: </strong>gh<br><strong>Телефон: </strong>fgdh<br>', 1417094614),
(NULL, 2, 51, '<strong>Имя: </strong>gh<br><strong>Телефон: </strong>fgdh<br>', 1417095389),
(NULL, 3, 52, '<strong>Имя: </strong>ergr<br><strong>Телефон: </strong>345<br><strong>Email: </strong>erg@gr.rr<br><strong>Текст сообщения: </strong><br>htrht<br>', 1417095462),
(NULL, 4, 51, '<strong>Имя: </strong>gh<br><strong>Телефон: </strong>fgdh<br>', 1417096216),
(NULL, 5, 51, '<strong>Имя: </strong>gh<br><strong>Телефон: </strong>fgdh<br>', 1417096730),
(NULL, 6, 52, '<strong>Имя: </strong>пшд<br><strong>Телефон: </strong>пгшд<br><strong>Email: </strong>Serg@dfsg.erg<br><strong>Текст сообщения: </strong><br>awrgr<br>', 1417096878),
(NULL, 7, 52, '<strong>Имя: </strong>5t<br><strong>Телефон: </strong>w45<br><strong>Email: </strong>w45t@gg.rr<br><strong>Текст сообщения: </strong><br>rr<br>', 1417096996),
(NULL, 8, 52, '<strong>Имя: </strong>rg<br><strong>Телефон: </strong>ear<br><strong>Email: </strong>erg@gr.rr<br><strong>Текст сообщения: </strong><br>erger<br>', 1417097152),
(NULL, 9, 51, '<strong>Имя: </strong>ere<br><strong>Телефон: </strong>er<br>', 1417097416),
(NULL, 10, 51, '<strong>Имя: </strong>ere<br><strong>Телефон: </strong>er<br>', 1417097743),
(NULL, 11, 51, '<strong>Имя: </strong>jjh,v<br><strong>Телефон: </strong>hj,hj<br>', 1417097765),
(NULL, 12, 51, '<strong>Имя: </strong>цц<br><strong>Телефон: </strong>цц<br>', 1417152465),
(NULL, 13, 51, '<strong>Имя: </strong>sth<br><strong>Телефон: </strong>ghfg<br>', 1417152571),
(NULL, 14, 51, '<strong>Имя: </strong>regswer<br><strong>Телефон: </strong>gsergser<br>', 1419341389),
(NULL, 15, 51, '<strong>Имя: </strong>aergse<br><strong>Телефон: </strong>ergser<br>', 1419341553),
(NULL, 16, 51, '<strong>Имя: </strong>strh<br><strong>Телефон: </strong>srth<br>', 1419341902),
(NULL, 17, 51, '<strong>Имя: </strong>wefa<br><strong>Телефон: </strong>awef<br>', 1419342152),
(NULL, 18, 51, 'cjj,otybt jn', 1419342950),
(NULL, 19, 51, 'cjj,otybt jn', 1419342968),
(NULL, 20, 51, 'cjj,otybt jn', 1419343045),
(NULL, 21, 51, '<strong>Имя: </strong>керу<br><strong>Телефон: </strong>кервуке<br>', 1419343090),
(NULL, 22, 51, '<strong>Имя: </strong>укук<br><strong>Телефон: </strong>укук<br>', 1419343232),
(NULL, 23, 51, '<strong>Имя: </strong>укук<br><strong>Телефон: </strong>укук<br>', 1419343319),
(NULL, 24, 51, '<strong>Имя: </strong>укук<br><strong>Телефон: </strong>укук<br>', 1419344180),
(NULL, 25, 51, '<strong>Имя: </strong>куцпфику<br><strong>Телефон: </strong>укп<br>', 1419344274),
(NULL, 26, 51, '<strong>Имя: </strong>цер<br><strong>Телефон: </strong>ыкервыке<br>', 1419344304),
(NULL, 27, 51, '<strong>Имя: </strong>56уо<br><strong>Телефон: </strong>оу56о<br>', 1419344355),
(NULL, 28, 51, '<strong>Имя: </strong>ц45р<br><strong>Телефон: </strong>4ц5рц45<br>', 1419344406),
(NULL, 29, 51, '<strong>Имя: </strong>5пц45<br><strong>Телефон: </strong>ц45пц4<br>', 1419344477),
(NULL, 30, 51, '<strong>Имя: </strong>кервуек<br><strong>Телефон: </strong>вкервк<br>', 1419344547),
(NULL, 31, 53, 'Был оставлен новый вопрос, для ответа перейдите в административную панель сайта<strong>Отправитель: </strong>Денис<br><strong>Email: </strong>aff@ferg.er<br><strong>Тема вопроса: </strong>Тема<br><strong>Вопрос: </strong><br>Вопрос<br>', 1419407078),
(NULL, 32, 54, 'yes', 1419408672),
(NULL, 33, 54, 'На ваш вопрос был получен ответ<strong>Вопрос: </strong><br>Вопрос<br><strong>Ответ: </strong><br>zdfz<br>', 1419410829),
(NULL, 34, 54, 'На ваш вопрос был получен ответ<strong>Вопрос: </strong><br>Вопрос<br><strong>Ответ: </strong><br><br>', 1419410935),
(NULL, 35, 54, 'На ваш вопрос был получен ответ<strong>Вопрос: </strong><br>Вопрос<br><strong>Ответ: </strong><br>ergser<br>', 1419411044),
(NULL, 36, 53, 'Был оставлен новый вопрос, для ответа перейдите в административную панель сайта<strong>Отправитель: </strong>Ден<br><strong>Email: </strong>er@er.er<br><strong>Тема вопроса: </strong>ntvf<br><strong>Вопрос: </strong><br>argaerg<br>', 1419416965),
(NULL, 37, 53, 'Был оставлен новый вопрос, для ответа перейдите в административную панель сайта<strong>Кому адресовано: </strong>Данилова Любовь Валерьевна<br><strong>Отправитель: </strong>Денис<br><strong>Email: </strong>qw@qw.qw<br><strong>Тема вопроса: </strong>Тема вопроса<br><strong>Вопрос: </strong><br>Здравствуйте! Подскажите пожалуйста. У меня уже третий месяц растет живот, я беременна или у меня просто газы? Можно ли записаться к вам на узи<br>', 1419422744),
(NULL, 38, 53, 'Был оставлен новый вопрос, для ответа перейдите в административную панель сайта<strong>Кому адресовано: </strong>Данилова Любовь Валерьевна<br><strong>Отправитель: </strong>sfzbvfb<br><strong>Email: </strong>zdfb@aer.ergs<br><strong>Тема вопроса: </strong>ergser<br><strong>Вопрос: </strong><br>serg<br>', 1419422950),
(NULL, 39, 53, 'Был оставлен новый вопрос, для ответа перейдите в административную панель сайта<strong>Кому адресовано: </strong>Данилова Любовь Валерьевна<br><strong>Отправитель: </strong>sbs<br><strong>Email: </strong>dsfb@arg.er<br><strong>Тема вопроса: </strong>aergser<br><strong>Вопрос: </strong><br>gsergser<br>', 1419422977),
(NULL, 42, 52, '<strong>Имя: </strong>sd<br><strong>Телефон: </strong>erg<br><strong>Email: </strong>esrg@tg.ee<br><strong>Текст сообщения: </strong><br>hsrthdrth<br>', 1419515048);

-- --------------------------------------------------------

--
-- Структура таблицы `da_event_format`
--

CREATE TABLE IF NOT EXISTS `da_event_format` (
  `id_event_format` int(8) NOT NULL COMMENT 'id',
  `description` varchar(255) NOT NULL COMMENT 'Описание',
  `place` int(1) NOT NULL DEFAULT '1' COMMENT 'Расположение',
  `file_name` varchar(50) DEFAULT NULL COMMENT 'Имя файла во вложении',
  `name` varchar(20) NOT NULL COMMENT 'Сокращённое название (для разработчика)',
  PRIMARY KEY (`id_event_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Формат сообщения';

--
-- Дамп данных таблицы `da_event_format`
--

INSERT INTO `da_event_format` (`id_event_format`, `description`, `place`, `file_name`, `name`) VALUES
(1, 'Текстовое содержимое', 1, 'text.txt', 'TXT'),
(2, 'HTML-письмо', 1, NULL, 'HTML');

-- --------------------------------------------------------

--
-- Структура таблицы `da_event_process`
--

CREATE TABLE IF NOT EXISTS `da_event_process` (
  `id_event` int(8) NOT NULL DEFAULT '0',
  `email` varchar(70) NOT NULL,
  `notify_date` int(10) DEFAULT NULL,
  `id_event_subscriber` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_event`,`email`,`id_event_subscriber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_event_process`
--

INSERT INTO `da_event_process` (`id_event`, `email`, `notify_date`, `id_event_subscriber`) VALUES
(1, 'krvdns@mail.ru', 1409725974, 1),
(2, 'krvdns@mail.ru', 1409726333, 1),
(3, 'krvdns@mail.ru', 1409726657, 1),
(3, 'krvdns@mail.ru', 1417095463, 2),
(4, 'krvdns@mail.ru', 1409727670, 1),
(5, 'krvdns@mail.ru', 1409727901, 1),
(6, 'krvdns@mail.ru', 1409727912, 1),
(6, 'krvdns@mail.ru', 1417096878, 2),
(7, 'krvdns@mail.ru', 1409727933, 1),
(7, 'krvdns@mail.ru', 1417096997, 2),
(8, 'krvdns@mail.ru', 1409728016, 1),
(8, 'krvdns@mail.ru', 1417097153, 2),
(9, 'krvdns@mail.ru', 1409728027, 1),
(10, 'krvdns@mail.ru', 1409728047, 1),
(11, 'krvdns@mail.ru', 1409728216, 1),
(12, 'krvdns@mail.ru', 1417152466, 1),
(12, 'krvdns@mail.ru', 1409746949, 2),
(13, 'krvdns@mail.ru', 1417152571, 1),
(13, 'krvdns@mail.ru', 1409749854, 2),
(14, 'krvdns@mail.ru', 1419341390, 1),
(14, 'krvdns@mail.ru', 1409750636, 2),
(15, 'krvdns@mail.ru', 1419341553, 1),
(15, 'krvdns@mail.ru', 1409750767, 2),
(16, 'krvdns@mail.ru', 1419341902, 1),
(16, 'krvdns@mail.ru', 1409750863, 2),
(17, 'krvdns@mail.ru', 1419342153, 1),
(17, 'krvdns@mail.ru', 1411029608, 3),
(18, 'krvdns@mail.ru', 1411048683, 3),
(19, 'krvdns@mail.ru', 1411132494, 4),
(20, 'krvdns@mail.ru', 1411132931, 4),
(21, 'krvdns@mail.ru', 1411976600, 5),
(22, 'krvdns@mail.ru', 1411998061, 6),
(23, 'krvdns@mail.ru', NULL, 3),
(23, 'krvdns@mail.ru', 1411998278, 6),
(24, 'krvdns@mail.ru', 1419344180, 3),
(24, 'krvdns@mail.ru', 1411999020, 6),
(25, 'agr@sfdg.er', 1419344275, 3),
(25, 'krvdns@mail.ru', NULL, 1),
(26, 'agr@sfdg.er', 1419344304, 3),
(26, 'krvdns@mail.ru', 1412234056, 1),
(27, 'agr@sfdg.er', 1419344355, 3),
(27, 'krvdns@mail.ru', 1412334968, 1),
(28, 'agr@sfdg.er', 1419344406, 3),
(29, 'krvdns@mail.ru', 1419344478, 3),
(30, 'agr@sfdg.er', 1419344548, 4),
(31, 'krvdns@mail.ru', 1419407079, 5),
(32, 'aff@ferg.er', 1419408672, 4),
(33, 'aff@ferg.er', 1419410830, 4),
(34, 'aff@ferg.er', 1419410935, 4),
(35, 'aff@ferg.er', 1419411044, 4),
(36, 'krvdns@mail.ru', 1419416966, 5),
(37, 'krvdns@mail.ru', 1419422744, 5),
(38, 'krvdns@mail.ru', 1419422950, 5),
(39, 'krvdns@mail.ru', 1419422978, 5),
(42, 'krvdns@mail.ru', 1419515050, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `da_event_subscriber`
--

CREATE TABLE IF NOT EXISTS `da_event_subscriber` (
  `id_event_subscriber` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_event_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип события',
  `id_user` int(8) DEFAULT NULL COMMENT 'Пользователь',
  `format` int(2) NOT NULL COMMENT 'Формат сообщения',
  `archive_attach` int(1) NOT NULL DEFAULT '0' COMMENT 'Архивировать ли вложение',
  `email` varchar(60) DEFAULT NULL COMMENT 'E-mail адрес',
  `name` varchar(255) DEFAULT NULL COMMENT 'Имя подписчика',
  PRIMARY KEY (`id_event_subscriber`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Подписчики на события' AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `da_event_subscriber`
--

INSERT INTO `da_event_subscriber` (`id_event_subscriber`, `id_event_type`, `id_user`, `format`, `archive_attach`, `email`, `name`) VALUES
(2, 52, NULL, 2, 0, 'krvdns@mail.ru', 'Денис'),
(3, 51, NULL, 2, 0, 'krvdns@mail.ru', 'Денис'),
(4, 54, NULL, 2, 0, NULL, NULL),
(5, 53, NULL, 2, 0, 'krvdns@mail.ru', 'Денис');

-- --------------------------------------------------------

--
-- Структура таблицы `da_event_type`
--

CREATE TABLE IF NOT EXISTS `da_event_type` (
  `id_event_type` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'Объект для работы',
  `last_time` int(10) DEFAULT NULL COMMENT 'Дата последей обработки',
  `interval_value` int(8) DEFAULT NULL COMMENT 'Интервал времени, через которое будет проходить обработка события',
  `sql_condition` varchar(255) DEFAULT NULL COMMENT 'SQL получающая экемпляры ("AS id_instance")',
  `name` varchar(100) NOT NULL COMMENT 'Название',
  `condition_done` varchar(255) DEFAULT NULL COMMENT 'SQL выражение, срабатывающее после обработки экземпляра (<<id_instance>>)',
  `id_mail_account` int(8) NOT NULL DEFAULT '0' COMMENT 'Используемый почтовый аккаунт',
  PRIMARY KEY (`id_event_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Тип события' AUTO_INCREMENT=57 ;

--
-- Дамп данных таблицы `da_event_type`
--

INSERT INTO `da_event_type` (`id_event_type`, `id_object`, `last_time`, `interval_value`, `sql_condition`, `name`, `condition_done`, `id_mail_account`) VALUES
(50, NULL, 1344593834, 300, NULL, 'Новый комментарий', NULL, 10),
(51, '', NULL, 90, NULL, 'Письмо заказа звонка', NULL, 0),
(52, '', NULL, 90, NULL, 'Письмо с формы обратной связи', NULL, 0),
(53, '', NULL, 90, NULL, 'Новый вопрос', NULL, 0),
(54, '', NULL, 90, NULL, 'Ответ на вопрос', NULL, 0),
(56, NULL, NULL, 90, NULL, 'Новый отзыв', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `da_files`
--

CREATE TABLE IF NOT EXISTS `da_files` (
  `id_file` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `file_path` varchar(255) NOT NULL COMMENT 'Путь к файлу',
  `id_file_type` int(8) DEFAULT NULL COMMENT 'Тип файла',
  `count` int(6) DEFAULT NULL COMMENT 'Количество загрузок',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'Объект',
  `id_instance` int(8) DEFAULT NULL COMMENT 'ИД экземпляра',
  `id_parameter` varchar(255) DEFAULT NULL COMMENT 'Свойство объекта',
  `id_property` int(8) DEFAULT NULL COMMENT 'Пользовательское свойство',
  `create_date` int(10) DEFAULT NULL COMMENT 'Дата создания файла',
  `id_parent_file` int(8) DEFAULT NULL COMMENT 'Родительский файл',
  `id_tmp` varchar(32) DEFAULT NULL COMMENT 'Временный ИД',
  `status_process` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус создания превью-файла',
  PRIMARY KEY (`id_file`),
  KEY `id_tmp` (`id_tmp`,`id_object`,`id_instance`,`id_parameter`,`id_parent_file`,`id_file_type`,`file_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Файлы' AUTO_INCREMENT=38 ;

--
-- Дамп данных таблицы `da_files`
--

INSERT INTO `da_files` (`id_file`, `file_path`, `id_file_type`, `count`, `id_object`, `id_instance`, `id_parameter`, `id_property`, `create_date`, `id_parent_file`, `id_tmp`, `status_process`) VALUES
(1, 'content/slider/1/65d3c9f0b23351fb959a5689de1cc101b42568cc.jpg', 1, NULL, 'project-slaider', 1, 'project-slaider-image', NULL, 1419248921, NULL, NULL, 0),
(9, 'content/slider/1/65d3c9f0b23351fb959a5689de1cc101b42568cc_da.jpg', 1, NULL, 'project-slaider', 1, 'project-slaider-image', NULL, 1419248921, 1, NULL, 0),
(10, 'content/slider/1/65d3c9f0b23351fb959a5689de1cc101b42568cc_slider.jpg', 1, NULL, 'project-slaider', 1, 'project-slaider-image', NULL, 1419248926, 1, NULL, 0),
(11, 'content/slider/2/7bffd74978271a6a98874ab41208099f0bf05272.jpg', 1, NULL, 'project-slaider', 2, 'project-slaider-image', NULL, 1419249616, NULL, NULL, 0),
(14, 'content/slider/2/7bffd74978271a6a98874ab41208099f0bf05272_da.jpg', 1, NULL, 'project-slaider', 2, 'project-slaider-image', NULL, 1419249619, 11, NULL, 0),
(15, 'content/slider/2/7bffd74978271a6a98874ab41208099f0bf05272_slider.jpg', 1, NULL, 'project-slaider', 2, 'project-slaider-image', NULL, 1419249660, 11, NULL, 0),
(16, 'content/services/1/feb6c52aa3fe3cd21d8800e857bab4e74a060fe6.jpg', 1, NULL, 'project-uslugi', 1, 'project-uslugi-image', NULL, 1419321515, NULL, NULL, 0),
(19, 'content/services/1/feb6c52aa3fe3cd21d8800e857bab4e74a060fe6_da.jpg', 1, NULL, 'project-uslugi', 1, 'project-uslugi-image', NULL, 1419321515, 16, NULL, 0),
(20, 'content/services/2/b0cfd2a347c46eb045403edf0751cc5bab82c0f1.jpg', 1, NULL, 'project-uslugi', 2, 'project-uslugi-image', NULL, 1419321619, NULL, NULL, 0),
(22, '/content/services/3/27bb36f4f7ea4c96020b53acda191f7e3499f7bc.jpg', 1, NULL, 'project-uslugi', 3, 'project-uslugi-image', NULL, 1419321592, NULL, NULL, 0),
(24, '/content/services/3/27bb36f4f7ea4c96020b53acda191f7e3499f7bc_da.jpg', 1, NULL, 'project-uslugi', 3, 'project-uslugi-image', NULL, 1419321592, 22, NULL, 0),
(25, 'content/services/2/b0cfd2a347c46eb045403edf0751cc5bab82c0f1_da.jpg', 1, NULL, 'project-uslugi', 2, 'project-uslugi-image', NULL, 1419321619, 20, NULL, 0),
(26, 'content/services/1/feb6c52aa3fe3cd21d8800e857bab4e74a060fe6_widget.jpg', 1, NULL, 'project-uslugi', 1, 'project-uslugi-image', NULL, 1419321630, 16, NULL, 0),
(27, 'content/services/2/b0cfd2a347c46eb045403edf0751cc5bab82c0f1_widget.jpg', 1, NULL, 'project-uslugi', 2, 'project-uslugi-image', NULL, 1419321630, 20, NULL, 0),
(28, 'content/services/3/27bb36f4f7ea4c96020b53acda191f7e3499f7bc_widget.jpg', 1, NULL, 'project-uslugi', 3, 'project-uslugi-image', NULL, 1419321630, 22, NULL, 0),
(29, '/content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419331355, NULL, NULL, 0),
(30, '/content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7_da.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419331355, 29, NULL, 0),
(31, 'content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7_avatar_gs.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419331468, 29, NULL, 0),
(32, 'content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7_avatar_gs.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419331778, 29, NULL, 0),
(33, 'content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7_da.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419331789, 29, NULL, 0),
(34, 'content/specialist/1/c2e34151719db70ca855bc3ecb0b9fdf9a9f51b7_avatar_widget.jpg', 1, NULL, 'project-spezialisty', 1, 'project-spezialisty-avatar', NULL, 1419338231, 29, NULL, 0),
(35, 'content/news/1/c926b1c0a893d934980298a0e389e81556b0d9e8.jpg', 1, NULL, '502', 1, '1519', NULL, 1419493516, NULL, NULL, 0),
(36, 'content/news/1/c926b1c0a893d934980298a0e389e81556b0d9e8_da.jpg', 1, NULL, '502', 1, '1519', NULL, 1419493516, 35, NULL, 0),
(37, 'content/news/1/c926b1c0a893d934980298a0e389e81556b0d9e8_list.jpg', 1, NULL, '502', 1, '1519', NULL, 1419493548, 35, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `da_file_extension`
--

CREATE TABLE IF NOT EXISTS `da_file_extension` (
  `id_file_extension` int(8) NOT NULL COMMENT 'id',
  `ext` varchar(20) NOT NULL COMMENT 'Расширение',
  `id_file_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип файла',
  PRIMARY KEY (`id_file_extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Расширения файлов';

--
-- Дамп данных таблицы `da_file_extension`
--

INSERT INTO `da_file_extension` (`id_file_extension`, `ext`, `id_file_type`) VALUES
(1, 'jpg', 1),
(2, 'gif', 1),
(3, 'png', 1),
(4, 'doc', 2),
(5, 'xls', 2),
(6, 'txt', 2),
(9, 'css', 4),
(11, 'pdf', 2),
(12, 'docx', 2),
(13, 'jpeg', 1),
(14, 'flv', 5),
(201, 'bmp', 1),
(202, 'xlsx', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `da_file_type`
--

CREATE TABLE IF NOT EXISTS `da_file_type` (
  `id_file_type` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id_file_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы файлов';

--
-- Дамп данных таблицы `da_file_type`
--

INSERT INTO `da_file_type` (`id_file_type`, `name`) VALUES
(1, 'Картинка'),
(2, 'Документ'),
(4, 'Стили'),
(5, 'Flash-видео');

-- --------------------------------------------------------

--
-- Структура таблицы `da_group_system_parameter`
--

CREATE TABLE IF NOT EXISTS `da_group_system_parameter` (
  `id_group_system_parameter` int(8) NOT NULL COMMENT 'id',
  `name` varchar(100) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id_group_system_parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Группы системных параметров';

--
-- Дамп данных таблицы `da_group_system_parameter`
--

INSERT INTO `da_group_system_parameter` (`id_group_system_parameter`, `name`) VALUES
(1, 'Системные настройки'),
(2, 'Настройки сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `da_instruction`
--

CREATE TABLE IF NOT EXISTS `da_instruction` (
  `id_instruction` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `content` longtext NOT NULL COMMENT 'Описание',
  `desc_type` int(1) NOT NULL DEFAULT '1' COMMENT 'Относится только к этому сайту',
  `visible` int(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `num_seq` int(3) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_instruction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Инструкции';

--
-- Дамп данных таблицы `da_instruction`
--

INSERT INTO `da_instruction` (`id_instruction`, `name`, `content`, `desc_type`, `visible`, `num_seq`) VALUES
(6, 'Модуль Вопрос-ответ', '<ol>  <li>  <h2>Общий вид</h2>  <img style="float: left;" src="http://ygin.ru/instruction/engine/faq/faq-vid.jpg" alt=""></li>  <li>  <h2>Создание</h2>  <p>При нажатии на кнопку «Создать» появится страница с указанными полями:</p>  <ol style="list-style-type: lower-alpha;">  <li>  <p>Поле «Вопрос» отображает вопрос пришедший от пользователей на сайт.</p>  <img src="http://ygin.ru/instruction/engine/faq/faq-sozd-1.jpg" alt="Вопрос"></li>  <li>  <p>Поле «Ответ» позволяет написать ответ по вышеуказанному вопросу.</p>  <img src="http://ygin.ru/instruction/engine/faq/faq-sozd-2.jpg" alt="Ответ"></li>  <li>  <p>Поле «Дата» указывает дату подачи вопроса.</p>  <img src="http://ygin.ru/instruction/engine/faq/faq-sozd-3.jpg" alt="Дата"></li>  <li>  <p>Поле «Показать» отвечает за видимость данного вопроса и ответа для пользователей сайта. В любом случае, вопрос останется видимым в списке вопросов в панели администрирования. Как правило, данная галочка предназначена для того, чтобы скрыть нежелательные или не прошедшие модерацию вопросы.</p>  <img src="http://ygin.ru/instruction/engine/faq/faq-sozd-4.jpg" alt="Видимость"></li>  </ol></li>  </ol>', 0, 0, 18),
(11, 'Размещение видеороликов с Rutube', '<ol>  <li>Чтобы добавить видеоролик в раздел сайта, его необходимо загрузить на любой сервис, предоставляющий услуги хостинга видеоматериалов, например <a href="http://www.rutube.ru/">http://rutube.ru/register.html</a>.</li>  <li>Для загрузки видеороликов на сайте <a href="http://www.rutube.ru/">www.rutube.ru</a> необходимо зарегистрироваться и войти на сайт как авторизованный пользователь.</li>  <li>В меню пользователя появится возможность загрузки видеороликов.<br><span><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/3.png'';" src="http://ygin.ru/instruction/engine/rutube/3s.png" alt=""></span> <br><br><br><br></li>  <li>Для загрузки видеоролика нужно указать к нему путь, нажав кнопку «Обзор». Заполнить все необходимые поля и нажать на кнопку «Загрузить файл»<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/4.png'';" src="http://ygin.ru/instruction/engine/rutube/4s.png" alt=""><br><br><br><br></li>  <li>После загрузки видеоролика необходимо зайти в раздел «мои ролики»<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/5.png'';" src="http://ygin.ru/instruction/engine/rutube/5s.png" alt=""><br><br><br><br></li>  <li>Выбрать свой загруженный ролик, нажать на вкладку «ссылка и код» и скопировать ссылку на видеоролик.<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/6.png'';" src="http://ygin.ru/instruction/engine/rutube/6s.png" alt=""><br><br><br><br></li>  <li>После копирования ссылки, нужно зайти в Систему администрирования сайта, открыть редактируемый раздел где необходимо вставить видеоролик, выделить курсором мыши место расположения видеоролика в содержании раздела и нажать кнопку «Вставить/редактировать медиа-объект»<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/7.png'';" src="http://ygin.ru/instruction/engine/rutube/7s.png" alt=""><br><br><br><br></li>  <li>В сплывающем окне, в поле «Файл/адрес» нужно вставить скопированную ссылку на видеоролик, при необходимости отредактировать размеры и нажать кнопку «Вставить»<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/8.png'';" src="http://ygin.ru/instruction/engine/rutube/8s.png" alt=""><br><br><br><br></li>  <li>После чего в редактируемом разделе появится вставленный видеоролик.<br> <img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/rutube/9.png'';" src="http://ygin.ru/instruction/engine/rutube/9s.png" alt=""><br><br><br><br></li>  </ol>', 0, 1, 19),
(12, 'Размещение видеороликов с Youtube', '<ol>  <li class="noindent">Чтобы добавить видеоролик в раздел сайта, его необходимо загрузить на любой сервис, предоставляющий услуги хостинга видеоматериалов, например http://www.youtube.com/create_account?next=%2F</li>  <li>Для загрузки видеороликов на сайте www.youtube.ru необходимо зарегистрироваться и войти на сайт как авторизованный пользователь.</li>  <li>В меню пользователя появится возможность загрузки видеороликов.<br><br><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/youtube/3.png'';" src="http://ygin.ru/instruction/engine/youtube/3s.png" alt=""><br><br></li>  <li>Для загрузки видеоролика нужно указать к нему путь, нажав кнопку «Обзор» и после чего начнется загрузка файла.</li>  <li>По окончании загрузки нужно скопировать ссылку на видеоролик.<br><br><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/youtube/5.png'';" src="http://ygin.ru/instruction/engine/youtube/5s.png" alt=""><br><br></li>  <li>После копирования ссылки, нужно зайти в Систему администрирования сайта, открыть редактируемый раздел где необходимо вставить видеоролик, выделить курсором мыши место расположения видеоролика в содержании раздела и нажать кнопку «Вставить/редактировать медиа-объект»<br><br><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/youtube/6.png'';" src="http://ygin.ru/instruction/engine/youtube/6s.png" alt=""><br><br></li>  <li>В сплывающем окне, в поле «Файл/адрес» нужно вставить скопированную ссылку на видеоролик, при необходимости отредактировать размеры и нажать кнопку «Вставить»<br><br><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/youtube/7.png'';" src="http://ygin.ru/instruction/engine/youtube/7s.png" alt=""><br><br></li>  <li>После чего в редактируемом разделе появится вставленный видеоролик.<br><br><img style="margin-top: 15px; cursor: pointer;" onclick="this.src=''http://ygin.ru/instruction/engine/youtube/8.png'';" src="http://ygin.ru/instruction/engine/youtube/8s.png" alt=""><br><br></li>  </ol>', 0, 1, 20),
(13, 'Основные возможности', '<p>Интернет-сайт разработан с использованием системы управления сайтами <strong>ygin</strong>. В состав ее программного обеспечения входят:</p>  <ul>  <li>система взаимодействия с пользователем, реализующая графический оконный интерфейс;</li>  <li>система администрирования, обеспечивающая управление, ввод и  редактирование информации, используемой в процессе функционирования Интернет-сайта.</li>  </ul>  <p>Объектами автоматизации являются процессы вывода периодически изменяющейся информации, отправки сообщений, содержащих вопросы и предложения по различным проблемным областям. Основными субъектами информационного взаимодействия, реализуемого программным обеспечением Интернет-сайта, являются:</p>  <ol>  <li>посетители сайта, получающие доступ в открытые разделы;</li>  <li>редакторы сайта;</li>  <li>администраторы сайта.</li>  </ol>  <p> </p>  <ul></ul>', 0, 1, 1),
(14, 'Авторизация в системе', '<p>Для того чтобы начать работу в системе администрирования, потребуется выполнить ряд действий:</p>  <ol>  <li>Вход в систему администрирования начинается с ввода в адресной строке браузера особого адреса, который скрыт от пользователей. Для того чтобы попасть на скрытую страницу, необходимо ввести: http://название_сайта/admin/ , после чего откроется форма авторизации.  <p><img src="http://ygin.ru/instruction/engine/auto/clip_image001_0006.gif" alt="" height="204" width="274"><br><em>Авторизация в системе</em></p>  </li>  <li>В форму авторизации необходимо ввести логин и пароль, которые Вам сообщаются заранее администратором сайта.</li>  <li>В случае удачного входа, перед вами откроется основное меню с доступными для Вашего пользователя элементами.</li>  </ol>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/auto/clip_image003_0001.jpg" alt="" height="351" width="623"><br> <em>Главная страница системы управления сайтом</em></p>', 0, 1, 2),
(15, 'Структура системы управления', '<p>Раздел – структурная единица сайта, которая предназначена для выделения или объединения информации по какому-либо признаку. На сайте он представлен в виде странички с информацией. Раздел может иметь подразделы, которые в свою очередь могут иметь свои подразделы. Данное свойство называется вложенностью раздела.</p>  <p>Каталог разделов (папка) – раздел сайта, содержащий в себе другие разделы. <br> Обычно система каталогов имеет иерархическую структуру: каталог разделов (папка) может дополнительно содержать каталог более низкого уровня (подкаталог).</p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/strukt/clip_image002_0009.jpg" alt="" height="463" width="623"></p>  <p style="text-align: center;">Структура панели управления</p>  <p>Основное меню – главный элемент навигации по панели администрирования (см. область 1), разбит на списки экземпляров, каждый из которых именуется согласно задачам, которые можно решить. Например, внутри раздела «Меню» находится инструментарий, предназначенный для редактирования основного содержимого сайта, то есть структуры меню.</p>  <p>Панель управления – набор кнопок, предназначенных для выполнения различных операций над элементами списка (см. область 2). Например, на изображении выше можно увидеть две кнопки – «Создать» и «Упорядочить», соответственно добавляющую новый элемент в список и упорядочивающую элементы списка.</p>  <p>Основное содержание – внутри этого блока находится основное содержимое страницы администрирования (списки экземпляров объектов или свойства редактируемого объекта), на изображении выше присутствует список, с помощью которого редактируется меню (см. область 3).</p>  <p><strong>Рассмотрим более подробно список экземпляров в основном содержании:</strong></p>  <ul>  <li>ИД – уникальный идентификационный номер, предназначенный для однозначного определения раздела системой управления сайтом, т.е. одному ИД всегда соответствует только один экземпляр списка (экземпляром списка может быть, например, раздел, новость и т.д.).</li>  <li>Позиция раздела – порядковый номер отображения раздела в меню сайта. Чем меньше номер, тем выше отображается данный раздел.</li>  <li>Видимость раздела – если галочка стоит, то этот раздел будет виден в меню. В противном случае раздел не будет виден в меню сайта, но при прямом обращении по ссылке в адресной строке браузера он, по-прежнему, доступен.</li>  <li>Просмотр вложенных объектов – нажав на данную кнопку, можно просмотреть все вложенные разделы. Даже при отсутствии таковых существует возможность перейти к вложенным разделам и создать их при необходимости.</li>  <li>Редактировать – при нажатии на кнопку открывается список свойств элемента списка, который позволяет, например, менять заголовок и текст разделов или дату новостей.</li>  <li>Удалить – удаляет указанный элемент списка без возможности восстановления.</li>  </ul>', 0, 1, 3),
(16, 'Переключение локализации данных (языков)', '<p>С помощью блока управления языками, у редактора есть возможность переключить текущую локализацию данных. При этом весь последующий ввод данных будет применим к выбранной локализации данных.</p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/lokal/clip_image002_0009.jpg" alt="" height="29" width="134"></p>  <p style="text-align: center;"><em>Блок работы с локализацией</em></p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/lokal/clip_image004_0000.jpg" alt="" height="322" width="661"></p>  <p style="text-align: center;"><em>Список разделов при работе с английской локализацией данных</em></p>  <p>Для удобства восприятия шапка таблицы со списком экземпляров данных раскрашивается в определённый цвет, разный для каждого языка. Для перевода на другие языки отдельных фраз, используемых на сайте, в системе управления предусмотрена страница «Interface localization» (Основное меню -&gt; Локализация -&gt; Interface localization), где все используемые фразы перечислены в списке.</p>', 0, 0, 4),
(17, 'Поиск по спискам экземпляров', '<p>Поиск по спискам является крайне необходимым, когда количество экземпляров какого-либо объекта становится слишком велико. Ярким примером такой ситуации является наличие большого количества новостей или разделов сайта. В такой ситуации бывает очень трудно отыскать нужный раздел сайта, особенно, учитывая их иерархическую вложенность.</p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/poisk//clip_image002_0009.jpg" alt=""></p>  <p style="text-align: center;"><em>Элементы формы поиска</em></p>  <p style="text-align: left;"><strong>Форма поиска состоит из следующих элементов:</strong></p>  <ol>  <li>Список свойств объекта - перечень всех свойств объекта (см. область 1). Например, для объекта «Раздел» можно увидеть свойства «Заголовок», «Содержимое» и т.д. Из этого списка выбирается свойство, по которому будет вестись поиск.</li>  <li>Искомое значение свойства - указывается именно то значение свойства, которое нас интересует у экземпляров списка (см. область 2).</li>  <li>Кнопка «Найти» - после нажатия этой кнопки формируется список именно тех экземпляров списка, которые удовлетворяют условиям поиска (см. область 3).</li>  </ol>  <p>В качестве примера можно рассмотреть поиск нужного нам раздела по заголовку. Предположим, требуется найти на сайте раздел, заголовок которого «О компании». Для этого выполним ряд действий:</p>  <ol>  <li>В основном меню системы администрирования выбираем «Меню», чтобы перейти к списку всех разделов сайта.</li>  <li>В форме поиска, которая размещается над списком разделов, указываем свойство, по которому будет осуществляться поиск, т.е. «Заголовок раздела».</li>  <li>В качестве искомого значения свойства указываем «О компании» и нажимаем на кнопку «Найти»</li>  </ol>  <p><img src="http://ygin.ru/instruction/engine/poisk/clip_image004_0000.jpg" alt="" height="98" width="601"><br><em>Форма поиска пункта меню</em></p>  <p>В результате поиска мы получим только раздел, имеющий заголовок «О компании».</p>', 0, 1, 5),
(18, 'Элементы панели управления', '<table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td width="171">  <p align="center"><img src="http://ygin.ru/instruction/engine/uprav/clip_image001_0006.gif" alt="" height="25" width="98"></p>  </td>  <td valign="top" width="505">  <p>Кнопка «Создать». Данная кнопка предназначена для создания экземпляров открытого списка.</p>  </td>  </tr>  <tr>  <td width="171">  <p align="center"><img src="http://ygin.ru/instruction/engine/uprav/clip_image002_0006.gif" alt="" height="25" width="123"></p>  </td>  <td valign="top" width="505">  <p>Кнопка «Упорядочить» предназначена для упорядочивания элементов списка в каком-либо разделе основного меню, при этом порядок задаётся для каждой строки при помощи столбца «Порядковый номер».</p>  </td>  </tr>  <tr>  <td width="171">  <p align="center"><img src="http://ygin.ru/instruction/engine/uprav/clip_image003_0006.gif" alt="" height="25" width="76"></p>  </td>  <td valign="top" width="505">  <p>Кнопка «Вверх». Позволяет перейти из вложенной страницы на страницу уровнем выше.</p>  </td>  </tr>  </tbody>  </table>', 0, 1, 6),
(19, 'Редактор страниц', '<p></p>  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image002_0009.jpg" alt="" height="395" width="623"><br> Визуальный редактор</p>  <p>В данном поле находится всё содержимое раздела, которое будет отображаться на сайте. Для редактирования содержимого прилагается встроенный визуальный редактор. Редактор позволяет вводить и форматировать текст, таблицы, добавлять изображения, создавать в теле элемента ссылки на другие веб-ресурсы, вставлять текст из буфера обмена Windows, в том числе предварительно отформатированный, как например, из Microsoft Word.</p>  <p>Вставку форматированного текста из буфера обмена рекомендуется делать с использованием кнопки <img src="http://ygin.ru/instruction/engine/redaktor/clip_image003_0006.gif" alt="" height="14" width="16"> - Вставить из Word, чтобы имелась возможность удаления ненужного форматирования. После нажатия на кнопку выдается окно, в котором можно удалить определение стилей, шрифтов и лишние отступы и просмотреть результат. Затем производится форматирование текста по своему усмотрению в окне визуального редактора.</p>  <p><strong>Возможности редактора: </strong></p>  <table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td colspan="2" valign="top" width="676"><br> <em>Форматирование текста</em></td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image004.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>выравнивает текст по левому краю страницы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image005.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>выравнивает текст по правому краю страницы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image006.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>выравнивает текст по центру страницы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image007.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>выравнивает текст по ширине страницы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image008.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>делает текст Жирным</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image009.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>делает текст Курсивным</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image010.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>делает текст Зачёркнутым</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image011.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>делает текст Подчёркнутым</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image012.gif" alt="" height="22" width="86"></p>  </td>  <td valign="top" width="573">  <p>меняет формат текста</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image013.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>меняет цвет текста</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image014.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>меняет цвет фона</p>  </td>  </tr>  <tr>  <td colspan="2" valign="top" width="676">  <p><em>Работа с текстом</em></p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image015.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>вырезать текст</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image016.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>копировать текст</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image017.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>вставить</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image018.gif" alt="" height="14" width="16"></p>  </td>  <td valign="top" width="573">  <p>вставить как простой (без формата) текст</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image003_0007.gif" alt="" height="14" width="16"></p>  </td>  <td valign="top" width="573">  <p>вставить из Word (с форматом) текст</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image019.gif" alt="" height="13" width="15"></p>  </td>  <td valign="top" width="573">  <p>выделить весь текст</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image020.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>отменить последнее действие</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image021.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>повторить отменённое действие</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image022.gif" alt="" height="20" width="20"><img src="http://ygin.ru/instruction/engine/redaktor/clip_image023.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>задать отступы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image024.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>рисует горизонтальную разделительную линию</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image025.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет для выделенного текста или картинки создать гиперссылку, указав адрес ссылки. В качестве гиперссылки можно указать раздел сайта или загруженный в текущий раздел файл при помощи выбора из выпадающего списка</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image026.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет убрать гиперссылку из текста</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image027.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет вставлять изображения в текст, указав при этом адрес картинки. Существует возможность выбрать изображение, загруженное ранее в раздел сайта, при помощи функционального блока «Файлы»</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image028.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет делать текст в виде нижнего индекса</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image029.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет делать текст в виде верхнего индекса</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image030.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет создавать маркированный список</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image031.gif" alt="" height="20" width="20"></p>  </td>  <td valign="top" width="573">  <p>позволяет создавать нумерованный список</p>  </td>  </tr>  <tr>  <td colspan="2" valign="top" width="676">  <p><em>Работа с таблицами</em></p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image032.gif" alt="" height="16" width="16"></p>  </td>  <td width="573">  <p>позволяет вставить новую таблицу или отредактировать свойства выделенной таблицы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image033.gif" alt="" height="13" width="14"> <img src="http://ygin.ru/instruction/engine/redaktor/clip_image034.gif" alt="" height="13" width="14"></p>  </td>  <td width="573">  <p>позволяет просматривать свойства выделенной ячейки или строки</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image035.gif" alt="" height="15" width="16"></p>  </td>  <td width="573">  <p>позволяет вставить строку перед выделенной нами строкой</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image036.gif" alt="" height="15" width="16"></p>  </td>  <td width="573">  <p>позволяет вставить строку после выделенной нами строки</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image037.gif" alt="" height="13" width="16"></p>  </td>  <td width="573">  <p>позволяет удалить строку</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image038.gif" alt="" height="16" width="15"></p>  </td>  <td width="573">  <p>позволяет вставить столбец перед выделенным нами столбцом</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image039.gif" alt="" height="16" width="15"></p>  </td>  <td width="573">  <p>позволяет вставить столбец после выделенного нами столбца</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image040.gif" alt="" height="14" width="13"></p>  </td>  <td width="573">  <p>позволяет удалить столбец</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image041.gif" alt="" height="13" width="16"> <img src="http://ygin.ru/instruction/engine/redaktor/clip_image042.gif" alt="" height="13" width="16"></p>  </td>  <td width="573">  <p>позволяет объединять и разделять ячейки таблицы</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image043.gif" alt="" height="12" width="14"></p>  </td>  <td width="573">  <p>позволяет удалить любой формат заданного текста</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image044.gif" alt="" height="20" width="20"></p>  </td>  <td width="573">  <p>очищает исходный код страницы и исправление в нём ошибок</p>  </td>  </tr>  <tr>  <td width="103">  <p><img src="http://ygin.ru/instruction/engine/redaktor/clip_image045.gif" alt="*" height="5" width="18"></p>  </td>  <td width="573">  <p>позволяет просмотреть страницу в языке разметки (HTML)</p>  </td>  </tr>  </tbody>  </table>', 0, 1, 7),
(20, 'Работа с разделами и материалами сайта (Меню сайта)', '<p>Меню имеет иерархическую структуру с неограниченным уровнем вложенности. Список разделов в меню:</p>  <p style="text-align: center;"><br> <img src="http://ygin.ru/instruction/engine/rabota/clip_image002_0009.jpg" alt="" height="447" width="601"></p>  <p style="text-align: center;">Список разделов меню</p>  <p><strong>Создание нового раздела в меню:</strong></p>  <table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td colspan="2" valign="top" width="670"><em>Основные свойства</em></td>  </tr>  <tr>  <td valign="top" width="175">  <p>Имя</p>  </td>  <td valign="top" width="494">  <p>Введённое имя будет использоваться как название раздела в системе администрирования, а также в главном меню сайта.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Заголовок</p>  </td>  <td valign="top" width="494">  <p>Введённый текст будет использоваться в качестве заголовка страницы сайта (раздела) в тот момент, когда пользователь будет его просматривать на сайте.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Видимость</p>  </td>  <td valign="top" width="494">  <p>Признак, показывающий отображать ли раздел в меню сайта. <br> Несмотря на отсутствие видимости раздела в меню сайта, он всё равно будет доступен пользователям при наборе адреса раздела вручную.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Ссылка на страничку</p>  </td>  <td valign="top" width="494">  <p>В меню сайта для данного раздела будет сгенерирована ссылка на значение данного свойства</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Файлы</p>  </td>  <td valign="top" width="494">  <p>При помощи функционального блока «Файлы» возможна загрузка файлов, доступных для данного раздела. Это могут быть изображения, архивы, документы и прочее. Для того чтобы загрузить файл, необходимо нажать на кнопку «Обзор», указать при помощи стандартного диалогового окна местоположение файла и нажать на кнопку «Загрузить» <br> Все загруженные файлы можно увидеть в выпадающем списке доступных файлов в этом же функциональном блоке.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Содержимое раздела</p>  </td>  <td valign="top" width="494">  <p>Заполняется с помощью визуального редактора. Подробное описание редактора см. в разделе «Редактор страниц»</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>При отсутствии контента</p>  </td>  <td valign="top" width="494">  <p>Признак перехода к потомкам (вложенным разделам) при отсутствии контента необходим, когда структура меню становится очень ветвистой, т.е. присутствует множество разделов вложенных друг в друга. Если данный признак установлен, то при обращении к разделу сначала произойдёт проверка на наличие содержимого. При отсутствии контента происходит либо переход к списку вложенных разделов сайта, либо к содержимому первого раздела, либо выводится сообщение о том, что раздел находится в стадии разработки.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Название раздела в адресной строке (на англ) - одно слово</p>  </td>  <td valign="top" width="494">  <p>Необходимо записать название раздела на английском языке без пробелов (например, для раздела «Контактная информация» можно вписать «contacts»). Вписанное значение будет дополняться в адресе к разделу (URL), например, <a href="http://www.site.ru/contacts/">www.site.ru/contacts/</a>. Ссылка формируется по иерархии меню, т.е. сначала идёт данный параметр для корневого раздела и далее по иерархии до редактируемого раздела (/parent-alias/child-alias/).</p>  </td>  </tr>  <tr>  <td colspan="2" valign="top" width="670">  <p><em>Дополнительные свойства</em></p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Модули</p>  </td>  <td valign="top" width="494">  <p>В данном пункте присутствует список доступных шаблонов модулей (наборов модулей) для раздела.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Имя папки для хранения данных</p>  </td>  <td valign="top" width="494">  <p>Свойство определяет имя папки на диске, в которой будут храниться данные раздела (изображения и прочие загружаемые файлы). Рекомендуем оставлять его пустым.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Мета тэг description</p>  </td>  <td valign="top" width="494">  <p>короткое описание содержимого странички. Если оставить это поле пустым, то будет взят заголовок данного раздела.</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Мета тэг keywords</p>  </td>  <td valign="top" width="494">  <p>ключевые слова раздела</p>  </td>  </tr>  <tr>  <td valign="top" width="175">  <p>Значение тэга title</p>  </td>  <td valign="top" width="494">  <p>Установка значения заголовка окна браузера для данного раздела. При отсутствии заполняется автоматически.</p>  </td>  </tr>  </tbody>  </table>  <p>Для сохранения внесенных изменений необходимо нажать кнопку «Сохранить», при этом произойдет переход к списку элементов. При нажатии на кнопку «Применить», происходит сохранение всех данных без перехода куда-либо (обычно нужно, чтобы сохранить данные и продолжить работу со свойствами раздела). Для выхода из режима редактирования без сохранения изменений нажмите кнопку «Отменить».</p>', 0, 1, 8),
(21, 'Управление фотоальбомом и видеоматериалами', '<p></p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/foto/clip_image002_0009.jpg" alt="" height="356" width="623"></p>  <p style="text-align: center;">Фотоальбомы</p>  <p style="text-align: left;">В фотогалерее есть возможность создавать новые фотоальбомы, а также создавать вложенные фотоальбомы, т.е. многоуровневую иерархию альбомов.</p>  <p style="text-align: left;">Для добавления альбома в текущий список (т.е. на данный уровень вложенности) служит кнопка «Создать», расположенная на панели управления.</p>  <p style="text-align: left;">Для создания вложенного альбома, нужно перейти на нужный уровень вложенности при помощи кнопки «Вложенные объекты» (см. область 1 рисунка 10) и воспользоваться кнопкой «Создать».</p>  <table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td colspan="2" valign="top" width="676"><em>Свойства альбома</em></td>  </tr>  <tr>  <td valign="top" width="162">  <p>Название</p>  </td>  <td valign="top" width="514">  <p>Заголовок фотоальбома.</p>  </td>  </tr>  <tr>  <td valign="top" width="162">  <p>Превью-фото</p>  </td>  <td valign="top" width="514">  <p>Изображение, отображаемое в списке фотоальбомов на сайте рядом с названием и текстом в списке галерей.</p>  </td>  </tr>  <tr>  <td valign="top" width="162">  <p>Текст в списке галерей</p>  </td>  <td valign="top" width="514">  <p>В разделе «Фотоальбом» на сайте выводится список имеющихся на сайте фотоальбомов. Если данное поле заполнено, то введённый в него текст отображается рядом с превью-фото фотоальбома в этом списке.</p>  </td>  </tr>  <tr>  <td valign="top" width="162">  <p>Текст в галерее</p>  </td>  <td valign="top" width="514">  <p>Если данное поле заполнено, то введённый в него текст отображается на сайте во время просмотра фотоальбома перед фотографиями.</p>  </td>  </tr>  </tbody>  </table>  <p>Для загрузки фотографий в нужный альбом необходимо перейти по ссылке «Фотографии» (см. область 2 рисунка 10) и воспользоваться кнопкой «Создать»</p>  <p style="text-align: center;"><img src="http://ygin.ru/instruction/engine/foto/clip_image004_0000.jpg" alt="" height="344" width="470"></p>  <p style="text-align: center;">Фотографии фотоальбома «Чудо России – столбы выветривания плато Маньпупунёр»</p>  <table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td colspan="2" valign="top" width="676"><em>Свойства фотографий</em></td>  </tr>  <tr>  <td valign="top" width="197">  <p>Название</p>  </td>  <td valign="top" width="479">  <p>Название фотографии (если введено) отображается на сайте рядом с фотографией.</p>  </td>  </tr>  <tr>  <td valign="top" width="197">  <p>Изображение</p>  </td>  <td valign="top" width="479">  <p>Исходное изображение, на основании которого будет сгенерирована превью-картинка (уменьшенная до небольшого размера) для просмотра в фотоальбоме</p>  </td>  </tr>  </tbody>  </table>  <p>Управление видеоматериалами производится аналогичным образом, с той разницей, что к видеоролику можно добавлять изображение для его первого кадра.</p>', 0, 0, 9);
INSERT INTO `da_instruction` (`id_instruction`, `name`, `content`, `desc_type`, `visible`, `num_seq`) VALUES
(22, 'Новостная система', '<p></p>  <p align="center"><img src="http://ygin.ru/instruction/engine/novosti/clip_image002_0009.jpg" alt="" height="360" width="494"> <br> Рис.12. Список новостей</p>  <p style="text-align: left;"><br> Для добавления новости необходимо нажать на кнопку «Создать», расположенную над списком новостей.<br> <strong>На экране редактирования свойств новости заполните поля:</strong></p>  <table border="1" cellpadding="0" cellspacing="0">  <tbody>  <tr>  <td colspan="2" valign="top" width="676">  <p><em>Свойства новости</em></p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Заголовок новости</p>  </td>  <td valign="top" width="513">  <p>Введённое значение будет использоваться как заголовок новости на сайте.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Дата начала</p>  </td>  <td valign="top" width="513">  <p>Новость либо относится к данной дате, либо начинается с неё.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Показывать время</p>  </td>  <td valign="top" width="513">  <p>Показывать ли время в датах новости.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Дата окончания (анонс)</p>  </td>  <td valign="top" width="513">  <p>Иногда новость трактуется как событие, происходящее в определённый промежуток времени. Тогда «Дата анонса» (а позднее «Дата новости») на сайте отображается как «Дата начала — Дата окончания (анонс)».</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Дата для главной страницы (закрепление)</p>  </td>  <td valign="top" width="513">  <p>Используется только на главной странице портала. Предназначена для определения порядка новостей на ленте (закрепления новости до указанной даты).</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Показывать ли дату окончания</p>  </td>  <td valign="top" width="513">  <p>Признак, показывающий, отображать ли дату окончания события на сайте или нет.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Картинка</p>  </td>  <td valign="top" width="513">  <p>Изображение, привязанное к новости. Отображается как на главной странице сайта, так и в разделе конкретной новости.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Краткое содержание</p>  </td>  <td valign="top" width="513">  <p>Отображается на главной странице.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Файлы</p>  </td>  <td valign="top" width="513">  <p>Список файлов, ссылки на которые (либо сами файлы) необходимо вставить в «Текст новости». Для того, чтобы загрузить файл, необходимо нажать на кнопку «Обзор», указать при помощи стандартного диалогового окна местоположение файла и нажать на кнопку «Загрузить». Все загруженные файлы можно увидеть в выпадающем списке доступных файлов.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Текст новости</p>  </td>  <td valign="top" width="513">  <p>Заполняется с помощью визуального редактора. Выводится на сайте при отображении конкретной новости.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Анонс</p>  </td>  <td valign="top" width="513">  <p>В системе анонс становится новостью после того, как дата начала становится больше текущего времени. Пока анонс новостью не стал, содержимое этого поля выводится на главной странице в блоке анонсов вместе с заголовком.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Категории событий</p>  </td>  <td valign="top" width="513">  <p>Здесь перечислены категории, к которым может относиться новость. Одна новость может относиться к нескольким категориям.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Видимость</p>  </td>  <td valign="top" width="513">  <p>Признак, отвечающий за отображение новости как на главной странице сайта, так и в архиве новостей.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Видео</p>  </td>  <td valign="top" width="513">  <p>Видеоролик, связанный с новостью.</p>  </td>  </tr>  <tr>  <td valign="top" width="163">  <p align="left">Первый кадр видеоролика</p>  </td>  <td valign="top" width="513">  <p>Если есть видеоролик, то для его корректного отображения на странице (до начала воспроизведения) загружается изображение первого кадра. Оно отображается в плейере на странице новости на сайте.</p>  </td>  </tr>  </tbody>  </table>', 0, 0, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `da_instruction_rel`
--

CREATE TABLE IF NOT EXISTS `da_instruction_rel` (
  `id_instruction` int(8) NOT NULL,
  `rel_text` varchar(255) DEFAULT NULL,
  `id_instruction_rel` int(8) NOT NULL,
  PRIMARY KEY (`id_instruction`,`id_instruction_rel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_instruction_rel`
--

INSERT INTO `da_instruction_rel` (`id_instruction`, `rel_text`, `id_instruction_rel`) VALUES
(13, 'Структура системы управления', 15),
(14, 'Основные возможности', 13),
(14, 'Меню сайта', 20),
(15, 'Основные возможности', 13),
(15, 'Поиск данных', 17),
(15, 'Элементы панели управления', 18),
(17, 'Структура системы управления', 15),
(17, 'Элементы панели управления', 18),
(19, 'Размещение видео с Rutube', 11),
(19, 'Размещение видео с YouTube', 12),
(20, 'Редактор страниц', 19);

-- --------------------------------------------------------

--
-- Структура таблицы `da_job`
--

CREATE TABLE IF NOT EXISTS `da_job` (
  `id_job` varchar(255) NOT NULL COMMENT 'ИД задачи',
  `interval_value` int(8) NOT NULL DEFAULT '0' COMMENT 'Интервал запуска задачи (в секундах)',
  `error_repeat_interval` int(8) NOT NULL DEFAULT '0' COMMENT 'Интервал запуска задачи в случае ошибки (в секундах)',
  `first_start_date` int(10) DEFAULT NULL COMMENT 'Дата первого запуска',
  `last_start_date` int(10) DEFAULT NULL COMMENT 'Дата последнего запуска',
  `next_start_date` int(10) DEFAULT NULL COMMENT 'Дата следущего запуска',
  `failures` int(2) NOT NULL DEFAULT '0' COMMENT 'Количество ошибок',
  `name` varchar(255) NOT NULL COMMENT 'Имя задачи',
  `class_name` varchar(255) NOT NULL COMMENT 'Имя класса задачи',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Вкл.',
  `priority` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Приоритет запуска',
  `start_date` int(11) DEFAULT NULL COMMENT 'Дата запуска текущего потока',
  `max_second_process` int(10) DEFAULT NULL COMMENT 'Максимальное число секунд выполнения задачи',
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Планировщик';

--
-- Дамп данных таблицы `da_job`
--

INSERT INTO `da_job` (`id_job`, `interval_value`, `error_repeat_interval`, `first_start_date`, `last_start_date`, `next_start_date`, `failures`, `name`, `class_name`, `active`, `priority`, `start_date`, `max_second_process`) VALUES
('1', 90, 180, 1313061006, 1344593833, 1344593882, 0, 'Отсылка сообщений', 'ygin.modules.mail.components.DispatchEvents', 1, 0, NULL, 120);

-- --------------------------------------------------------

--
-- Структура таблицы `da_link_message_user`
--

CREATE TABLE IF NOT EXISTS `da_link_message_user` (
  `id_message` int(8) NOT NULL,
  `id_user` int(8) NOT NULL,
  PRIMARY KEY (`id_message`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `da_localization`
--

CREATE TABLE IF NOT EXISTS `da_localization` (
  `id_localization` int(8) NOT NULL COMMENT 'id',
  `name` varchar(60) NOT NULL COMMENT 'Название',
  `code` char(3) NOT NULL COMMENT 'Код',
  `is_use` int(1) NOT NULL DEFAULT '1' COMMENT 'Используется ли локализация',
  PRIMARY KEY (`id_localization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Доступные локализации';

--
-- Дамп данных таблицы `da_localization`
--

INSERT INTO `da_localization` (`id_localization`, `name`, `code`, `is_use`) VALUES
(1, 'Русская', 'ru', 1),
(2, 'Английская', 'en', 0),
(3, 'Финская', 'fi', 0),
(5, 'Немецкая', 'de', 0),
(6, 'Французская', 'fr', 0),
(7, 'Албанская', 'al', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `da_mail_account`
--

CREATE TABLE IF NOT EXISTS `da_mail_account` (
  `id_mail_account` int(8) NOT NULL COMMENT 'id',
  `email_from` varchar(50) DEFAULT NULL COMMENT 'E-mail для поля "От"',
  `from_name` varchar(100) DEFAULT NULL COMMENT 'Имя отправителя',
  `default_subject` varchar(255) DEFAULT NULL COMMENT 'Тема по умолчанию',
  `host` varchar(255) NOT NULL COMMENT 'HOST',
  `user_name` varchar(50) DEFAULT NULL COMMENT 'Имя пользователя для авторизации',
  `user_password` varchar(50) DEFAULT NULL COMMENT 'Пароль для авторизации',
  `smtp_auth` int(1) NOT NULL DEFAULT '1' COMMENT 'Требуется ли авторизация',
  PRIMARY KEY (`id_mail_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Почтовые аккаунты';

--
-- Дамп данных таблицы `da_mail_account`
--

INSERT INTO `da_mail_account` (`id_mail_account`, `email_from`, `from_name`, `default_subject`, `host`, `user_name`, `user_password`, `smtp_auth`) VALUES
(0, 'noreplay@test.lo', 'Письмо с Вашего сайта', 'Письмо с сайта', '127.0.0.1', NULL, NULL, 0),
(10, 'robot@test.ru', 'test.ru', NULL, 'smtp.test.ru', 'robot@test.ru', 'password', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_menu`
--

CREATE TABLE IF NOT EXISTS `da_menu` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL DEFAULT 'Имя раздела' COMMENT 'Название в меню',
  `caption` varchar(255) DEFAULT NULL COMMENT 'Заголовок раздела',
  `content` longtext COMMENT 'Содержимое раздела',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Смена родительского раздела',
  `sequence` int(2) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `note` varchar(200) DEFAULT NULL COMMENT 'Примечания',
  `alias` varchar(100) DEFAULT NULL COMMENT 'Название в адресной строке',
  `title_teg` varchar(255) DEFAULT NULL COMMENT '<title>',
  `meta_description` varchar(255) DEFAULT NULL COMMENT '<meta name="description">',
  `meta_keywords` varchar(255) DEFAULT NULL COMMENT '<meta name="keywords">',
  `go_to_type` tinyint(1) DEFAULT '1' COMMENT 'При отсутствии контента:',
  `id_module_template` int(8) DEFAULT NULL COMMENT 'Набор модулей',
  `external_link` varchar(255) DEFAULT NULL COMMENT 'Ссылка (авто)',
  `external_link_type` tinyint(1) DEFAULT NULL COMMENT 'Открывать в новом окне (авто)',
  `removable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Разрешить удаление',
  `image` int(8) DEFAULT NULL COMMENT 'Картинка для раздела',
  `type_main_menu` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'В главном меню',
  `css_class` varchar(255) DEFAULT NULL COMMENT 'CSS Класс',
  `css_class_a` varchar(255) DEFAULT NULL COMMENT 'CSS Класс ссылки',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `id` (`id`,`id_parent`,`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Меню' AUTO_INCREMENT=138 ;

--
-- Дамп данных таблицы `da_menu`
--

INSERT INTO `da_menu` (`id`, `name`, `caption`, `content`, `visible`, `id_parent`, `sequence`, `note`, `alias`, `title_teg`, `meta_description`, `meta_keywords`, `go_to_type`, `id_module_template`, `external_link`, `external_link_type`, `removable`, `image`, `type_main_menu`, `css_class`, `css_class_a`) VALUES
(100, 'Главная', NULL, '', 1, NULL, 1, NULL, '/', NULL, NULL, NULL, 4, 1, NULL, 0, 1, NULL, 1, 'home', NULL),
(122, 'Новости', NULL, '', 1, NULL, 6, NULL, 'news', NULL, NULL, NULL, 4, 1, '/news/', 0, 0, NULL, 1, 'news', NULL),
(124, 'Фотоальбом', NULL, '', 0, NULL, 5, NULL, 'photo-vodeo', NULL, NULL, NULL, 2, 1, NULL, 0, 1, NULL, 1, NULL, NULL),
(125, 'Фотоальбомы', NULL, '', 1, 124, 1, NULL, 'photoalbum', NULL, NULL, NULL, 1, 1, '/photoalbum/', 0, 1, NULL, 1, NULL, NULL),
(128, 'Видеоальбом', NULL, '', 1, 124, 6, NULL, 'videoalbum', NULL, NULL, NULL, 1, 1, '/videoalbum/', 0, 1, NULL, 1, NULL, NULL),
(129, 'Контакты', NULL, '', 1, NULL, 7, NULL, 'contact', NULL, NULL, NULL, 4, 1, '/contact/', 0, 1, NULL, 1, 'contacts', NULL),
(132, 'О центре', NULL, '<h2 class="title-main">История создания</h2>\r\n<p class="text-main">Современные медицинские центры, как правило, предлагают своим пациентам все способы оздоровления и лечения. Клиники совмещают диагностику с назначением и проведением терапевтического, а далее реабилитационного курса. Это очень удобно, так как в значительной степени экономит полезное время человека, позволяя, во-первых, не выпадать надолго из привычного ритма жизни, а, во-вторых, не дать перейти заболеванию в хроническую форму.</p>\r\n<h2 class="title-main">Современное оборудование</h2>\r\n<ol>\r\n<li class="text-main">Аппарат лазерный на парах меди <a href="#" class="a-main" target="_blank">"Яхрома -Мед"</a>;</li>\r\n<li class="text-main">Аппарат ультразвуковой оториноларинологический <a href="#" class="a-main" target="_blank">"Тонзиллор - М"</a>;</li>\r\n<li class="text-main">Аппарат УЗИ <a href="#" class="a-main" target="_blank">SA-8000 MEDISON</a>;</li>\r\n<li class="text-main">Аппарат 3-MAX - инновационное оборудование, в котором сочетаются три новые эффективные технологии применяемые для омоложения тела и лица;</li>\r\n<li><span style="line-height: 1.5em;">Современный аппарат прессотерапии <a href="#" class="a-main" target="_blank">BODY BEAUTY CLINIC 12</a></span></li>\r\n</ol>\r\n<h2 class="title-main">Cертификаты</h2>\r\n<p><a href="/files/about/certificates/full/img-9.jpg" class="js-popup-img-overlay"><img src="/files/about/certificates/img-9.jpg" width="199" height="275" /></a>&nbsp;<a href="/files/about/certificates/full/img-10.jpg" class="js-popup-img-overlay"><img src="/files/about/certificates/img-10.jpg" width="195" height="268" /></a></p>', 1, NULL, 2, NULL, 'o_centre', NULL, NULL, NULL, 4, 1, NULL, 0, 1, NULL, 1, 'about', NULL),
(134, 'Специалисты', NULL, '', 1, NULL, 3, NULL, 'specialist', NULL, NULL, NULL, 4, 1, '/specialist/', 0, 1, NULL, 1, 'specialists', NULL),
(137, 'Отзывы', 'Отзывы', '', 1, NULL, 4, NULL, 'review', NULL, NULL, NULL, 4, 1, '/review/', 0, 0, NULL, 1, 'reviews', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_message`
--

CREATE TABLE IF NOT EXISTS `da_message` (
  `id_message` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `text` longtext NOT NULL COMMENT 'Текст',
  `date` int(10) unsigned NOT NULL COMMENT 'Дата создания',
  `type` int(8) NOT NULL DEFAULT '1' COMMENT 'тип',
  `sender` varchar(255) NOT NULL,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Уведомления' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `da_migration`
--

CREATE TABLE IF NOT EXISTS `da_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_migration`
--

INSERT INTO `da_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1342684221),
('m120716_174729_macro', 1342684221),
('m120718_122532_macro', 1342690162),
('m120720_002139_ngin', 1342729247),
('m120720_223512_ngin', 1343114947),
('m120724_125848_ngin', 1343130984),
('m120724_210920_ngin', 1343214415),
('m120725_075905_add_field_to_system_parameters', 1343204594),
('m120727_000458_ngin', 1344594601),
('m120728_223710_ngin', 1344594601),
('m120731_013940_ngin', 1344594601),
('m120731_121338_ngin', 1344594601),
('m120731_211556_ngin', 1344594601),
('m120802_162026_ngin', 1344594601),
('m120804_002349_ngin', 1344594601),
('m120806_103133_mail_add_html_format_type', 1344594601),
('m120807_065040_drop_field_is_send_from_feedback', 1344594601),
('m120807_083424_drop_field_send_status_from_faq', 1344594601),
('m120807_114536_make_autoincrement_pk_for_da_event', 1344594601),
('m120807_131907_ngin', 1344594601),
('m120807_164940_ngin', 1344594601),
('m120808_152726_ngin', 1344594601),
('m120809_134126_ngin', 1344836799),
('m120810_102553_update_shop_offer_event_type', 1344594601),
('m120814_173220_ngin', 1345536580),
('m120814_230010_ngin', 1345536580),
('m120815_005422_ngin', 1345536581),
('m120820_134546_shop_module', 1345529983),
('m120820_134744_shop_module', 1345529983),
('m120820_135212_shop_module', 1345529983),
('m120820_135324_shop_module', 1345529984),
('m120820_174205_ngin', 1345536581),
('m120820_182032_ngin', 1345536581),
('m120821_221310_ngin', 1346662247),
('m120822_170237_ngin', 1346662247),
('m120828_103629_ngin', 1347362840),
('m120828_164130_ngin', 1347362840),
('m120828_174955_ngin', 1347362841),
('m120904_121407_shop_module', 1347362841),
('m120904_150721_ngin', 1347362841),
('m120913_171217_ngin', 1347626896),
('m120914_121927_ngin', 1347626896),
('m120915_223619_ngin', 1347808318),
('m120918_081514_shop_module', 1348481234),
('m120919_073944_shop_module', 1348481234),
('m121002_083734_shop_module', 1349179578),
('m121004_055741_ngin', 1349360888),
('m121004_080450_change_tables_engine_to_innodb', 1349360910),
('m121004_114202_ngin', 1349360912),
('m121004_130202_ngin', 1349360912),
('m121005_071430_menu_module', 1349449650),
('m121009_071450_scheduler_module', 1349791566),
('m121009_071615_scheduler_module', 1349791567),
('m121009_071744_scheduler_module', 1349791567),
('m121009_100803_mail_module', 1349791567),
('m121009_132010_struct_ngin', 1349791569),
('m121009_132011_ngin', 1349791569),
('m121009_153554_struct_ngin', 1349791570),
('m121009_153555_ngin', 1349791570),
('m121009_160202_ngin', 1349791570),
('m121010_020603_ngin', 1349863743),
('m121010_133117_ngin', 1350371052),
('m121015_123829_user_module', 1351367964),
('m121015_163208_ngin', 1350371056),
('m121016_131442_struct_ngin', 1351367966),
('m121016_131443_ngin', 1351367967),
('m121016_134502_struct_ngin', 1351367967),
('m121018_062400_photogallery_module', 1351367967),
('m121018_062954_photogallery_module', 1351367968),
('m121023_130918_struct_ngin', 1351367974),
('m121023_130919_ngin', 1351367974),
('m121025_130718_ngin', 1351367974),
('m121029_114119_ngin', 1351501041),
('m121030_105729_ngin', 1352115207),
('m121031_193910_ngin', 1352115209),
('m121101_150836_ngin', 1352115210),
('m121102_141700_news_module', 1352543161),
('m121107_145242_ngin', 1352543162),
('m121108_095825_ngin', 1352543162),
('m121109_095000_messenger_module', 1352543162),
('m121109_095521_messenger_module', 1352543162),
('m121109_100548_messenger_module', 1352543163),
('m121109_131529_messenger_module', 1352543163),
('m121115_194907_ngin', 1353672671),
('m121210_153052_struct_ngin', 1355989242),
('m121210_153053_ngin', 1355989243),
('m121227_071724_ngin', 1359392575),
('m130110_221227_ngin', 1359392575),
('m130116_075255_messanger_module', 1359392576),
('m130206_161859_ngin', 1363760020),
('m130212_205359_add_comments_to_tables', 1363760046),
('m130220_165514_ngin', 1363760046),
('m130222_211623_ngin', 1363760046),
('m130304_105600_ngin', 1363760046),
('m130312_094709_struct_ngin', 1363760047),
('m130312_094710_ngin', 1363760048),
('m130313_163310_struct_ngin', 1363760048),
('m130313_163311_ngin', 1363760048),
('m130313_172038_struct_ngin', 1363760049),
('m130313_172039_ngin', 1363760050),
('m130328_151605_struct_ngin', 1365597562),
('m130328_151606_ngin', 1365597563),
('m130404_131839_ngin', 1365597563),
('m130405_132019_filename_translit', 1365597563),
('m130409_094600_project_bootstrap', 1365597563),
('m130419_131440_user_module', 1369160489),
('m130422_065535_new_users', 1369160489),
('m130423_121920_struct_ngin', 1369160491),
('m130423_121921_ngin', 1369160491),
('m130423_122356_new_users_password', 1369160491),
('m130514_104329_comments_module', 1369160491),
('m130515_142007_ngin', 1369160491),
('m130516_140735_struct_ngin', 1369160494),
('m130516_140736_ngin', 1369160494),
('m130516_190627_menu_visible_view_column', 1369160494),
('m130517_230538_struct_ngin', 1369160495),
('m130517_230539_ngin', 1369160495),
('m130520_123302_struct_ngin', 1369160496),
('m130520_170216_ngin', 1369160496),
('m130521_224420_ngin', 1369639016),
('m130522_122615_ngin', 1369639016),
('m130522_171926_ngin', 1369639016),
('m130522_175400_backend', 1369681065),
('m130522_185724_ngin', 1369681066),
('m130523_112000_ygin', 1369987438),
('m130523_133419_ygin', 1369681069),
('m130524_013011_struct_ygin', 1369681072),
('m130524_013012_ygin', 1369681072),
('m130528_204220_struct_ygin', 1371535225),
('m130528_204221_ygin', 1371535226),
('m130606_160848_struct_ygin', 1371535226),
('m130606_160849_ygin', 1371535226),
('m130607_155511_ygin', 1371535226),
('m130607_171901_struct_ygin', 1371535228),
('m130607_171902_ygin', 1371535228),
('m130610_225445_struct_ygin', 1371535228),
('m130610_225446_ygin', 1371535229),
('m130611_164844_ygin', 1371535229),
('m130611_204701_struct_ygin', 1371535229),
('m130611_204702_ygin', 1371535230),
('m130611_205034_struct_ygin', 1371535230),
('m130611_205035_ygin', 1371535230),
('m130611_205234_struct_ygin', 1371535230),
('m130614_131857_ygin', 1371535230),
('m130614_152109_ygin', 1371542921),
('m130618_115456_ygin', 1371542921),
('m130618_155228_ygin', 1408711596),
('m130620_143444_struct_ygin', 1408711596),
('m130620_143445_ygin', 1408711596),
('m130621_130836_ygin', 1408711596),
('m130622_115016_ygin', 1408711597),
('m130626_100540_faq_categories_mail', 1408711597),
('m130708_174249_ygin', 1408711597),
('m130710_114016_struct_ygin', 1408711598),
('m130710_114017_ygin', 1408711598),
('m130710_142133_struct_ygin', 1408711598),
('m130711_112950_struct_ygin', 1408711598),
('m130711_112951_ygin', 1408711599),
('m130714_230340_ygin', 1408711599),
('m130716_120239_ygin', 1408711599),
('m130717_113251_struct_ygin', 1408711599),
('m130717_113252_ygin', 1408711599),
('m130718_213659_struct_ygin', 1408711601),
('m130723_095944_ygin', 1408711601),
('m130724_100320_ygin', 1408711601),
('m130820_064119_comments_column', 1408711601),
('m130826_135610_struct_ygin', 1408711602),
('m130826_135611_ygin', 1408711602),
('m130909_174245_ygin', 1408711602),
('m130916_162832_ygin', 1408711602),
('m130920_163425_struct_ygin', 1408711602),
('m130920_163426_ygin', 1408711602),
('m130924_120617_ygin', 1408711602),
('m131001_131235_struct_ygin', 1408711603),
('m131001_131236_ygin', 1408711603),
('m131002_193452_banner_module_aggregate_job', 1408711603),
('m131021_144947_ygin', 1408711603),
('m140108_191559_ygin', 1408711603),
('m140109_163100_backend_config', 1408711603),
('m140208_112056_ygin', 1408711603),
('m140224_111615_struct_ygin', 1408711603),
('m140224_111616_ygin', 1408711603),
('m140305_055619_ygin_banner_id', 1408711604),
('m140421_125605_ygin', 1408711607),
('m140430_115900_ygin', 1408711607),
('m140605_112159_ygin', 1408711607),
('m240919_073944_shop_plugin', 1348572502);

-- --------------------------------------------------------

--
-- Структура таблицы `da_object`
--

CREATE TABLE IF NOT EXISTS `da_object` (
  `id_object` varchar(255) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Имя объекта',
  `id_field_order` varchar(255) DEFAULT NULL COMMENT 'Свойство для порядка',
  `order_type` int(2) NOT NULL DEFAULT '1' COMMENT 'Тип порядка',
  `table_name` varchar(255) DEFAULT NULL COMMENT 'Таблица / Обработчик / Объект',
  `id_field_caption` varchar(255) DEFAULT NULL COMMENT 'Свойство для отображения',
  `object_type` int(2) NOT NULL DEFAULT '1' COMMENT 'Тип объекта',
  `folder_name` varchar(255) DEFAULT NULL COMMENT 'Путь к документам',
  `parent_object` varchar(255) DEFAULT NULL COMMENT 'Родитель',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `use_domain_isolation` int(1) NOT NULL DEFAULT '0' COMMENT 'Использовать доменную изоляцию данных',
  `field_caption` varchar(255) DEFAULT NULL COMMENT 'Свойство модели для отображения',
  `yii_model` varchar(255) DEFAULT NULL COMMENT 'yii-модель',
  PRIMARY KEY (`id_object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Объекты';

--
-- Дамп данных таблицы `da_object`
--

INSERT INTO `da_object` (`id_object`, `name`, `id_field_order`, `order_type`, `table_name`, `id_field_caption`, `object_type`, `folder_name`, `parent_object`, `sequence`, `use_domain_isolation`, `field_caption`, `yii_model`) VALUES
('1', 'Разработка', '', 1, NULL, '', 1, NULL, NULL, 3, 0, NULL, NULL),
('101', 'Наборы виджетов', '16', 2, 'da_site_module_template', '13', 1, NULL, '102', 7, 0, 'name', 'SiteModuleTemplate'),
('102', 'Модули', NULL, 1, NULL, NULL, 1, NULL, '4', 23, 0, NULL, NULL),
('103', 'Виджеты сайта', '542', 1, 'da_site_module', '542', 1, 'content/modules', '102', 8, 0, 'name', 'SiteModule'),
('105', 'Голосование', '536', 2, 'pr_voting', '531', 1, 'content/vote/', '4', 18, 0, 'name', 'vote.models.Voting'),
('106', 'Ответы на голосование', NULL, 2, 'pr_voting_answer', NULL, 1, NULL, '105', 9, 0, NULL, NULL),
('2', 'Пользователи и группы', NULL, 1, NULL, NULL, 1, NULL, NULL, 4, 0, NULL, NULL),
('20', 'Объекты', '104', 1, 'da_object', '64', 1, NULL, '1', 1, 0, 'name', 'ygin.models.object.DaObject'),
('21', 'Свойства объекта', '77', 1, 'da_object_parameters', '79', 1, '', '20', 2, 0, 'caption', 'ygin.models.object.ObjectParameter'),
('22', 'Типы данных', '105', 1, 'da_object_parameter_type', '86', 1, NULL, '1', 16, 0, 'name', NULL),
('24', 'Пользователи', NULL, 1, 'da_users', '90', 1, 'content/user', '2', 3, 0, 'name', 'user.models.User'),
('25', 'Типы прав доступа', NULL, 1, 'da_permission_type', '97', 1, NULL, '2', 3, 0, 'name', NULL),
('250', 'Комментарии', '1206', 2, 'pr_comment', '1207', 1, NULL, '4', 19, 0, 'comment_text', 'comments.models.CommentYii'),
('260', 'Баннеры', '1310', 1, 'pr_banner', NULL, 1, 'content/bnimg', '261', 18, 0, NULL, 'banners.models.Banner'),
('261', 'Баннерные места', '1318', 1, 'pr_banner_place', '1312', 1, NULL, '4', 22, 0, 'title', 'banners.models.BannerPlace'),
('27', 'Справочники', '109', 1, 'da_references', '110', 1, NULL, '3', 4, 0, 'name', 'ygin.models.Reference'),
('28', 'Значения справочника', '112', 1, 'da_reference_element', '114', 1, NULL, '27', 3, 0, 'value', 'ygin.models.ReferenceElement'),
('29', 'Группы системных параметров', '48', 2, 'da_group_system_parameter', '49', 1, NULL, '3', 5, 0, 'name', NULL),
('3', 'Общие настройки', NULL, 1, NULL, NULL, 1, NULL, NULL, 5, 0, NULL, NULL),
('30', 'Настройки сайта', '117', 1, 'da_system_parameter', NULL, 1, NULL, '3', 3, 0, NULL, 'ygin.models.SystemParameter'),
('31', 'Домены сайта', '281', 1, 'da_domain', '283', 1, 'content/domain/', '3', 2, 0, 'name', 'ygin.models.Domain'),
('313', 'Настройка кэширования', '', 1, 'engine/admin/special/cacheSettings.php', '', 3, NULL, '1', 14, 0, NULL, NULL),
('33', 'Формат сообщения', '250', 1, 'da_event_format', '31', 1, NULL, '6', 3, 0, 'description', NULL),
('34', 'Подписчики на события', NULL, 2, 'da_event_subscriber', '37', 1, NULL, '6', 3, 0, 'id_event_type', 'ygin.modules.mail.models.NotifierEventSubscriber'),
('35', 'Тип события', '50', 2, 'da_event_type', '51', 1, NULL, '6', 3, 0, 'name', 'ygin.modules.mail.models.NotifierEventType'),
('37', 'Файлы', '134', 1, 'da_files', '135', 1, NULL, '3', 7, 0, 'file_path', 'ygin.models.File'),
('39', 'Типы файлов', '129', 1, 'da_file_type', '130', 1, NULL, '37', 11, 0, 'name', NULL),
('4', 'Контент', NULL, 1, NULL, NULL, 1, NULL, NULL, 1, 0, NULL, NULL),
('40', 'Расширения файлов', '131', 1, 'da_file_extension', '132', 1, NULL, '37', 12, 0, 'ext', NULL),
('41', 'Пользовательские свойства', NULL, 1, 'da_object_property', NULL, 1, NULL, '1', 15, 0, NULL, NULL),
('43', 'SQL', '', 1, 'backend/special/sql', '', 3, NULL, '1', 9, 0, NULL, NULL),
('49', 'События', '253', 2, 'da_event', NULL, 1, NULL, '6', 2, 0, NULL, NULL),
('50', 'Почтовые аккаунты', '262', 1, 'da_mail_account', '262', 1, NULL, '6', 2, 0, 'host', NULL),
('500', 'Фотогалереи', '1500', 1, 'pr_photogallery', '1501', 1, 'content/photogallery', '4', 14, 0, 'name', 'photogallery.models.Photogallery'),
('501', 'Фотографии', '1511', 1, 'pr_photogallery_photo', '1507', 1, 'content/photos', '500', 14, 0, 'id_photogallery_photo', 'photogallery.models.PhotogalleryPhoto'),
('502', 'Новости', '1516', 2, 'pr_news', NULL, 1, 'content/news', '4', 9, 0, NULL, 'ygin.modules.news.models.News'),
('503', 'Категории новостей', '1523', 1, 'pr_news_category', '1522', 1, NULL, '502', 3, 0, 'name', NULL),
('504', 'Консультации', NULL, 1, NULL, NULL, 1, NULL, '4', 16, 0, NULL, NULL),
('505', 'Вопрос', '1530', 2, 'pr_consultation_ask', '1528', 1, 'content/consultation_ask', '504', 1, 0, 'ask', NULL),
('506', 'Ответ', '1536', 2, 'pr_consultation_answer', '1538', 1, NULL, '504', 2, 0, 'answer', NULL),
('507', 'Отвечающий', '1547', 1, 'pr_consultation_answerer', '1547', 1, 'content/answerer', '504', 3, 0, 'name', NULL),
('508', 'Специализация отвечающего', '1551', 1, 'pr_consultation_specialization', '1551', 1, NULL, '504', 4, 0, 'specialization', NULL),
('509', 'Категории продукции', '1636', 1, 'pr_product_category', '1553', 1, 'content/product_category', '518', 2, 0, 'name', 'shop.models.ProductCategory'),
('51', 'Планировщик', '267', 1, 'da_job', '274', 1, NULL, '1', 4, 0, 'name', 'ygin.modules.scheduler.models.Job'),
('511', 'Продукция', '1564', 1, 'pr_product', '1567', 1, 'content/product', '518', 1, 0, 'name', 'shop.models.Product'),
('512', 'Вопрос-ответ', '1579', 2, 'pr_question', '1581', 1, 'content/question', '4', 17, 0, 'question', NULL),
('517', 'Обратная связь', '1609', 2, 'pr_feedback', '1614', 1, NULL, '4', 15, 0, 'date', NULL),
('518', 'Магазин', NULL, 1, NULL, NULL, 1, NULL, NULL, 2, 0, NULL, NULL),
('519', 'Заказы пользователей', '1626', 2, 'pr_offer', '1621', 1, NULL, '518', 40, 0, 'fio', NULL),
('520', 'Витрина', '1635', 1, 'pr_vitrine', '1632', 1, 'content/vitrine', '4', 20, 0, 'title', NULL),
('521', 'Викторины', '1638', 2, 'pr_quiz', '1639', 1, 'content/quiz', '4', 21, 0, 'name', NULL),
('522', 'Вопросы викторины', '1648', 1, 'pr_quiz_question', '1646', 1, 'content/quiz_quest', '521', 43, 0, 'question', NULL),
('523', 'Варианты ответов', '1653', 1, 'pr_quiz_answer', '1651', 1, NULL, '521', 44, 0, 'answer', NULL),
('524', 'Ответ пользователя', '1661', 2, 'pr_quiz_answer_user', '1656', 1, NULL, '521', 45, 0, 'name', NULL),
('525', 'Брэнды', '1670', 1, 'pr_product_brand', '1667', 1, 'content/product_brand', '518', 46, 0, 'name', NULL),
('527', 'Плагины системы', '', 1, 'da_plugin', '', 1, 'content/plugin', '3', 1, 0, NULL, NULL),
('528', 'Настройка плагинов', NULL, 1, 'backend/plugin/plugin', NULL, 3, NULL, '527', 48, 0, NULL, NULL),
('529', 'Статусы остатка продукции', '1679', 1, 'pr_remain_status', '1680', 1, NULL, '518', 49, 0, NULL, NULL),
('530', 'Отзывы клиентов', '1686', 2, 'pr_client_review', '1685', 1, NULL, '4', 5, 0, 'name', NULL),
('531', 'Уведомления', '1695', 2, 'da_message', '1694', 1, 'content/messages', '3', 6, 0, 'text', 'ygin.modules.messenger.models.Message'),
('54', 'Доступные локализации', '310', 1, 'da_localization', '311', 1, NULL, '3', 8, 0, 'name', NULL),
('6', 'Почта', NULL, 1, NULL, NULL, 1, NULL, NULL, 7, 0, NULL, NULL),
('60', 'Проверка файлов', '', 1, 'backend/special/clearPreview', '', 3, NULL, '1', 10, 0, NULL, NULL),
('61', 'Инструкции', '145', 1, 'da_instruction', '141', 1, NULL, '1', 11, 0, 'name', NULL),
('63', 'Представление', '408', 1, 'da_object_view', '406', 1, '', '1', 3, 0, 'name', 'backend.models.DaObjectView'),
('66', 'Колонка представления', '419', 1, 'da_object_view_column', NULL, 1, '', '63', 9, 0, NULL, 'backend.models.DaObjectViewColumn'),
('80', 'php-скрипты', '337', 1, 'da_php_script_type', '342', 1, NULL, '1', 2, 0, 'description', 'ygin.models.PhpScript'),
('86', 'Интерфейс php-скрипта', '351', 1, 'da_php_script_interface', '348', 1, NULL, '80', 4, 0, 'name', NULL),
('89', 'Сброс кэша', '', 1, 'backend/special/clearCache', '', 3, NULL, '1', 7, 0, NULL, NULL),
('91', 'Поисковый индекс', '', 1, 'backend/special/recreateSearchIndex', '', 3, NULL, '1', 8, 0, NULL, NULL),
('94', 'Логи', '', 1, 'backend/special/logView', '', 3, NULL, '1', 12, 0, NULL, NULL),
('project-bloki-kontenta', 'Блоки контента', '', 1, 'app_block_content', '', 1, NULL, '4', 11, 0, NULL, 'BlockContent'),
('project-forma-obratnoi-svyazi', 'Форма обратной связи', 'project-forma-obratnoi-svyazi-id-app-form-feedback', 2, 'app_form_feedback', '', 1, NULL, '4', 13, 0, NULL, 'FormFeedback'),
('project-forma-zakaza-zvonka', 'Форма заказа звонка', 'project-forma-zakaza-zvonka-id-app-form-callback', 2, 'app_form_callback', '', 1, NULL, '4', 12, 0, NULL, 'FormCallback'),
('project-fotoalbomy', 'Фотоальбомы', 'project-fotoalbomy-sequence', 1, 'app_photoalbum', '', 1, NULL, '4', 7, 0, NULL, 'Photoalbum'),
('project-kontakty', 'Контакты', '', 1, 'app_contact', '', 1, NULL, '4', 10, 0, NULL, 'Contact'),
('project-slaider', 'Слайдер', 'project-slaider-sequence', 1, 'app_main_slider', '', 1, '/content/slider', '4', 6, 0, NULL, 'MainSlider'),
('project-spezialisty', 'Специалисты', 'project-spezialisty-sequence', 1, 'app_specialist', 'project-spezialisty-name', 1, '/content/specialist', '4', 3, 0, 'name', 'Specialist'),
('project-uslugi', 'Услуги', 'project-uslugi-sequence', 1, 'app_services', '', 1, '/content/services', '4', 2, 0, NULL, 'Services'),
('project-vidoalbom', 'Видоальбом', 'project-vidoalbom-sequence', 1, 'app_videoalbum', '', 1, NULL, '4', 8, 0, NULL, 'Videoalbum'),
('project-vopros-otvet', 'Вопрос-ответ', 'project-vopros-otvet-datetime', 2, 'app_question_answer', '', 1, NULL, '4', 4, 0, NULL, 'QuestionAnswer'),
('ygin-gii', 'gii (debug=true)', '', 1, '/gii/', '', 5, NULL, '1', 6, 0, NULL, NULL),
('ygin-invoice', 'Счета', 'ygin-invoice-create-date', 2, 'pr_invoice', 'ygin-invoice-create-date', 1, NULL, '518', 51, 0, 'create_date', 'Invoice'),
('ygin-menu', 'Меню', '7', 1, 'da_menu', '7', 1, 'content/menu', '4', 1, 0, 'name', 'Menu'),
('ygin-override', 'Переопределение представлений (debug=true)', '', 1, '/override/', '', 5, NULL, '1', 5, 0, NULL, NULL),
('ygin-views-generator', 'Генерация вьюхи', '', 1, 'viewGenerator/default/index', '', 3, NULL, '1', 50, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_instance`
--

CREATE TABLE IF NOT EXISTS `da_object_instance` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(8) NOT NULL,
  `id_domain` int(8) NOT NULL,
  `create_date` int(10) DEFAULT NULL,
  `last_modify_date` int(10) DEFAULT NULL,
  `delete_date` int(10) DEFAULT NULL,
  `id_user_creator` int(8) DEFAULT NULL,
  PRIMARY KEY (`id_object`,`id_instance`),
  KEY `id_object` (`id_object`,`id_instance`,`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_parameters`
--

CREATE TABLE IF NOT EXISTS `da_object_parameters` (
  `id_object` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Объект',
  `id_parameter` varchar(255) NOT NULL COMMENT 'ИД',
  `id_parameter_type` int(2) NOT NULL DEFAULT '0' COMMENT 'Тип свойства',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `widget` varchar(255) DEFAULT NULL COMMENT 'Виджет',
  `caption` varchar(255) NOT NULL COMMENT 'Название',
  `field_name` varchar(255) DEFAULT NULL COMMENT 'Имя поля в БД',
  `add_parameter` varchar(255) DEFAULT NULL COMMENT 'Параметр',
  `default_value` varchar(255) DEFAULT NULL COMMENT 'Значение по умолчанию',
  `not_null` tinyint(1) DEFAULT '1' COMMENT 'Обязательное',
  `sql_parameter` varchar(255) DEFAULT NULL COMMENT 'SQL условие',
  `is_unique` tinyint(1) DEFAULT NULL COMMENT 'Уникальное',
  `group_type` int(8) DEFAULT NULL COMMENT 'Связь с главным объектом',
  `need_locale` int(1) NOT NULL DEFAULT '0' COMMENT 'Переводить на другие языки',
  `search` int(1) NOT NULL DEFAULT '0' COMMENT 'Доступно для поиска',
  `is_additional` int(1) NOT NULL DEFAULT '0' COMMENT 'Дополнительное',
  `hint` text COMMENT 'Всплывающая подсказка',
  `visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_object`,`id_parameter`),
  UNIQUE KEY `id_parameter` (`id_parameter`),
  KEY `id_object` (`id_object`),
  KEY `id_object_2` (`id_object`,`sequence`),
  KEY `id_object_3` (`id_object`,`is_additional`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Свойства объекта';

--
-- Дамп данных таблицы `da_object_parameters`
--

INSERT INTO `da_object_parameters` (`id_object`, `id_parameter`, `id_parameter_type`, `sequence`, `widget`, `caption`, `field_name`, `add_parameter`, `default_value`, `not_null`, `sql_parameter`, `is_unique`, `group_type`, `need_locale`, `search`, `is_additional`, `hint`, `visible`) VALUES
('101', '12', 11, 8, NULL, 'id', 'id_module_template', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('101', '13', 2, 8, NULL, 'Название набора', 'name', '0', NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('101', '14', 10, 10, 'menu.backend.widgets.manageModule.ManageModuleWidget', 'Виджеты', NULL, '2', NULL, 0, 'SiteModuleTemplateListOfModule', 0, 0, 0, 0, 0, '', 1),
('101', '16', 9, 8, NULL, 'Использовать по умолчанию', 'is_default_template', '0', '0', 1, NULL, 0, 0, 0, 0, 0, 'Набор виджетов будет применяться к новым пунктам Меню корневого уровня', 1),
('101', 'ygin-widget-list-sequence', 13, 291, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('103', '541', 11, 1, NULL, 'id', 'id_module', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('103', '542', 2, 2, NULL, 'Имя', 'name', NULL, NULL, 1, NULL, 0, NULL, 1, 0, 0, NULL, 1),
('103', '544', 10, 4, 'menu.backend.widgets.phpScript.PhpScriptWidget', 'Обработчик', 'id_php_script', '2', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('103', '545', 14, 6, NULL, 'Простой текст', 'content', NULL, NULL, 0, NULL, 0, NULL, 1, 0, 0, 'Используется для хранения JavaScript и текст без форматирования', 1),
('103', '546', 9, 3, NULL, 'Видимость', 'is_visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, 'Позволяет временно отключать модуль из всех наборов модулей', 1),
('103', '554', 3, 8, NULL, 'Форматированный текст', 'html', NULL, NULL, 0, NULL, 0, 0, 1, 0, 0, 'Используется, если необходимо хранить форматированный текст, ссылки, файлы', 1),
('103', '556', 15, 7, NULL, 'Загрузить файлы', NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('105', '530', 11, 7, NULL, 'ID', 'id_voting', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('105', '531', 2, 7, NULL, 'Название голосования', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('105', '532', 10, 7, 'vote.backend.widgets.answerList.AnswerListWidget', 'Варианты ответов', NULL, '2', NULL, 0, 'VoteAnswerVisualElement', 0, 0, 0, 0, 0, '', 1),
('105', '533', 9, 7, NULL, 'Множество ответов', 'is_checkbox', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('105', '534', 9, 7, NULL, 'Активное', 'is_active', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('105', '535', 9, 7, NULL, 'Показать в модуле', 'in_module', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('105', '536', 4, 7, NULL, 'Дата создания голосования', 'create_date', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('106', '537', 11, 7, NULL, 'id', 'id_voting_answer', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('106', '538', 7, 7, NULL, 'Голосование', 'id_voting', '105', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('106', '539', 2, 7, NULL, 'Вариант ответа', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('106', '540', 1, 10, NULL, 'Количество голосов', 'count', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('20', '104', 13, 2, NULL, '&nbsp;', 'sequence', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('20', '124', 9, 13, NULL, 'Использовать доменную изоляцию данных', 'use_domain_isolation', NULL, '0', 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('20', '151', 10, 19, 'backend.backend.object.objectPermission.ObjectPermissionWidget', 'Права доступа', NULL, '2', '', 0, 'ObjectPermissionWidget', 0, 0, 0, 0, 0, '', 1),
('20', '155', 2, 7, NULL, 'Путь к документам', 'folder_name', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, 'Путь к папке на сервере относительно корня сайта для хранения загружаемых файлов. Например, "content/news" для сохранения загрузки фотографий к новостям.', 1),
('20', '157', 2, 16, NULL, 'yii-модель', 'yii_model', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, 'Например: ygin.models.File или просто Domain, если есть точная уверенность, что система уже знает о модели', 1),
('20', '207', 2, 8, NULL, 'Свойство модели для отображения', 'field_caption', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Используется для отображения названия экземпляра в списках и других местах.\r\nНапример, для отображения имени раздела можно указать name\r\nВ этом случае будет выполнено такое выражение: $model->name;\r\nТ.о. можно указывать любые доступные атрибуты объекта модели.', 1),
('20', '62', 10, 15, 'backend.backend.object.objectManageView.ObjectManageViewWidget', 'Создать представление', NULL, '2', '', 0, 'ObjectManageViewWidget', 0, 0, 0, 0, 1, '', 1),
('20', '63', 11, 1, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_object', '1', NULL, 1, 'name', 0, 0, 0, 0, 0, 'Уникальный ИД объекта в виде строки. Например для новостей ИД будет равен ygin-news  Префикс ygin зарезервирован для системных объектов, вместо него необходимо использовать название проекта или компании.', 1),
('20', '64', 2, 4, NULL, 'Имя объекта', 'name', NULL, NULL, 1, NULL, 0, NULL, 1, 0, 0, NULL, 1),
('20', '65', 7, 10, NULL, 'Свойство для порядка', 'id_field_order', '21', '', 0, 't.id_object=:id_instance', 0, 0, 0, 0, 0, 'По умолчанию выводимые в системе администрирования экземпляры данного объекта будут упорядочены по этому свойству', 1),
('20', '66', 6, 11, NULL, 'Тип порядка', 'order_type', '30', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('20', '67', 2, 6, NULL, 'Таблица / Контроллер / Ссылка', 'table_name', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'В зависимости от типа объекта, поле хранит различные значения. При стандартном - имя таблицы, при контроллере - алиас класса', 1),
('20', '68', 7, 9, NULL, 'Свойство для отображения', 'id_field_caption', '21', '', 0, 't.id_object=:id_instance', 0, 0, 0, 0, 0, 'Значение данного свойства будет выводится в списке, когда экземпляр данного объекта будет выбираться в качестве свойства другого объекта', 1),
('20', '69', 6, 5, NULL, 'Тип объекта', 'object_type', '31', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('20', '70', 12, 3, NULL, 'Родитель', 'parent_object', '1', NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('21', '156', 9, 14, NULL, 'Уникальное', 'is_unique', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Слежение за уникальностью значения на уровне системы администрирования', 1),
('21', '205', 9, 15, NULL, 'Связь с главным объектом', 'group_type', NULL, '0', 0, NULL, 0, 0, 0, 0, 0, 'У главного объекта в системном представлении появится возможность переходить к зависимым данным этого объекта, связанным по данному свойству', 1),
('21', '206', 9, 12, NULL, 'Переводить на другие языки', 'need_locale', NULL, '0', 0, NULL, 0, 0, 0, 0, 0, 'Определяет возможность перевода значения свойства на другие языки (английский, немецкий). Перевод возможен только для параметров с типом данных Строка, TextArea, CLOB.', 1),
('21', '220', 9, 13, NULL, 'Доступно для поиска', 'search', NULL, '0', 0, NULL, 0, 0, 0, 0, 0, 'Добавляет это поле в поисковый индекс, который используется при поиске по сайту', 1),
('21', '73', 9, 16, NULL, 'Дополнительное', 'is_additional', NULL, '0', 0, NULL, 0, 0, 0, 0, 0, 'Данное поле будет вынесено в отдельный контейнер, который изначально скрыт при редактировании объекта и появляется при нажатии на кнопку "Дополнительные свойства"', 1),
('21', '74', 11, 6, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_parameter', '1', NULL, 1, 'field_name/caption; id_object', 0, 0, 0, 0, 0, 'Уникальный ИД свойства объекта в виде строки. Например для заголовка новостей ИД будет равен ygin-news-title  Префикс ygin зарезервирован для системных свойств, вместо него необходимо использовать название проекта или компании.', 1),
('21', '75', 7, 3, NULL, 'Объект', 'id_object', '20', NULL, 1, '', 0, 1, 0, 0, 0, NULL, 1),
('21', '76', 7, 1, 'backend.backend.objectParameter.typeObjectParameterWidget.TypeObjectParameterWidget', 'Тип свойства', 'id_parameter_type', '22', '2', 1, '', 0, 0, 0, 0, 0, '', 1),
('21', '77', 13, 4, NULL, '&nbsp;', 'sequence', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('21', '78', 2, 7, NULL, 'Виджет', 'widget', '0', '', 0, '', 0, 0, 0, 0, 0, 'Алиас до виджета. Например ygin.widgets.textField.TextFieldWidget', 1),
('21', '79', 2, 2, NULL, 'Название', 'caption', NULL, NULL, 1, NULL, 0, 0, 1, 0, 0, 'Краткое название свойства на русском языке. Используется в заголовке столбца при отображении списка экземпляров объекта.', 1),
('21', '80', 2, 5, NULL, 'Имя поля в БД', 'field_name', '0', '', 0, '', 0, 0, 0, 0, 0, '', 1),
('21', '81', 2, 8, NULL, 'Параметр', 'add_parameter', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, 'Поле хранит значения, применяемые в различных ситуациях разработчиками', 1),
('21', '82', 14, 17, NULL, 'Всплывающая подсказка', 'hint', NULL, NULL, 0, NULL, 0, 0, 1, 0, 0, 'Если заполнено, рядом с полем будет отображаться вопросик, при наведении на который всплывает подсказка', 1),
('21', '83', 2, 10, NULL, 'Значение по умолчанию', 'default_value', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('21', '84', 9, 11, NULL, 'Обязательное', 'not_null', NULL, '1', 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('21', '87', 2, 9, NULL, 'SQL условие', 'sql_parameter', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Дополнительное условие для выборки нужных значений. Обращение к таблице текущего объекта следует использовать синоним <<current_instance>>.', 1),
('21', 'ygin-object-parameter-visible', 9, 288, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, 'Доступно ли свойство при редактировании экземпляра объекта', 1),
('22', '105', 13, 117, NULL, 'Порядок', 'sequence', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('22', '85', 11, 8, NULL, 'ИД', 'id_parameter_type', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('22', '86', 2, 9, NULL, 'Название типа данных', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('22', 'ygin-objectParameterType-sqlType', 2, 287, NULL, 'sql тип', 'sql_type', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('24', '122', 9, 4, NULL, 'Активен', 'active', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, 'Есть ли возможность пройти авторизацию и пользоваться сервисами сайта', 1),
('24', '123', 17, 10, NULL, 'count_post', 'count_post', NULL, '0', 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('24', '1697', 9, 0, NULL, 'Необходимо сменить пароль', 'requires_new_password', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('24', '238', 2, 9, NULL, 'Регистрационный ИД', 'rid', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, 'Токен в ссылке, которая приходит пользователю на почту для подтверждения регистрации. Пока это поле заполнено,пользователь не сможет авторизоваться.', 1),
('24', '239', 4, 8, NULL, 'Дата регистрации пользователя', 'create_date', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('24', '88', 11, 3, NULL, 'id', 'id_user', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('24', '89', 10, 2, 'user.backend.widgets.roles.RolesWidget', 'Роли пользователя', NULL, '2', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('24', '90', 2, 5, NULL, 'Логин', 'name', NULL, NULL, 1, NULL, 1, NULL, 0, 0, 0, NULL, 1),
('24', '91', 2, 6, 'user.backend.widgets.userPassword.UserPasswordWidget', 'Пароль', 'user_password', '2', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('24', '92', 2, 7, NULL, 'E-mail', 'mail', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('24', '93', 2, 1, NULL, 'Имя пользователя', 'full_name', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('25', '96', 11, 8, NULL, 'id', 'id_permission_type', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('25', '97', 2, 8, NULL, 'name', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('250', '1201', 11, 1, NULL, 'id', 'id_comment', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1202', 7, 2, NULL, 'Объект', 'id_object', '20', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1203', 1, 3, NULL, 'Экземпляр', 'id_instance', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1204', 2, 4, NULL, 'Автор', 'comment_name', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1205', 7, 5, NULL, 'Пользователь', 'id_user', '24', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1206', 4, 7, NULL, 'Дата', 'comment_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1207', 14, 6, NULL, 'Комментарий', 'comment_text', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1208', 6, 8, NULL, 'Отмодерировано', 'moderation', 'ygin-comment-reference-status', NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('250', '1209', 2, 9, NULL, 'IP', 'ip', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1210', 2, 7, NULL, 'Тема', 'comment_theme', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1211', 12, 11, NULL, 'id_parent', 'id_parent', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('250', '1588', 2, 195, NULL, 'Токен', 'token', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1300', 11, 1, NULL, 'ID', 'id_banner', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1302', 2, 3, NULL, 'Уникальное название баннера (на английском языке)', 'unique_name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1303', 2, 4, NULL, 'Ссылка на сайт', 'link', NULL, 'http://', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1304', 2, 6, NULL, 'Текстовое описание', 'alt', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1305', 8, 7, NULL, 'Файл', 'file', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1306', 7, 8, NULL, 'Баннерное место', 'id_banner_place', '261', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('260', '1307', 10, 9, 'banners.backend.widgets.bannerStatistic.BannerStatisticWidget', 'Статистика', NULL, '2', '', 0, 'BannerStatisticVisualElement', 0, 0, 0, 0, 0, '', 1),
('260', '1309', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('260', '1310', 13, 11, NULL, 'Порядок', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1311', 11, 4, NULL, 'ID', 'id_banner_place', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1312', 2, 1, NULL, 'Название', 'title', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1313', 6, 5, NULL, 'Тип показа баннеров', 'showing', '50', '3', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1314', 1, 6, NULL, 'id_instance', 'id_instance', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1315', 1, 7, NULL, 'id_object', 'id_object', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1317', 12, 9, NULL, 'Родительский ключ', 'id_parent', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1318', 13, 10, NULL, 'Порядок', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1320', 1, 3, NULL, 'Высота', 'height', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('261', '1321', 1, 2, NULL, 'Ширина', 'width', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('27', '109', 11, 8, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_reference', '1', NULL, 1, 'name;-; reference', 0, 0, 0, 0, 0, 'Уникальный ИД справочника. Например ygin-shop-reference-orderStatus  Префикс ygin зарезервирован для системных справочников, вместо него необходимо использовать название проекта или компании.', 1),
('27', '110', 2, 8, NULL, 'Название справочника', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('28', '111', 11, 1, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_reference_element_instance', '1', NULL, 1, 'value; id_reference', 0, 0, 0, 0, 0, 'Уникальный ИД элемента справочника. Например ygin-shop-reference-orderStatus-new  Префикс ygin зарезервирован для системных справочников, вместо него необходимо использовать название проекта или компании.', 1),
('28', '112', 7, 2, NULL, 'Справочник', 'id_reference', '27', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('28', '113', 1, 3, NULL, 'Значение элемента', 'id_reference_element', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, 'Идентификатор текущего значения справочника', 1),
('28', '114', 2, 4, NULL, 'Описание значения', 'value', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, 'Описывает текущее значение справочника и, как правило, отображается в списке', 1),
('28', '115', 2, 5, NULL, 'Картинка для значения', 'image_element', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('29', '48', 11, 8, NULL, 'id', 'id_group_system_parameter', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('29', '49', 2, 9, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('30', '116', 11, 8, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_system_parameter', '1', NULL, 1, 'name;-;parameter', 0, 0, 0, 0, 0, 'Уникальный ИД системного параметра. Например ygin-parameter-phone   Префикс ygin зарезервирован для системных параметров, вместо него необходимо использовать название проекта или компании.', 1),
('30', '117', 7, 8, NULL, 'Группа параметров', 'id_group_system_parameter', '29', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('30', '118', 2, 9, NULL, 'Имя для разработчика', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('30', '119', 2, 10, NULL, 'Значение параметра', 'value', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('30', '120', 2, 12, NULL, 'Описание', 'note', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('30', '125', 14, 280, NULL, 'Значение для больших текстовых данных (longtext)', 'long_text_value', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('30', '600', 7, 12, NULL, 'Тип\r\nданных', 'id_parameter_type', '22', '2', 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('31', '281', 11, 1, NULL, 'id', 'id_domain', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '282', 2, 6, NULL, 'Путь к содержимому домена', 'domain_path', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Заполняется для многодоменных сайтов.\r\nВсе загружаемые файлы для домена будут попадать относительно заданной директории.\r\nНапример, если для новостей указан путь к данным как "content/news", а в данном свойстве указано "my_domain", то файлы будут загружаться по такому пути: "/my_domain/content/news/1/sample.jpg". При этом в формируемых ссылках папка my_domain не включается.', 1),
('31', '283', 2, 2, NULL, 'Доменное имя', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '284', 2, 4, NULL, 'Описание', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '285', 2, 7, NULL, 'Путь к данным по http', 'path2data_http', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Заполняется для многодоменных сайтов.\r\nДополнительный путь, автоматически приписываемый ко всем загружаемым файлам домена при использовании метода getPath() у класса Files.', 1),
('31', '286', 10, 8, 'backend.backend.domain.DomainLocalizationVisualElement', 'Доступные локализации', NULL, '2', '', 0, 'DomainLocalizationVisualElement', 0, 0, 0, 0, 0, '', 1),
('31', '287', 1, 5, NULL, 'ID страницы по умолчанию', 'id_default_page', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '289', 14, 10, NULL, 'Настройки', 'settings', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '303', 2, 11, NULL, 'Ключевые слова', 'keywords', NULL, NULL, 0, NULL, 0, 0, 1, 0, 0, 'Ключевые слова подставляются в мета-тег keywords в случае отсутствия такового у раздела меню.', 1),
('31', '304', 9, 3, NULL, 'Активен', 'active', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('31', '638', 8, 12, NULL, 'Картинка для сохранения в закладках', 'image_src', '1', NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('33', '250', 11, 8, NULL, 'id', 'id_event_format', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('33', '31', 2, 8, NULL, 'Описание', 'description', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('33', '32', 6, 9, NULL, 'Расположение', 'place', '2', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('33', '33', 2, 10, NULL, 'Имя файла во вложении', 'file_name', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('33', '34', 2, 11, NULL, 'Сокращённое название (для разработчика)', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '37', 7, 2, NULL, 'Тип события', 'id_event_type', '35', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '38', 7, 4, NULL, 'Пользователь', 'id_user', '24', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '40', 7, 9, NULL, 'Формат сообщения', 'format', '33', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('34', '41', 9, 8, NULL, 'Архивировать ли вложение', 'archive_attach', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '42', 2, 6, NULL, 'E-mail адрес', 'email', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '43', 11, 3, NULL, 'id', 'id_event_subscriber', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('34', '44', 2, 7, NULL, 'Имя подписчика', 'name', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('35', '245', 7, 9, NULL, 'Объект для работы', 'id_object', '20', NULL, 0, '', 0, NULL, 0, 0, 0, NULL, 1),
('35', '246', 4, 10, NULL, 'Дата последей обработки', 'last_time', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('35', '248', 2, 13, NULL, 'SQL получающая экемпляры ("AS id_instance")', 'sql_condition', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('35', '249', 2, 14, NULL, 'SQL выражение, срабатывающее после обработки экземпляра (<<id_instance>>)', 'condition_done', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('35', '252', 1, 16, NULL, 'Интервал времени, через которое будет проходить обработка события', 'interval_value', NULL, '90', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('35', '266', 7, 8, NULL, 'Используемый почтовый аккаунт', 'id_mail_account', '50', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('35', '50', 11, 8, NULL, 'id', 'id_event_type', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('35', '51', 2, 8, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '128', 9, 112, NULL, 'Статус создания превью-файла', 'status_process', NULL, '0', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('37', '134', 11, 8, NULL, 'id', 'id_file', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '135', 2, 8, NULL, 'Путь к файлу', 'file_path', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '136', 7, 9, NULL, 'Тип файла', 'id_file_type', '39', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '137', 1, 10, NULL, 'Количество загрузок', 'count', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '138', 7, 11, NULL, 'Объект', 'id_object', '20', NULL, 0, '', 0, NULL, 0, 0, 0, NULL, 1),
('37', '139', 1, 12, NULL, 'ИД экземпляра', 'id_instance', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '230', 7, 17, NULL, 'Свойство объекта', 'id_parameter', '21', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '231', 7, 51, NULL, 'Пользовательское свойство', 'id_property', '41', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '232', 4, 22, NULL, 'Дата создания файла', 'create_date', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('37', '508', 2, 27, NULL, 'Временный ИД', 'id_tmp', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('37', '509', 7, 27, NULL, 'Родительский файл', 'id_parent_file', '37', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('39', '129', 11, 8, NULL, 'id', 'id_file_type', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('39', '130', 2, 8, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 1, 0, 0, NULL, 1),
('40', '131', 11, 8, NULL, 'id', 'id_file_extension', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('40', '132', 2, 8, NULL, 'Расширение', 'ext', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('40', '133', 7, 8, NULL, 'Тип файла', 'id_file_type', '39', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '290', 11, 8, NULL, 'ID_PROPERTY', 'ID_PROPERTY', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '292', 9, 9, NULL, 'Обязательно для заполнения', 'IS_NECESSARILY', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '293', 7, 10, NULL, 'Значение из справочника', 'ID_REFERENCE', '27', NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '294', 7, 11, NULL, 'Тип свойства', 'ID_PROPERTY_TYPE', '22', NULL, 1, 'id_parameter_type IN (1,2,3,4,6,7,8,9,14,17)', 0, NULL, 0, 0, 0, NULL, 1),
('41', '295', 7, 12, NULL, 'Объект', 'ID_OBJECT', '20', NULL, 1, '', 0, 0, 0, 0, 0, NULL, 1),
('41', '296', 2, 12, NULL, 'Значение по умолчанию', 'DEFAULT_VALUE', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '297', 7, 13, NULL, 'Значение берется из объекта', 'ID_SELECTOR_OBJECT', '20', NULL, 0, '', 0, NULL, 0, 0, 0, NULL, 1),
('41', '298', 2, 8, NULL, 'Описание', 'caption', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('41', '299', 2, 8, NULL, 'Имя свойства', 'NAME', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('49', '253', 11, 8, NULL, 'id', 'id_event', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('49', '254', 7, 8, NULL, 'Тип события', 'id_event_type', '35', NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('49', '255', 1, 10, NULL, 'ИД экземпляра', 'id_instance', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('49', '256', 4, 11, NULL, 'Дата создания события', 'event_create', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('49', '257', 14, 16, NULL, 'Содержимое', 'event_message', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '258', 11, 8, NULL, 'id', 'id_mail_account', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '259', 2, 8, NULL, 'E-mail для поля "От"', 'email_from', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '260', 2, 8, NULL, 'Имя отправителя', 'from_name', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '261', 2, 10, NULL, 'Тема по умолчанию', 'default_subject', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '262', 2, 12, NULL, 'HOST', 'host', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '263', 2, 14, NULL, 'Имя пользователя для авторизации', 'user_name', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '264', 9, 13, NULL, 'Требуется ли авторизация', 'smtp_auth', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('50', '265', 2, 15, NULL, 'Пароль для авторизации', 'user_password', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('500', '1500', 11, 1, NULL, 'ID', 'id_photogallery', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('500', '1501', 2, 2, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 1, 0, 0, NULL, 1),
('500', '1503', 3, 5, NULL, 'Текст в галерее', 'text_in_gallery', NULL, NULL, 0, NULL, 0, 0, 1, 0, 0, NULL, 1),
('500', '1504', 13, 7, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('500', '1505', 12, 6, NULL, 'Родительский раздел', 'id_parent', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('501', '1507', 11, 1, NULL, 'ID', 'id_photogallery_photo', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('501', '1508', 2, 4, NULL, 'Название', 'name', NULL, NULL, 0, NULL, 0, 0, 1, 0, 0, '', 1),
('501', '1509', 1, 3, NULL, 'Экземпляр-фотогалерея', 'id_photogallery_instance', '411', NULL, 1, NULL, 0, 1, 0, 0, 0, 'Экземпляр объекта, являющийся галереей фотографий', 1),
('501', '1510', 8, 6, NULL, 'Файл', 'file', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('501', '1511', 13, 7, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('501', '1512', 1, 2, NULL, 'Объект', 'id_photogallery_object', '20', NULL, 1, NULL, 0, 0, 0, 0, 0, 'Объект системы, экземпляр которого является фотогалереей', 1),
('501', '501-visible', 9, 345, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('502', '1513', 14, 8, NULL, 'Краткое содержание', 'short', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, '', 1),
('502', '1514', 7, 6, NULL, 'Категория', 'id_news_category', '503', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('502', '1515', 3, 11, NULL, 'Содержание', 'content', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, '', 1),
('502', '1516', 4, 5, NULL, 'Дата', 'date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('502', '1517', 2, 2, NULL, 'Заголовок', 'title', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, '', 1),
('502', '1518', 11, 1, NULL, 'ID', 'id_news', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('502', '1519', 8, 7, NULL, 'Картинка', 'photo', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, 'Позволяет прикреплять иллюстрацию к новости', 1),
('502', '1520', 9, 4, NULL, 'Видимость', 'is_visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('502', '1577', 15, 10, NULL, 'Загрузить файлы', NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('502', '502-meta-description', 14, 381, NULL, 'Описание <meta name="description">', 'meta_description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('502', '502-meta-keywords', 14, 380, NULL, 'Ключевые слова <meta name="keywords">', 'meta_keywords', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('502', '502-page-title', 2, 379, NULL, 'Заголовок <title>', 'page_title', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('503', '1521', 11, 52, NULL, 'id', 'id_news_category', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('503', '1522', 2, 53, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('503', '1523', 13, 54, NULL, 'п/п', 'seq', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('503', '1524', 9, 149, NULL, 'Видимость', 'is_visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1525', 11, 1, NULL, 'id', 'id_consultation_ask', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1526', 2, 2, NULL, 'ФИО спрашивающего', 'user_fio', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1527', 2, 3, NULL, 'E-mail спрашивающего', 'email', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1528', 14, 4, NULL, 'Вопрос', 'ask', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('505', '1529', 10, 5, 'project/plugin/consultation/ConsultationSpecializationVisualElement.php', 'Специализация вопроса', NULL, '2', NULL, 0, 'ConsultationSpecializationVisualElement', 0, 0, 0, 0, 0, NULL, 1),
('505', '1530', 4, 6, NULL, 'Дата вопроса', 'ask_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1531', 2, 7, NULL, 'IP спрашивающего', 'ip', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1532', 9, 147, NULL, 'Видимость', 'is_visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('505', '1587', 8, 0, NULL, 'Приложение', 'attachment', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('506', '1533', 11, 1, NULL, 'id', 'id_consultation_answer', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('506', '1534', 7, 2, NULL, 'Отвечающий', 'id_consultation_answerer', '507', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('506', '1535', 2, 3, NULL, 'Отвечающий (ручной ввод)', 'answerer', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('506', '1536', 4, 4, NULL, 'Дата ответа', 'answer_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('506', '1537', 7, 5, NULL, 'На вопрос', 'id_consultation_ask', '505', NULL, 1, NULL, 0, 1, 0, 0, 0, 'Указываем вопрос, на который даётся ответ', 1),
('506', '1538', 3, 6, NULL, 'Ответ', 'answer', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('506', '1539', 2, 7, NULL, 'IP отвечающего', 'ip', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1540', 10, 6, 'project/plugin/consultation/AnswererSpecializationVisualElement.php', 'Специализация', NULL, '2', NULL, 0, 'AnswererSpecializationVisualElement', 0, 0, 0, 0, 0, NULL, 1),
('507', '1541', 3, 9, NULL, 'Полное описание', 'full_info', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1542', 3, 8, NULL, 'Краткое описание', 'short_info', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('507', '1543', 2, 4, NULL, 'Подпись после ФИО', 'caption_after', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1544', 8, 7, NULL, 'Фото', 'photo', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1545', 2, 2, NULL, 'Подпись перед ФИО', 'caption_before', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1546', 2, 5, NULL, 'e-mail', 'email', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('507', '1547', 2, 3, NULL, 'ФИО отвечающего', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('507', '1548', 11, 1, NULL, 'id', 'id_consultation_answerer', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('508', '1549', 14, 81, NULL, 'Описание', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('508', '1550', 11, 79, NULL, 'id', 'id_consultation_specialization', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('508', '1551', 2, 80, NULL, 'Специализация', 'specialization', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('509', '1552', 11, 2, NULL, 'id', 'id_product_category', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('509', '1553', 2, 3, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('509', '1554', 12, 5, NULL, 'Родитель', 'id_parent', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('509', '1555', 8, 6, NULL, 'Изображение', 'image', '1', NULL, 0, NULL, 0, 0, 0, 0, 1, 'Изображение категории каталога', 1),
('509', '1557', 1, 7, NULL, 'Наценка', 'price_markup', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, 'Наценка на оптовую цену перед выводом на сайт', 1),
('509', '1636', 13, 1, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('509', '1637', 9, 4, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('51', '267', 11, 1, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_job', '1', NULL, 1, 'name;-;job', 0, 0, 0, 0, 0, 'Уникальный ИД задачи планировщика. Например для отправки уведомлений это будет ygin-job-sendMail   Префикс ygin зарезервирован для системных задач, вместо него необходимо использовать название проекта или компании.', 1),
('51', '268', 1, 4, NULL, 'Интервал запуска задачи (в секундах)', 'interval_value', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('51', '269', 1, 5, NULL, 'Интервал запуска задачи в случае ошибки (в секундах)', 'error_repeat_interval', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('51', '270', 4, 6, NULL, 'Дата первого запуска', 'first_start_date', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 1, NULL, 1),
('51', '271', 4, 7, NULL, 'Дата последнего запуска', 'last_start_date', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('51', '272', 4, 8, NULL, 'Дата следущего запуска', 'next_start_date', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('51', '273', 1, 9, NULL, 'Количество ошибок', 'failures', NULL, '0', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('51', '274', 2, 2, NULL, 'Имя задачи', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('51', '276', 2, 10, NULL, 'Имя класса задачи', 'class_name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('51', '291', 9, 3, NULL, 'Вкл.', 'active', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('51', '300', 1, 11, NULL, 'Приоритет запуска', 'priority', NULL, '0', 1, NULL, 0, 0, 0, 0, 1, NULL, 1),
('51', '301', 4, 12, NULL, 'Дата запуска текущего потока', 'start_date', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('51', '302', 1, 13, NULL, 'Максимальное число секунд выполнения задачи', 'max_second_process', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, 'NULL или 0 - без ограничений', 1),
('511', '1564', 11, 1, NULL, 'id', 'id_product', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1565', 7, 2, NULL, 'Каталог продукции', 'id_product_category', '509', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('511', '1566', 2, 3, NULL, 'Артикул', 'code', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, 'Артикул, внутренний код и т.п.', 1),
('511', '1567', 2, 4, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('511', '1568', 1, 7, NULL, 'Оптовая цена', 'trade_price', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1569', 1, 8, NULL, 'Мал. оптовая цена', 'sm_trade_price', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1570', 1, 9, NULL, 'Розничная цена', 'retail_price', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1571', 2, 10, NULL, 'Единица измерения', 'unit', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1573', 2, 11, NULL, 'Остаток', 'remain', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1574', 14, 13, NULL, 'Описание товара', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('511', '1576', 8, 6, NULL, 'Изображение', 'image', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1616', 9, 14, NULL, 'Удален', 'deleted', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1617', 3, 15, NULL, 'Характеристики', 'properties', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('511', '1619', 3, 16, NULL, 'Монтаж', 'additional_desc', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('511', '1663', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1671', 7, 12, NULL, 'Брэнд', 'id_brand', '525', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('511', '1691', 14, 0, NULL, 'Видео', 'video', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, 'HTML-код видео (напр. с сайта youtube.com)', 1),
('512', '1578', 11, 1, NULL, 'id', 'id_question', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('512', '1579', 4, 2, NULL, 'Дата', 'ask_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('512', '1580', 2, 3, NULL, 'Спрашивающий', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('512', '1581', 14, 5, NULL, 'Вопрос', 'question', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('512', '1582', 2, 4, NULL, 'E-mail', 'email', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('512', '1583', 3, 7, NULL, 'Ответ', 'answer', NULL, NULL, 0, NULL, 0, 0, 0, 1, 0, NULL, 1),
('512', '1585', 9, 6, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('512', 'ygin-faq-category', 6, 0, NULL, 'Категория', 'category', 'ygin-faq-reference-categoryQuestion', '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('512', 'ygin-faq-send', 9, 0, NULL, 'Отправить ответ на email', 'send', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('517', '1609', 11, 1, NULL, 'id', 'id_feedback', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('517', '1610', 2, 0, NULL, 'ФИО', 'fio', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('517', '1611', 2, 0, NULL, 'Телефон', 'phone', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('517', '1612', 2, 257, NULL, 'e-mail', 'mail', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('517', '1613', 14, 0, NULL, 'Сообщение', 'message', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('517', '1614', 4, 0, NULL, 'Дата сообщения', 'date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('517', '1615', 2, 0, NULL, 'ip', 'ip', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1620', 11, 1, NULL, 'id', 'id_offer', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('519', '1621', 2, 4, NULL, 'ФИО', 'fio', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1622', 2, 5, NULL, 'Телефон', 'phone', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1623', 2, 6, NULL, 'e-mail', 'mail', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1624', 14, 7, NULL, 'Пожелания', 'comment', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1625', 14, 8, NULL, 'Заказ', 'offer_text', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1626', 4, 2, NULL, 'Дата заказа', 'create_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1627', 9, 3, NULL, 'Обработано', 'is_process', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', '1628', 2, 9, NULL, 'ip', 'ip', NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, 1),
('519', '1629', 9, 10, NULL, 'Отправлено ли уведомление', 'is_send', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('519', '1665', 6, 0, NULL, 'Статус', 'status', '101', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('519', 'ygin-offer-amount', 1, 297, NULL, 'Сумма', 'amount', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('519', 'ygin-offer-id-invoice', 7, 298, NULL, 'Счет', 'id_invoice', 'ygin-invoice', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('520', '1630', 11, 1, NULL, 'id', 'id_vitrine', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('520', '1631', 2, 3, NULL, 'Ссылка на переход', 'link', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('520', '1632', 2, 4, NULL, 'Заголовок', 'title', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('520', '1633', 14, 5, NULL, 'Дополнительный текст', 'text', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('520', '1634', 8, 2, NULL, 'Фото', 'image', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('520', '1635', 13, 0, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('521', '1638', 11, 1, NULL, 'id', 'id_quiz', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('521', '1639', 2, 0, NULL, 'name', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('521', '1640', 15, 0, NULL, 'Файлы', NULL, '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('521', '1641', 3, 0, NULL, 'description', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('521', '1642', 9, 0, NULL, 'active', 'active', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('522', '1643', 11, 1, NULL, 'id', 'id_quiz_question', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('522', '1644', 7, 269, NULL, 'Викторина', 'id_quiz', '521', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('522', '1645', 15, 0, NULL, 'Файлы', NULL, '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('522', '1646', 3, 271, NULL, 'Текст вопроса', 'question', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('522', '1647', 6, 272, NULL, 'Тип ответов', 'type', '100', '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('522', '1648', 13, 0, NULL, 'п/п', 'sequence', '521', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('523', '1649', 11, 1, NULL, 'id', 'id_quiz_answer', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('523', '1650', 7, 0, NULL, 'Вопрос', 'id_quiz_question', '522', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('523', '1651', 2, 0, NULL, 'Текст ответа', 'answer', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('523', '1652', 9, 0, NULL, 'Правильный ли ответ', 'is_right', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('523', '1653', 13, 0, NULL, 'п/п', 'sequence', '522', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1654', 11, 1, NULL, 'id', 'id_quiz_answer_user', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('524', '1655', 7, 278, NULL, 'Викторина', 'id_quiz', '521', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('524', '1656', 2, 279, NULL, 'ФИО', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1657', 2, 0, NULL, 'mail', 'mail', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1658', 2, 0, NULL, 'Читательский билет', 'library_card', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1659', 2, 0, NULL, 'Контактная информация', 'contact', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1660', 14, 0, NULL, 'Ответ', 'answer', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1661', 4, 0, NULL, 'Дата', 'create_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('524', '1662', 2, 0, NULL, 'ip', 'ip', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('525', '1666', 11, 1, NULL, 'id', 'id_brand', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('525', '1667', 2, 2, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 1, 0, NULL, 1),
('525', '1668', 12, 4, NULL, 'Родительский брэнд', 'id_parent', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('525', '1669', 8, 3, NULL, 'Логотип брэнда', 'image', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('525', '1670', 13, 0, NULL, 'п/п', 'sequence', '-1', NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('527', '1673', 11, 1, NULL, 'id', 'id_plugin', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('527', '1674', 2, 0, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('527', '1675', 2, 0, NULL, 'Код', 'code', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('527', '1676', 1, 282, NULL, 'Статус', 'status', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('527', '1677', 14, 283, NULL, 'Сериализованные настройки', 'config', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO `da_object_parameters` (`id_object`, `id_parameter`, `id_parameter_type`, `sequence`, `widget`, `caption`, `field_name`, `add_parameter`, `default_value`, `not_null`, `sql_parameter`, `is_unique`, `group_type`, `need_locale`, `search`, `is_additional`, `hint`, `visible`) VALUES
('527', '1678', 2, 0, NULL, 'Класс плагина', 'class_name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('527', '1681', 14, 286, NULL, 'data', 'data', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('529', '1679', 11, 1, NULL, 'id', 'id_remain_status', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('529', '1680', 2, 0, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('529', '1682', 1, 0, NULL, 'Макс. значение по-умолчанию', 'max_value', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('529', '1683', 1, 0, NULL, 'Мин. значение по-умолчанию', 'min_value', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('529', '1692', 2, 0, NULL, 'Иконка', 'icon', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1684', 11, 1, NULL, 'id', 'id_client_review', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('530', '1685', 2, 0, NULL, 'ФИО', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1686', 4, 0, NULL, 'Дата', 'create_date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1687', 14, 0, NULL, 'Текст отзыва', 'review', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1688', 2, 0, NULL, 'ip', 'ip', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1689', 9, 0, NULL, 'Видимость на сайте', 'visible', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('530', '1690', 2, 0, NULL, 'Контакты клиента', 'contact', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('531', '1693', 11, 1, NULL, 'id', 'id_message', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('531', '1694', 14, 0, NULL, 'Текст', 'text', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('531', '1695', 4, 0, NULL, 'Дата создания', 'date', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('531', '1696', 1, 0, NULL, 'тип', 'type', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('54', '310', 11, 8, NULL, 'id', 'id_localization', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('54', '311', 2, 8, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('54', '312', 2, 8, NULL, 'Код', 'code', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('54', '313', 9, 12, NULL, 'Используется ли локализация', 'is_use', NULL, '1', 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '140', 11, 9, NULL, 'id', 'id_instruction', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '141', 2, 10, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '142', 3, 13, NULL, 'Описание', 'content', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '143', 9, 12, NULL, 'Относится только к этому сайту', 'desc_type', NULL, '0', 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '144', 9, 11, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('61', '145', 13, 8, NULL, 'п/п', 'num_seq', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('63', '400', 11, 2, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_object_view', '1', NULL, 1, 'name; id_object; view', 0, 0, 0, 0, 0, 'Уникальный ИД представления объекта в виде строки. Обычно имеет имя ygin-news-view-main  Префикс ygin зарезервирован для системных представлений, вместо него необходимо использовать название проекта или компании.', 1),
('63', '401', 7, 5, NULL, 'Объект', 'id_object', '20', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('63', '402', 2, 8, NULL, 'SELECT', 'sql_select', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('63', '403', 2, 9, NULL, 'FROM', 'sql_from', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('63', '404', 2, 10, NULL, 'WHERE', 'sql_where', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('63', '405', 2, 11, NULL, 'ORDER BY', 'sql_order_by', NULL, NULL, 0, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('63', '406', 2, 1, NULL, 'Имя представления', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('63', '407', 1, 12, NULL, 'Экземпляров на странице', 'count_data', '0', '50', 1, NULL, 0, 0, 0, 0, 0, 'Определяет количество отображаемых экземпляров объекта на одной странице', 1),
('63', '408', 13, 6, NULL, 'п/п', 'order_no', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('63', '409', 9, 7, NULL, 'Видимость', 'visible', '0', '1', 1, NULL, 0, 0, 0, 0, 0, 'Определяет видимость представления в меню системы управления. В случае отсутствия видимости, представление остаётся доступным по прямой ссылке.', 1),
('63', '410', 2, 3, NULL, 'css-класс иконки', 'icon_class', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Css-класс иконки из Twitter Bootstrap, который будет отображаться в меню', 1),
('63', '411', 2, 4, NULL, 'Иерархия по полю', 'id_parent', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Родительское поле для построения иерархии данных', 1),
('63', 'ygin-object-view-description', 14, 292, NULL, 'Описание представления', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Данное описание будет выводится в списке экземпляров сразу после заголовка представления. Служит для подробного описания назначения объекта и представления.', 1),
('66', '415', 11, 1, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_object_view_column', '1', NULL, 1, 'field_name/caption; id_object_view', 0, 0, 0, 0, 0, 'Уникальный ИД колонки представления. Например для заголовка новости ИД будет равен ygin-news-view-main-title    Префикс ygin зарезервирован для системных колонок, вместо него необходимо использовать название проекта или компании.', 1),
('66', '416', 7, 2, NULL, 'Представление', 'id_object_view', '63', NULL, 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('66', '417', 7, 3, NULL, 'Объект', 'id_object', '20', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('66', '418', 2, 4, NULL, 'Заголовок', 'caption', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('66', '419', 13, 6, NULL, 'п/п', 'order_no', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('66', '420', 7, 7, 'backend.backend.objectParameter.selectObjectParameterWidget.SelectObjectParameterWidget', 'Свойство объекта', 'id_object_parameter', '21', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('66', '421', 7, 8, NULL, 'Тип данных', 'id_data_type', '22', '2', 1, 'id_parameter_type IN (1, 2, 3, 4, 6, 7, 8, 9, 10, 14)', 0, 0, 0, 0, 0, NULL, 1),
('66', '422', 2, 9, NULL, 'Имя поля', 'field_name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('66', '423', 7, 10, NULL, 'Обработчик', 'handler', '80', NULL, 0, 'id_php_script_interface IN (6, 7)', 0, 0, 0, 0, 0, 'Особый зарегистрированный в системе скрипт, который будет формировать колонку', 1),
('66', '424', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('80', '334', 9, 4, NULL, 'Активен', 'active', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('80', '337', 7, 2, NULL, 'Интерфейс', 'id_php_script_interface', '86', '1', 1, NULL, 0, 1, 0, 0, 0, NULL, 1),
('80', '338', 11, 1, 'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget', 'id', 'id_php_script_type', '1', NULL, 1, 'description; -; php', 0, 0, 0, 0, 0, 'Уникальный ИД пхп обработчика. Например для модуля новостей это будет ygin-news-module-last  Префикс ygin зарезервирован для системных обработчиков, вместо него необходимо использовать название проекта или компании.', 1),
('80', '339', 2, 5, NULL, 'Алиас', 'file_path', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('80', '342', 2, 3, NULL, 'Название скрипта', 'description', NULL, NULL, 1, NULL, 0, NULL, 0, 0, 0, NULL, 1),
('86', '347', 11, 1, NULL, 'id', 'id_php_script_interface', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('86', '348', 2, 3, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('86', '350', 14, 4, NULL, 'Шаблон файла', 'template', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 'Не надо открывать и закрывать php (<?php ?>), пишется только тело кода.\r\nДопускаются следующие переменные шаблона:\r\n<<class_name>> - имя класса', 1),
('86', '351', 13, 2, NULL, 'Порядок', 'sequence', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, NULL, 1),
('project-bloki-kontenta', 'project-bloki-kontenta-explanation', 2, 308, NULL, 'Объяснение', 'explanation', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-bloki-kontenta', 'project-bloki-kontenta-html-ru', 3, 312, NULL, 'html контент по русски', 'html', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-bloki-kontenta', 'project-bloki-kontenta-id-app-block-content', 11, 1, NULL, 'id', 'id_app_block_content', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-bloki-kontenta', 'project-bloki-kontenta-name', 2, 307, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-bloki-kontenta', 'project-bloki-kontenta-state', 9, 309, NULL, 'Видимость', 'state', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-bloki-kontenta', 'project-bloki-kontenta-text-ru', 14, 310, NULL, 'Обычный текст по русски', 'text', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-datetime', 4, 306, NULL, 'Дата отправки', 'datetime', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-email', 2, 304, NULL, 'Ваш e-mail', 'email', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-id-app-form-feedback', 11, 1, NULL, 'id', 'id_app_form_feedback', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-name', 2, 302, NULL, 'Ваше имя', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-phone', 2, 303, NULL, 'Ваш телефон', 'phone', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-obratnoi-svyazi', 'project-forma-obratnoi-svyazi-text', 14, 305, NULL, 'Текст сообщения', 'text', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-zakaza-zvonka', 'project-forma-zakaza-zvonka-datetime', 4, 301, NULL, 'Дата отправки', 'datetime', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-zakaza-zvonka', 'project-forma-zakaza-zvonka-id-app-form-callback', 11, 1, NULL, 'id', 'id_app_form_callback', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-forma-zakaza-zvonka', 'project-forma-zakaza-zvonka-name', 2, 299, NULL, 'Ваше имя', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-forma-zakaza-zvonka', 'project-forma-zakaza-zvonka-phone', 2, 300, NULL, 'Ваш телефон', 'phone', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-fotoalbomy', 'project-fotoalbomy-alias', 2, 4, NULL, 'В адресной строке', 'alias', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-fotoalbomy', 'project-fotoalbomy-id-app-photoalbom', 11, 1, NULL, 'id', 'id_app_photoalbum', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-fotoalbomy', 'project-fotoalbomy-name-ru', 2, 2, 'menu.backend.widgets.menuName.MenuNameWidget', 'Название альбома', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-fotoalbomy', 'project-fotoalbomy-sequence', 13, 6, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-fotoalbomy', 'project-fotoalbomy-visible', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-kontakty', 'project-kontakty-content-ru', 3, 368, NULL, 'Содержание', 'content', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-kontakty', 'project-kontakty-id-app-contact', 11, 1, NULL, 'id', 'id_app_contact', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-slaider', 'project-slaider-id-app-main-slider', 11, 1, NULL, 'id', 'id_app_main_slider', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-slaider', 'project-slaider-image', 8, 371, NULL, 'Картинка', 'image', '1', NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-sequence', 13, 374, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-subtitle', 2, 370, NULL, 'Подзаголовок', 'subtitle', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-text', 14, 372, NULL, 'Текст (описание)', 'text', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-title', 2, 369, NULL, 'Заголовок', 'title', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-url', 2, 373, NULL, 'Ссылка', 'url', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-slaider', 'project-slaider-visible', 9, 375, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-avatar', 8, 7, NULL, 'Аватар', 'avatar', '1', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-description', 14, 8, NULL, 'Описание', 'description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-experience', 2, 4, NULL, 'Опыт работы', 'experience', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-id-app-specialist', 11, 1, NULL, 'id', 'id_app_specialist', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-spezialisty', 'project-spezialisty-job-title', 2, 3, NULL, 'Должность', 'job_title', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-meta-description', 14, 12, NULL, 'Описание <meta name="description">', 'meta_description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-spezialisty', 'project-spezialisty-meta-keywords', 14, 11, NULL, 'Ключевые слова <meta name="keywords">', 'meta_keywords', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-spezialisty', 'project-spezialisty-name', 2, 2, NULL, 'Имя (ФИО)', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-on-main', 9, 6, NULL, 'Отображать на главной странице', 'on_main', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-page-title', 2, 10, NULL, 'Заголовок <title>', 'page_title', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-spezialisty', 'project-spezialisty-sequence', 13, 9, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-spezialisty', 'project-spezialisty-visible', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-alias', 2, 3, NULL, 'В адресной строке', 'alias', NULL, NULL, 1, NULL, 1, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-content', 3, 8, NULL, 'Контент', 'content', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-id-app-services', 11, 1, NULL, 'id', 'id_app_services', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-uslugi', 'project-uslugi-image', 8, 5, NULL, 'Картинка на фон блока', 'image', '1', NULL, 1, NULL, 0, 0, 0, 0, 0, 'Картинка на фоне блока услуги в виджете на главной странице', 1),
('project-uslugi', 'project-uslugi-meta-description', 14, 378, NULL, 'Описание <meta name="description">', 'meta_description', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-uslugi', 'project-uslugi-meta-keywords', 14, 377, NULL, 'Ключевые слова <meta name="keywords">', 'meta_keywords', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-uslugi', 'project-uslugi-name', 2, 2, 'menu.backend.widgets.menuName.MenuNameWidget', 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-on-main', 9, 7, NULL, 'Отображать на главной странице', 'on_main', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-page-title', 2, 376, NULL, 'Заголовок <title>', 'page_title', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('project-uslugi', 'project-uslugi-sequence', 13, 9, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-uslugi', 'project-uslugi-style', 6, 6, NULL, 'Внешний вид блока услуги', 'style', 'project-reference-vneschnii-vid-bloka-uslugi', NULL, 1, NULL, 0, 0, 0, 0, 0, 'Это внешний вид блока услуги в виджете на главной странице (синий, зеленый или белый)', 1),
('project-uslugi', 'project-uslugi-visible', 9, 4, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vidoalbom', 'project-vidoalbom-id-app-videoalbum', 11, 1, NULL, 'id', 'id_app_videoalbum', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-vidoalbom', 'project-vidoalbom-name-ru', 2, 2, NULL, 'Название', 'name', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vidoalbom', 'project-vidoalbom-sequence', 13, 6, NULL, 'п/п', 'sequence', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vidoalbom', 'project-vidoalbom-url', 2, 4, NULL, 'Ссылка на видео', 'url', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, 'В данной версии сайта поддерживается хостинг youtube.com. Ссылки на видео должны быть такого формата: http://youtube.com/watch?v=_5r_OIA3IeQ ', 1),
('project-vidoalbom', 'project-vidoalbom-visible', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-answer', 3, 10, NULL, 'Ответ', 'answer', NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-datetime', 4, 8, NULL, 'Дата отправки', 'datetime', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-email', 2, 4, NULL, 'Email', 'email', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-id-app-question-answer', 11, 1, NULL, 'id', 'id_app_question_answer', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('project-vopros-otvet', 'project-vopros-otvet-id-app-specialist', 7, 2, NULL, 'Специалист', 'id_app_specialist', 'project-spezialisty', NULL, 1, NULL, 0, 1, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-immya', 2, 3, NULL, 'Отправитель', 'immya', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-question', 14, 9, NULL, 'Вопрос', 'question', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-send', 9, 7, NULL, 'Отправить на email спрашивающего', 'send', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, 'Выберите этот пункт для однократной отправки вашего ответа на email спрашивающего', 1),
('project-vopros-otvet', 'project-vopros-otvet-theme', 2, 5, NULL, 'Тема вопроса', 'theme', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('project-vopros-otvet', 'project-vopros-otvet-visible', 9, 6, NULL, 'Видимость', 'visible', NULL, '0', 1, NULL, 0, 0, 0, 0, 0, '', 1),
('ygin-invoice', 'ygin-invoice-amount', 1, 295, NULL, 'Сумма', 'amount', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('ygin-invoice', 'ygin-invoice-create-date', 4, 293, NULL, 'Дата создания', 'create_date', '0', NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('ygin-invoice', 'ygin-invoice-id-invoice', 11, 1, NULL, 'id', 'id_invoice', NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, 1),
('ygin-invoice', 'ygin-invoice-id-offer', 7, 296, NULL, 'Заказ', 'id_offer', '519', NULL, 1, NULL, 0, 0, 0, 0, 0, '', 1),
('ygin-invoice', 'ygin-invoice-pay-date', 4, 294, NULL, 'Дата оплаты', 'pay_date', '0', NULL, 0, NULL, 0, 0, 0, 0, 0, '', 1),
('ygin-menu', '1', 11, 1, NULL, 'id', 'id', NULL, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('ygin-menu', '10', 6, 16, NULL, 'Дополнительные опции обработки контента:', 'go_to_type', '33', '1', 1, NULL, 0, 0, 0, 0, 0, 'Определяет дополнительные действия для содержимого раздела', 1),
('ygin-menu', '11', 8, 23, NULL, 'Картинка для раздела', 'image', '1', NULL, 0, NULL, 0, 0, 0, 0, 1, NULL, 1),
('ygin-menu', '127', 2, 3, NULL, 'В адресной строке', 'alias', '0', NULL, 0, NULL, 1, 0, 0, 0, 0, 'Обязательно на английском языке. Необходимо для формирования человекопонятных URLов.', 1),
('ygin-menu', '15', 7, 17, NULL, 'Набор виджетов', 'id_module_template', '101', 'SiteModuleTemplate::getIdDefaultTemplate();', 0, NULL, 0, 1, 0, 0, 1, '', 1),
('ygin-menu', '152', 2, 19, NULL, '<title>', 'title_teg', NULL, NULL, 0, NULL, 0, 0, 1, 1, 1, 'При отсутствии значения, заполняется автоматически заголовком раздела', 1),
('ygin-menu', '153', 2, 21, NULL, '<meta name="description">', 'meta_description', NULL, NULL, 0, NULL, 0, 0, 1, 0, 1, NULL, 1),
('ygin-menu', '154', 2, 20, NULL, '<meta name="keywords">', 'meta_keywords', NULL, NULL, 0, NULL, 0, 0, 1, 1, 1, NULL, 1),
('ygin-menu', '2', 2, 2, 'menu.backend.widgets.menuName.MenuNameWidget', 'Название в меню', 'name', '0', 'Имя раздела', 1, NULL, 0, 0, 1, 1, 0, 'Краткое название раздела, отображаемое в меню сайта', 1),
('ygin-menu', '3', 2, 4, NULL, 'Заголовок раздела', 'caption', '0', NULL, 0, NULL, 0, 0, 1, 1, 0, 'Заголовок не виден в меню сайта, но отображается крупно над текстом в самом разделе. Заголовок обычно длиннее названия в меню.', 1),
('ygin-menu', '330', 9, 22, NULL, 'Разрешить удаление', 'removable', '0', '1', 1, NULL, 0, 0, 0, 0, 1, 'Актуально для динамических разделов, удаление которых приводит к возникновению критических ошибок.', 1),
('ygin-menu', '4', 3, 13, NULL, 'Содержимое раздела', 'content', NULL, NULL, 0, NULL, 0, 0, 1, 1, 0, '', 1),
('ygin-menu', '5', 9, 5, NULL, 'Видимость', 'visible', NULL, '1', 0, NULL, 0, 0, 0, 0, 0, 'Раздел не отображается в меню, но остаётся доступным по пряму адресу (URL)', 1),
('ygin-menu', '549', 10, 11, 'menu.backend.widgets.externalLink.ExternalLinkWidget', 'Ссылка на страницу', 'external_link', '2', '', 0, 'MenuHref', 0, 0, 0, 0, 1, 'При отображении в меню раздел будет иметь указанную ссылку. Используется для создания внешних ссылок в меню или прямых ссылок на файл (например, прайс).', 1),
('ygin-menu', '6', 12, 18, NULL, 'Смена родительского раздела', 'id_parent', '1', NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('ygin-menu', '7', 13, 15, NULL, '&nbsp;', 'sequence', '-1', NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 1),
('ygin-menu', '8', 2, 14, NULL, 'Примечания', 'note', NULL, NULL, 0, NULL, 0, 0, 1, 0, 1, 'Нигде не отображаются, исключительно для комментариев в системе администрирования', 1),
('ygin-menu', 'ygin-menu-css-class', 2, 9, NULL, 'CSS Класс обертки ссылки', 'css_class', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('ygin-menu', 'ygin-menu-css-class-a', 2, 10, NULL, 'CSS Класс ссылки', 'css_class_a', NULL, NULL, 0, NULL, 0, 0, 0, 0, 1, '', 1),
('ygin-menu', 'ygin-menu-type-main-menu', 9, 6, NULL, 'В главном меню', 'type_main_menu', NULL, '1', 1, NULL, 0, 0, 0, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_parameter_type`
--

CREATE TABLE IF NOT EXISTS `da_object_parameter_type` (
  `id_parameter_type` int(8) NOT NULL COMMENT 'ИД',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT 'Порядок',
  `name` varchar(100) NOT NULL COMMENT 'Название типа данных',
  `sql_type` varchar(255) DEFAULT NULL COMMENT 'sql тип',
  PRIMARY KEY (`id_parameter_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы данных';

--
-- Дамп данных таблицы `da_object_parameter_type`
--

INSERT INTO `da_object_parameter_type` (`id_parameter_type`, `sequence`, `name`, `sql_type`) VALUES
(1, 2, 'Число', 'INT(8)'),
(2, 3, 'Строка', 'VARCHAR(255)'),
(3, 7, 'HTML-редактор', 'LONGTEXT'),
(4, 4, 'Дата', 'INT(10) UNSIGNED'),
(6, 10, 'Справочник', 'INT(8)'),
(7, 11, 'Внешний ключ (Объект)', 'INT(8)'),
(8, 8, 'Файл', 'INT(8)'),
(9, 5, 'Логический (bool)', 'TINYINT(1)'),
(10, 12, 'Абстрактный', NULL),
(11, 1, 'Первичный ключ', 'INT(8)'),
(12, 14, 'Родительский ключ', 'INT(8)'),
(13, 17, 'Последовательность', 'INT(8)'),
(14, 6, 'Текст (text area)', 'LONGTEXT'),
(15, 9, 'Список файлов', NULL),
(17, 16, 'Скрытое поле', 'VARCHAR(255)');

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_property`
--

CREATE TABLE IF NOT EXISTS `da_object_property` (
  `ID_PROPERTY` int(8) NOT NULL COMMENT 'ID_PROPERTY',
  `NAME` varchar(255) NOT NULL COMMENT 'Имя свойства',
  `IS_NECESSARILY` int(1) NOT NULL DEFAULT '0' COMMENT 'Обязательно для заполнения',
  `ID_REFERENCE` int(8) DEFAULT NULL COMMENT 'Значение из справочника',
  `ID_PROPERTY_TYPE` int(2) NOT NULL DEFAULT '0' COMMENT 'Тип свойства',
  `ID_OBJECT` int(8) NOT NULL DEFAULT '0' COMMENT 'Объект',
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL COMMENT 'Значение по умолчанию',
  `ID_SELECTOR_OBJECT` int(8) DEFAULT NULL COMMENT 'Значение берется из объекта',
  `caption` varchar(255) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (`ID_PROPERTY`),
  KEY `ID_OBJECT` (`ID_OBJECT`,`ID_PROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Пользовательские свойства';

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_property_value`
--

CREATE TABLE IF NOT EXISTS `da_object_property_value` (
  `ID_PROPERTY` int(8) NOT NULL DEFAULT '0',
  `ID_REFERENCE_ELEMENT` int(8) DEFAULT NULL,
  `VALUE` text,
  `ID_OBJECT_INSTANCE_VAL` int(8) DEFAULT NULL,
  `id_object_instance` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_PROPERTY`,`id_object_instance`),
  KEY `ID_PROPERTY` (`ID_PROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_view`
--

CREATE TABLE IF NOT EXISTS `da_object_view` (
  `id_object_view` varchar(255) NOT NULL COMMENT 'id',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `name` varchar(255) NOT NULL COMMENT 'Имя представления',
  `order_no` int(8) DEFAULT '1' COMMENT 'п/п',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость в панели',
  `sql_select` varchar(255) DEFAULT NULL COMMENT 'SELECT',
  `sql_from` varchar(255) DEFAULT NULL COMMENT 'FROM',
  `sql_where` varchar(255) DEFAULT NULL COMMENT 'WHERE',
  `sql_order_by` varchar(255) DEFAULT NULL COMMENT 'ORDER BY',
  `count_data` int(8) NOT NULL DEFAULT '50' COMMENT 'Количество отображаемых результатов на страницу',
  `icon_class` varchar(255) DEFAULT NULL COMMENT 'css-класс иконки',
  `id_parent` varchar(255) DEFAULT NULL COMMENT 'Иерархия по полю',
  `description` longtext COMMENT 'Описание представления',
  PRIMARY KEY (`id_object_view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Представление';

--
-- Дамп данных таблицы `da_object_view`
--

INSERT INTO `da_object_view` (`id_object_view`, `id_object`, `name`, `order_no`, `visible`, `sql_select`, `sql_from`, `sql_where`, `sql_order_by`, `count_data`, `icon_class`, `id_parent`, `description`) VALUES
('10', '28', 'Значения справочника', 1, 0, NULL, NULL, NULL, 'id_reference ASC', 50, NULL, NULL, NULL),
('100', '261', 'Баннерные места', 1, 1, NULL, NULL, NULL, 'sequence ASC', 50, 'glyphicon glyphicon-import', 'id_parent', NULL),
('101', '260', 'Баннеры', 1, 0, NULL, NULL, NULL, 'sequence ASC', 50, NULL, NULL, NULL),
('11', '29', 'Группы системных параметров', 1, 1, NULL, NULL, NULL, 'id_group_system_parameter DESC', 50, NULL, NULL, NULL),
('12', '30', 'Настройки сайта', 1, 1, NULL, NULL, NULL, 'id_group_system_parameter ASC', 50, 'glyphicon glyphicon-cog', NULL, NULL),
('13', '31', 'Домены сайта', 1, 1, NULL, NULL, NULL, 'id_domain ASC', 50, 'glyphicon glyphicon-pushpin', NULL, NULL),
('15', '33', 'Формат сообщения', 1, 1, NULL, NULL, NULL, 'id_event_format DESC', 50, NULL, NULL, NULL),
('16', '34', 'Подписчики на события', 1, 1, NULL, NULL, NULL, NULL, 50, 'glyphicon glyphicon-envelope', NULL, NULL),
('17', '35', 'Тип события', 1, 1, NULL, NULL, NULL, 'id_event_type DESC', 50, NULL, NULL, NULL),
('19', '37', 'Файлы', 1, 1, NULL, NULL, NULL, 'id_file ASC', 50, NULL, NULL, NULL),
('2', '20', 'Объекты', 1, 1, NULL, NULL, NULL, 'sequence ASC', 50, 'glyphicon glyphicon-th-large', 'parent_object', NULL),
('2001', '501', 'Фотографии', 1, 0, NULL, NULL, NULL, 'id_photogallery_photo ASC', 50, NULL, NULL, NULL),
('2002', '502', 'Новости', 1, 1, NULL, NULL, NULL, 'date DESC', 50, NULL, NULL, ''),
('2003', '503', 'Новости » Категории', 1, 1, NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL),
('2004', '505', 'Консультации » Вопросы', 1, 0, 'IF(LENGTH(ask)<300, ask, CONCAT(SUBSTR(ask, 1, 300), ''...'')) AS short_ask', NULL, NULL, 'ask_date DESC', 50, NULL, NULL, NULL),
('2005', '506', 'Консультации » Ответ', 1, 0, 'IF(LENGTH(answer)<300, answer, CONCAT(SUBSTR(answer, 1, 300), ''...'')) AS short_answer', NULL, NULL, 'answer_date DESC', 50, NULL, NULL, NULL),
('2006', '507', 'Консультации » Специалисты', 1, 0, NULL, NULL, NULL, 'name ASC', 50, NULL, NULL, NULL),
('2007', '508', 'Консультации » Специализации', 1, 0, NULL, NULL, NULL, 'specialization ASC', 50, NULL, NULL, NULL),
('2008', '509', 'Каталог продукции', 1, 1, NULL, NULL, NULL, NULL, 50, 'glyphicon glyphicon-gift', 'id_parent', NULL),
('2010', '511', 'Продукция', 1, 0, NULL, NULL, NULL, 'id_product ASC', 50, NULL, NULL, NULL),
('2011', '512', 'Вопрос-ответ', 1, 1, NULL, NULL, NULL, 'ask_date DESC', 50, 'glyphicon glyphicon-retweet', NULL, NULL),
('2016', '517', 'Обратная связь', 1, 1, NULL, NULL, NULL, 'id_feedback DESC', 50, 'glyphicon glyphicon-share-alt', NULL, NULL),
('2017', '519', 'Заказы пользователей', 1, 1, NULL, NULL, NULL, 'create_date DESC', 50, 'glyphicon glyphicon-shopping-cart', NULL, NULL),
('2018', '520', 'Витрина', 1, 1, NULL, NULL, NULL, 'sequence ASC', 50, 'glyphicon glyphicon-fullscreen', NULL, NULL),
('2019', '521', 'Викторины', 1, 1, NULL, NULL, NULL, 'id_quiz DESC', 50, NULL, NULL, NULL),
('2020', '522', 'Вопросы викторины', 1, 0, NULL, NULL, NULL, 'sequence ASC', 50, NULL, NULL, NULL),
('2021', '523', 'Варианты ответов', 1, 0, NULL, NULL, NULL, 'sequence ASC', 50, NULL, NULL, NULL),
('2022', '524', 'Ответ пользователя', 1, 0, NULL, NULL, NULL, 'create_date DESC', 50, NULL, NULL, NULL),
('2023', '525', 'Брэнды', 1, 1, NULL, NULL, NULL, 'sequence ASC', 50, 'glyphicon glyphicon-bookmark', 'id_parent', NULL),
('2024', '529', 'Статусы остатка продукции', 1, 1, NULL, NULL, NULL, 'id_remain_status ASC', 50, NULL, NULL, NULL),
('2025', '530', 'Отзывы клиентов', 1, 1, NULL, NULL, NULL, 'create_date DESC', 50, NULL, NULL, ''),
('2026', '531', 'Уведомления', 1, 1, NULL, NULL, NULL, 'date DESC', 50, NULL, NULL, NULL),
('21', '39', 'Типы файлов', 1, 1, NULL, NULL, NULL, 'id_file_type ASC', 50, NULL, NULL, NULL),
('22', '40', 'Расширения файлов', 1, 1, NULL, NULL, NULL, 'id_file_extension ASC', 50, NULL, NULL, NULL),
('22-view-main', '22', 'Типы данных', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('23', '41', 'Пользовательские свойства', 1, 1, NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL),
('3', '21', 'Свойства объекта', 1, 0, NULL, NULL, NULL, 'sequence ASC', 50, NULL, NULL, NULL),
('31', '49', 'События', 1, 1, NULL, NULL, NULL, 'id_event DESC', 50, NULL, NULL, NULL),
('32', '50', 'Почтовые аккаунты', 1, 1, NULL, NULL, NULL, 'host ASC', 50, 'glyphicon glyphicon-inbox', NULL, NULL),
('33', '51', 'Планировщик', 1, 1, NULL, NULL, NULL, 'id_job ASC', 50, 'glyphicon glyphicon-calendar', NULL, NULL),
('35', '54', 'Доступные локализации', 1, 1, NULL, NULL, NULL, 'id_localization ASC', 50, NULL, NULL, NULL),
('41', '61', 'Инструкции', 1, 1, NULL, NULL, NULL, 'num_seq ASC', 50, NULL, NULL, NULL),
('43', '63', 'Представление', 1, 0, NULL, NULL, NULL, 'order_no ASC', 50, NULL, NULL, '<p>Для получения возможности редактирования экземпляров объекта (осуществления CRUD-операций) необходимо создать представление. Один объект может иметь множество представлений. Представление хранит информацию о свойствах объекта, отображаемых в списке экземпляров, а также о порядке их сортировки.</p>  <p>Быстро создать представление для объекта и его колонки можно при редактировании объекта в блоке Создать представление. Подробнее читай в <a target="_blank" href="http://www.ygin.ru/documentation/sozdanie-predstavlenii-obekta-17/">документации</a> к системе.</p>'),
('44', '66', 'Колонка представления', 1, 0, NULL, NULL, NULL, 'order_no ASC', 50, NULL, NULL, NULL),
('50', '80', 'php-скрипты', 1, 0, NULL, NULL, NULL, 'id_php_script_type ASC', 50, NULL, NULL, NULL),
('52', '86', 'Интерфейс php-скрипта', 1, 1, NULL, NULL, NULL, 'id_php_script_interface ASC', 50, NULL, NULL, NULL),
('55', '101', 'Виджеты &raquo; Наборы виджетов', 1, 1, NULL, NULL, NULL, 'id_module_template ASC', 50, 'glyphicon glyphicon-tags', NULL, NULL),
('57', '103', 'Виджеты &raquo; Виджеты сайта', 1, 1, NULL, NULL, NULL, 'name ASC', 50, 'glyphicon glyphicon-tag', NULL, NULL),
('58', '105', 'Голосование', 1, 1, NULL, NULL, NULL, 'create_date DESC', 50, 'icon-check', NULL, NULL),
('59', '106', 'Ответы на голосование', 1, 0, NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL),
('6', '24', 'Пользователи', 1, 1, NULL, NULL, NULL, NULL, 50, 'glyphicon glyphicon-user', NULL, NULL),
('67', '250', 'Комментарии', 1, 1, 'id_object, id_instance', NULL, NULL, 'comment_date ASC', 50, 'glyphicon glyphicon-comment', 'id_parent', NULL),
('7', '25', 'Типы прав доступа', 1, 1, NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL),
('9', '27', 'Справочники', 1, 1, NULL, NULL, NULL, 'id_reference ASC', 50, 'glyphicon glyphicon-book', NULL, NULL),
('project-bloki-kontenta-view-main', 'project-bloki-kontenta', 'Блоки контента', 1, 1, NULL, NULL, NULL, NULL, 50, NULL, NULL, ''),
('project-forma-obratnoi-svyazi-view-main', 'project-forma-obratnoi-svyazi', 'Форма свзяи', 1, 1, NULL, NULL, NULL, 'id_app_form_feedback DESC', 50, NULL, NULL, ''),
('project-forma-zakaza-zvonka-view-main', 'project-forma-zakaza-zvonka', 'Форма заказа звонка', 1, 1, NULL, NULL, NULL, 'id_app_form_callback DESC', 50, NULL, NULL, ''),
('project-fotoalbomy-view-main', 'project-fotoalbomy', 'Фотоальбомы', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('project-kontakty-view-main', 'project-kontakty', 'Контакты', 1, 1, NULL, NULL, NULL, '', 50, NULL, NULL, NULL),
('project-slaider-view-main', 'project-slaider', 'Слайдер', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('project-spezialisty-view-main', 'project-spezialisty', 'Специалисты', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('project-uslugi-view-main', 'project-uslugi', 'Услуги', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('project-vidoalbom-view-main', 'project-vidoalbom', 'Видоальбом', 1, 1, NULL, NULL, NULL, 'sequence', 50, NULL, NULL, NULL),
('project-vopros-otvet-view-main', 'project-vopros-otvet', 'Вопрос-ответ', 1, 1, NULL, NULL, NULL, 'datetime DESC', 50, NULL, NULL, ''),
('ygin-invoice-view-main', 'ygin-invoice', 'Счета', 1, 1, NULL, NULL, NULL, 'create_date DESC', 50, NULL, NULL, NULL),
('ygin-menu-view-main', 'ygin-menu', 'Меню', 1, 1, 'external_link, go_to_type, alias, id_parent, content', NULL, NULL, 'sequence ASC', 50, 'glyphicon glyphicon-list-alt', 'id_parent', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_object_view_column`
--

CREATE TABLE IF NOT EXISTS `da_object_view_column` (
  `id_object_view_column` varchar(255) NOT NULL COMMENT 'id',
  `id_object_view` varchar(255) NOT NULL COMMENT 'Представление',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `caption` varchar(255) DEFAULT NULL COMMENT 'Заголовок колонки',
  `order_no` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  `id_object_parameter` varchar(255) DEFAULT NULL COMMENT 'Параметр объекта',
  `id_data_type` int(8) NOT NULL DEFAULT '2' COMMENT 'Тип данных',
  `field_name` varchar(255) NOT NULL COMMENT 'Имя поля',
  `handler` varchar(255) DEFAULT NULL COMMENT 'Обработчик',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_object_view_column`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Колонка представления';

--
-- Дамп данных таблицы `da_object_view_column`
--

INSERT INTO `da_object_view_column` (`id_object_view_column`, `id_object_view`, `id_object`, `caption`, `order_no`, `id_object_parameter`, `id_data_type`, `field_name`, `handler`, `visible`) VALUES
('105', '50', '80', 'Доступен', 0, '334', 9, 'active', NULL, 1),
('106', '50', '80', 'Интерфейс', 1, '337', 7, 'id_php_script_interface', NULL, 1),
('107', '50', '80', 'Путь к скрипту', 3, '339', 2, 'file_path', NULL, 1),
('11', '6', '24', 'Имя пользователя', 1, '93', 2, 'full_name', NULL, 1),
('112', '52', '86', 'Название', 1, '348', 2, 'name', NULL, 1),
('115', 'ygin-menu-view-main', 'ygin-menu', 'Название в меню', 1, '2', 2, 'name', NULL, 1),
('116', 'ygin-menu-view-main', 'ygin-menu', NULL, 2, '5', 10, 'visible', '3', 1),
('117', '55', '101', 'Название шаблона', 1, '13', 2, 'name', NULL, 1),
('118', '55', '101', 'Шаблон по умолчанию', 2, '16', 9, 'is_default_template', NULL, 1),
('119', '50', '80', 'Имя', 2, '342', 2, 'description', NULL, 1),
('12', '6', '24', 'Группа пользователя', 2, '89', 7, 'id_group', NULL, 0),
('120', '57', '103', 'Видимость', 3, '546', 9, 'is_visible', NULL, 1),
('121', '57', '103', NULL, 2, '544', 10, 'link', '4', 1),
('122', '57', '103', 'Имя', 1, '542', 2, 'name', NULL, 1),
('123', '58', '105', 'Название голосования', 1, '531', 2, 'name', NULL, 1),
('124', '58', '105', 'Активное', 2, '534', 9, 'is_active', NULL, 1),
('125', '58', '105', 'Показать в модуле', 3, '535', 9, 'in_module', NULL, 1),
('126', '58', '105', 'Дата создания голосования', 4, '536', 4, 'create_date', NULL, 1),
('127', '59', '106', 'Вариант ответа', 1, '539', 2, 'name', NULL, 1),
('13', '6', '24', 'Логин', 3, '90', 2, 'name', NULL, 1),
('14', '6', '24', 'Дата регистрации пользователя', 4, '239', 4, 'create_date', NULL, 1),
('145', '67', '250', 'Автор', 1, '1204', 2, 'comment_name', NULL, 1),
('146', '67', '250', 'Дата', 2, '1206', 4, 'comment_date', NULL, 1),
('147', '67', '250', 'Статус', 5, '1208', 6, 'moderation', '', 1),
('148', '67', '250', 'Ссылка', 3, NULL, 10, '-', '443', 0),
('149', '50', '80', '', 5, NULL, 10, 'cache', '5', 0),
('15', '7', '25', 'name', 1, '97', 2, 'name', NULL, 1),
('150', '57', '103', NULL, 3, NULL, 10, 'cache', '5', 0),
('170', '100', '261', 'Заголовок группы', 1, '1312', 2, 'title', NULL, 1),
('171', '100', '261', 'Тип показа баннеров', 2, '1313', 6, 'showing', NULL, 1),
('172', '101', '260', 'Текстовое описание', 1, '1304', 2, 'alt', NULL, 1),
('173', '101', '260', 'Ссылка на сайт', 2, '1303', 2, 'link', NULL, 1),
('2', '2', '20', 'Имя объекта', 1, '64', 2, 'name', NULL, 1),
('20', '9', '27', 'Название справочника', 1, '110', 2, 'name', NULL, 1),
('2001-visible', '2001', '501', 'Видимость', 3, '501-visible', 9, 'visible', NULL, 1),
('21', '10', '28', 'Справочник', 1, '112', 7, 'id_reference', NULL, 1),
('22', '10', '28', 'Значение элемента', 2, '113', 1, 'id_reference_element', NULL, 1),
('22-view-main-name', '22-view-main', '22', 'Название типа данных', 1, '86', 2, 'name', NULL, 1),
('23', '10', '28', 'Имя значения', 3, '114', 2, 'value', NULL, 1),
('24', '11', '29', 'Название', 1, '49', 2, 'name', NULL, 1),
('25', '12', '30', 'Группа параметров', 1, '117', 7, 'id_group_system_parameter', NULL, 1),
('26', '12', '30', 'Значение параметра', 2, '119', 2, 'value', NULL, 1),
('27', '12', '30', 'Описание', 3, '120', 2, 'note', NULL, 1),
('28', '13', '31', 'Путь к содержимому домена', 1, '282', 2, 'domain_path', NULL, 1),
('29', '13', '31', 'Доменное имя', 2, '283', 2, 'name', NULL, 1),
('3', '3', '21', 'Объект', 1, '75', 7, 'id_object', NULL, 1),
('30', '13', '31', 'Описание', 3, '284', 2, 'description', NULL, 1),
('31', '15', '33', 'Описание', 1, '31', 2, 'description', NULL, 1),
('32', '16', '34', 'Тип события', 1, '37', 7, 'id_event_type', NULL, 1),
('33', '16', '34', 'E-mail адрес', 2, '42', 2, 'email', NULL, 1),
('34', '16', '34', 'Имя подписчика', 3, '44', 2, 'name', NULL, 1),
('35', '17', '35', 'Название', 1, '51', 2, 'name', NULL, 1),
('36', '17', '35', 'Объект для работы', 2, '245', 7, 'id_object', NULL, 1),
('37', '6', '24', 'Активен', 5, '122', 9, 'active', NULL, 1),
('39', '19', '37', 'Путь к файлу', 1, '135', 2, 'file_path', NULL, 1),
('4', '3', '21', 'Тип свойства', 2, '76', 7, 'id_parameter_type', NULL, 1),
('40', '19', '37', 'Объект', 2, '138', 7, 'id_object', NULL, 1),
('41', '21', '39', 'Название', 1, '130', 2, 'name', NULL, 1),
('42', '22', '40', 'Тип файла', 1, '133', 7, 'id_file_type', NULL, 1),
('43', '22', '40', 'Расширение', 2, '132', 2, 'ext', NULL, 1),
('44', '23', '41', 'Описание', 1, '298', 2, 'caption', NULL, 1),
('45', '23', '41', 'Имя свойства', 2, '299', 2, 'NAME', NULL, 1),
('46', '23', '41', 'Тип свойства', 3, '294', 7, 'ID_PROPERTY_TYPE', NULL, 1),
('47', '23', '41', 'Объект', 4, '295', 7, 'ID_OBJECT', NULL, 1),
('5', '3', '21', 'Виджет', 3, '78', 2, 'widget', NULL, 1),
('6', '3', '21', 'Описание свойства', 4, '79', 2, 'caption', NULL, 1),
('60', '31', '49', 'Тип события', 1, '254', 7, 'id_event_type', NULL, 1),
('6001', '2001', '501', 'Файл', 1, '1510', 8, 'file', NULL, 1),
('6002', '2001', '501', 'Название', 2, '1508', 2, 'name', NULL, 1),
('6004', '2002', '502', 'Категория', 3, '1514', 7, 'id_news_category', NULL, 0),
('6005', '2002', '502', 'Заголовок', 2, '1517', 2, 'title', NULL, 1),
('6006', '2002', '502', 'Дата', 4, '1516', 4, 'date', NULL, 1),
('6008', '2002', '502', 'Фото', 1, '1519', 8, 'photo', NULL, 1),
('6009', '2002', '502', 'Видимость', 14, '1520', 9, 'is_visible', NULL, 1),
('6010', '2003', '503', 'Название', 1, '1522', 2, 'name', NULL, 1),
('6011', '2003', '503', 'Видимость', 13, '1524', 9, 'is_visible', NULL, 1),
('6012', '2004', '505', 'Спрашивает', 1, '1526', 2, 'user_fio', NULL, 1),
('6014', '2004', '505', 'Дата', 1, '1530', 4, 'ask_date', NULL, 1),
('6015', '2004', '505', 'Вопрос', 1, '1528', 14, 'short_ask', NULL, 1),
('6016', '2004', '505', 'Видимость', 12, '1532', 9, 'is_visible', NULL, 1),
('6017', '2005', '506', 'Отвечающий', 2, '1534', 7, 'id_consultation_answerer', NULL, 1),
('6018', '2005', '506', 'Отвечающий (ручной ввод)', 3, '1535', 2, 'answerer', NULL, 1),
('6019', '2005', '506', 'Дата', 1, '1536', 4, 'answer_date', NULL, 1),
('6020', '2005', '506', 'Ответ', 4, '1538', 3, 'short_answer', NULL, 1),
('6021', '2005', '506', 'IP', 5, '1539', 2, 'ip', NULL, 1),
('6022', '2006', '507', 'ФИО отвечающего', 2, '1547', 2, 'name', NULL, 1),
('6023', '2006', '507', 'e-mail', 3, '1546', 2, 'email', NULL, 1),
('6024', '2006', '507', 'Фото', 1, '1544', 8, 'photo', NULL, 1),
('6025', '2007', '508', 'Описание', 2, '1549', 14, 'description', NULL, 1),
('6026', '2007', '508', 'Специализация', 1, '1551', 2, 'specialization', NULL, 1),
('6027', '2002', '502', 'Комменты', 10, NULL, 10, '-', '250', 0),
('6028', '2004', '505', 'Комменты', 10, NULL, 10, '-', '250', 1),
('6029', '2008', '509', 'Название', 2, '1553', 2, 'name', NULL, 1),
('6030', '2008', '509', 'Наценка (%)', 3, '1557', 1, 'price_markup', NULL, 1),
('6033', '2010', '511', 'Название', 3, '1567', 2, 'name', NULL, 1),
('6036', '2010', '511', 'Розничная цена', 4, '1570', 1, 'retail_price', NULL, 1),
('6037', '2010', '511', 'Изображение', 1, '1576', 8, 'image', NULL, 1),
('6038', '2010', '511', 'Код', 2, '1566', 1, 'code', NULL, 1),
('6039', '2011', '512', 'Дата', 1, '1579', 4, 'ask_date', NULL, 1),
('6040', '2011', '512', 'Спрашивающий', 1, '1580', 2, 'name', NULL, 1),
('6041', '2011', '512', 'Вопрос', 1, '1581', 14, 'question', NULL, 1),
('6043', '2011', '512', 'Видимость', 1, '1585', 9, 'visible', NULL, 1),
('6056', '2016', '517', 'ФИО', 1, '1610', 2, 'fio', NULL, 1),
('6057', '2016', '517', 'Сообщение', 1, '1613', 14, 'message', NULL, 1),
('6058', '2016', '517', 'Дата сообщения', 1, '1614', 4, 'date', NULL, 1),
('6059', '2010', '511', NULL, 5, NULL, 10, '-', '1001', 1),
('6060', '2017', '519', 'ФИО', 2, '1621', 2, 'fio', NULL, 1),
('6061', '2017', '519', 'e-mail', 3, '1623', 2, 'mail', NULL, 1),
('6062', '2017', '519', 'Заказ', 4, '1625', 14, 'offer_text', NULL, 1),
('6063', '2017', '519', 'Обработано', 5, '1627', 9, 'is_process', NULL, 1),
('6064', '2018', '520', 'Фото', 3, '1634', 8, 'image', NULL, 1),
('6065', '2018', '520', 'Ссылка на переход', 2, '1631', 2, 'link', NULL, 1),
('6066', '2018', '520', 'Заголовок', 1, '1632', 2, 'title', NULL, 1),
('6068', '101', '260', 'Видимость', 3, '1309', 9, 'visible', NULL, 1),
('6069', '2019', '521', 'name', 1, '1639', 2, 'name', NULL, 1),
('6070', '2019', '521', 'active', 1, '1642', 9, 'active', NULL, 1),
('6071', '2020', '522', 'Викторина', 1, '1644', 7, 'id_quiz', NULL, 1),
('6072', '2020', '522', 'Текст вопроса', 1, '1646', 3, 'question', NULL, 1),
('6073', '2020', '522', 'Тип ответов', 1, '1647', 6, 'type', NULL, 1),
('6074', '2021', '523', 'Вопрос', 1, '1650', 7, 'id_quiz_question', NULL, 1),
('6075', '2021', '523', 'Текст ответа', 1, '1651', 2, 'answer', NULL, 1),
('6076', '2021', '523', 'Правильный ли ответ', 1, '1652', 9, 'is_right', NULL, 1),
('6077', '2022', '524', 'Викторина', 1, '1655', 7, 'id_quiz', NULL, 1),
('6078', '2022', '524', 'ФИО', 1, '1656', 2, 'name', NULL, 1),
('6079', '2022', '524', 'Дата', 1, '1661', 4, 'create_date', NULL, 1),
('6080', '2008', '509', 'Фото', 1, '1555', 8, 'image', NULL, 1),
('6081', '2017', '519', 'Дата', 1, '1626', 4, 'create_date', NULL, 1),
('6082', '2008', '509', 'Видимость', 4, '1637', 9, 'visible', NULL, 1),
('6083', '2010', '511', 'Видимость', 6, '1663', 9, 'visible', NULL, 1),
('6084', '2023', '525', 'Название', 1, '1667', 2, 'name', NULL, 1),
('6085', '2023', '525', 'Логотип брэнда', 1, '1669', 8, 'image', NULL, 1),
('6086', '2024', '529', 'Название', 1, '1680', 2, 'name', NULL, 1),
('6087', '2025', '530', 'ФИО', 1, '1685', 2, 'name', NULL, 1),
('6088', '2025', '530', 'Дата', 4, '1686', 4, 'create_date', NULL, 1),
('6089', '2025', '530', 'Текст отзыва', 2, '1687', 14, 'review', NULL, 1),
('6090', '2025', '530', 'Видимость на сайте', 3, '1689', 9, 'visible', NULL, 1),
('6091', '2026', '531', 'Текст', 1, '1694', 14, 'text', NULL, 1),
('6092', '2026', '531', 'Дата создания', 1, '1695', 4, 'date', NULL, 1),
('6093', '67', '250', 'Комментарий', 4, '1207', 10, 'comment_text', '1046', 1),
('6094', 'ygin-menu-view-main', 'ygin-menu', 'Видимость', 23, '5', 9, 'visible', NULL, 1),
('61', '31', '49', 'Дата создания события', 2, '256', 4, 'event_create', NULL, 1),
('62', '32', '50', 'E-mail для поля "От"', 1, '259', 2, 'email_from', NULL, 1),
('63', '32', '50', 'Имя отправителя', 2, '260', 2, 'from_name', NULL, 1),
('64', '32', '50', 'HOST', 3, '262', 2, 'host', NULL, 1),
('65', '33', '51', 'Имя задачи', 1, '274', 2, 'name', NULL, 1),
('66', '33', '51', 'Дата последнего запуска', 2, '271', 4, 'last_start_date', NULL, 1),
('67', '33', '51', 'Дата следущего запуска', 3, '272', 4, 'next_start_date', NULL, 1),
('68', '33', '51', 'Количество ошибок', 4, '273', 1, 'failures', NULL, 1),
('69', '33', '51', 'Имя класса задачи', 5, '276', 2, 'class_name', NULL, 1),
('7', '3', '21', 'Имя поля в БД', 5, '80', 2, 'field_name', NULL, 1),
('70', '33', '51', 'Вкл.', 6, '291', 9, 'active', NULL, 1),
('71', '35', '54', 'Код', 1, '312', 2, 'code', NULL, 1),
('72', '35', '54', 'Название', 2, '311', 2, 'name', NULL, 1),
('73', '35', '54', 'Используется ли локализация', 3, '313', 9, 'is_use', NULL, 1),
('79', '13', '31', 'Активен', 4, '304', 9, 'active', NULL, 1),
('8', '3', '21', 'NOT NULL', 6, '84', 9, 'not_null', NULL, 1),
('85', '41', '61', 'Название', 1, '141', 2, 'name', NULL, 1),
('86', '41', '61', 'Видимость', 2, '144', 9, 'visible', NULL, 1),
('87', '41', '61', 'Относится только к этому сайту', 3, '143', 9, 'desc_type', NULL, 1),
('88', '43', '63', 'Объект', 1, '401', 7, 'id_object', NULL, 1),
('89', '43', '63', 'Имя представления', 2, '406', 2, 'name', NULL, 1),
('9', '2', '20', 'Таблица', 0, '67', 2, 'table_name', NULL, 1),
('90', '44', '66', 'Представление', 1, '416', 7, 'id_object_view', NULL, 1),
('91', '44', '66', 'Заголовок колонки', 2, '418', 2, 'caption', NULL, 1),
('92', '44', '66', 'Параметр объекта', 3, '420', 7, 'id_object_parameter', NULL, 1),
('93', '44', '66', 'Тип данных', 4, '421', 7, 'id_data_type', NULL, 1),
('94', '44', '66', 'Имя поля', 5, '422', 2, 'field_name', NULL, 1),
('95', '44', '66', 'Видимость', 6, '424', 9, 'visible', NULL, 1),
('project-bloki-kontenta-view-main-explanation', 'project-bloki-kontenta-view-main', 'project-bloki-kontenta', 'Объяснение', 1, 'project-bloki-kontenta-explanation', 2, 'explanation', NULL, 1),
('project-bloki-kontenta-view-main-name', 'project-bloki-kontenta-view-main', 'project-bloki-kontenta', 'Название', 1, 'project-bloki-kontenta-name', 2, 'name', NULL, 1),
('project-bloki-kontenta-view-main-state', 'project-bloki-kontenta-view-main', 'project-bloki-kontenta', 'Видимость', 1, 'project-bloki-kontenta-state', 9, 'state', NULL, 1),
('project-forma-obratnoi-svyazi-view-main-datetime', 'project-forma-obratnoi-svyazi-view-main', 'project-forma-obratnoi-svyazi', 'Дата отправки', 4, 'project-forma-obratnoi-svyazi-datetime', 4, 'datetime', NULL, 1),
('project-forma-obratnoi-svyazi-view-main-email', 'project-forma-obratnoi-svyazi-view-main', 'project-forma-obratnoi-svyazi', 'Ваш e-mail', 2, 'project-forma-obratnoi-svyazi-email', 2, 'email', NULL, 1),
('project-forma-obratnoi-svyazi-view-main-name', 'project-forma-obratnoi-svyazi-view-main', 'project-forma-obratnoi-svyazi', 'Ваше имя', 1, 'project-forma-obratnoi-svyazi-name', 2, 'name', NULL, 1),
('project-forma-obratnoi-svyazi-view-main-phone', 'project-forma-obratnoi-svyazi-view-main', 'project-forma-obratnoi-svyazi', 'Ваш телефон', 3, 'project-forma-obratnoi-svyazi-phone', 2, 'phone', NULL, 1),
('project-forma-zakaza-zvonka-view-main-datetime', 'project-forma-zakaza-zvonka-view-main', 'project-forma-zakaza-zvonka', 'Дата отправки', 3, 'project-forma-zakaza-zvonka-datetime', 4, 'datetime', NULL, 1),
('project-forma-zakaza-zvonka-view-main-name', 'project-forma-zakaza-zvonka-view-main', 'project-forma-zakaza-zvonka', 'Ваше имя', 1, 'project-forma-zakaza-zvonka-name', 2, 'name', NULL, 1),
('project-forma-zakaza-zvonka-view-main-phone', 'project-forma-zakaza-zvonka-view-main', 'project-forma-zakaza-zvonka', 'Ваш телефон', 2, 'project-forma-zakaza-zvonka-phone', 2, 'phone', NULL, 1),
('project-fotoalbomy-view-main-foto', 'project-fotoalbomy-view-main', '4', 'Фото', 2, '', 10, '-', '1001', 1),
('project-fotoalbomy-view-main-name-ru', 'project-fotoalbomy-view-main', 'project-fotoalbomy', 'Название альбома', 1, 'project-fotoalbomy-name-ru', 2, 'name', '', 1),
('project-fotoalbomy-view-main-visible', 'project-fotoalbomy-view-main', 'project-fotoalbomy', 'Видимость', 3, 'project-fotoalbomy-visible', 9, 'visible', NULL, 1),
('project-kontakty-view-main-content-ru', 'project-kontakty-view-main', 'project-kontakty', 'Содержание', 1, 'project-kontakty-content-ru', 3, 'content', '', 1),
('project-slaider-view-main-image', 'project-slaider-view-main', 'project-slaider', 'Картинка', 1, 'project-slaider-image', 8, 'image', NULL, 1),
('project-slaider-view-main-title', 'project-slaider-view-main', 'project-slaider', 'Заголовок', 1, 'project-slaider-title', 2, 'title', NULL, 1),
('project-slaider-view-main-visible', 'project-slaider-view-main', 'project-slaider', 'Видимость', 1, 'project-slaider-visible', 9, 'visible', NULL, 1),
('project-spezialisty-view-main-avatar', 'project-spezialisty-view-main', 'project-spezialisty', 'Аватар', 1, 'project-spezialisty-avatar', 8, 'avatar', NULL, 1),
('project-spezialisty-view-main-name', 'project-spezialisty-view-main', 'project-spezialisty', 'Имя (ФИО)', 1, 'project-spezialisty-name', 2, 'name', NULL, 1),
('project-spezialisty-view-main-on-main', 'project-spezialisty-view-main', 'project-spezialisty', 'Отображать на главной странице', 1, 'project-spezialisty-on-main', 9, 'on_main', NULL, 1),
('project-spezialisty-view-main-visible', 'project-spezialisty-view-main', 'project-spezialisty', 'Видимость', 1, 'project-spezialisty-visible', 9, 'visible', NULL, 1),
('project-uslugi-view-main-image', 'project-uslugi-view-main', 'project-uslugi', 'Картинка на фон блока', 1, 'project-uslugi-image', 8, 'image', NULL, 1),
('project-uslugi-view-main-name', 'project-uslugi-view-main', 'project-uslugi', 'Название', 1, 'project-uslugi-name', 2, 'name', NULL, 1),
('project-uslugi-view-main-on-main', 'project-uslugi-view-main', 'project-uslugi', 'Отображать на главной странице', 1, 'project-uslugi-on-main', 9, 'on_main', NULL, 1),
('project-uslugi-view-main-visible', 'project-uslugi-view-main', 'project-uslugi', 'Видимость', 1, 'project-uslugi-visible', 9, 'visible', NULL, 1),
('project-vidoalbom-view-main-name-ru', 'project-vidoalbom-view-main', 'project-vidoalbom', 'Название', 1, 'project-vidoalbom-name-ru', 2, 'name', '', 1),
('project-vidoalbom-view-main-visible', 'project-vidoalbom-view-main', 'project-vidoalbom', 'Видимость', 1, 'project-vidoalbom-visible', 9, 'visible', NULL, 1),
('project-vopros-otvet-view-main-datetime', 'project-vopros-otvet-view-main', 'project-vopros-otvet', 'Дата отправки', 5, 'project-vopros-otvet-datetime', 4, 'datetime', NULL, 1),
('project-vopros-otvet-view-main-id-app-specialist', 'project-vopros-otvet-view-main', 'project-vopros-otvet', 'Специалист', 1, 'project-vopros-otvet-id-app-specialist', 7, 'id_app_specialist', NULL, 1),
('project-vopros-otvet-view-main-immya', 'project-vopros-otvet-view-main', 'project-vopros-otvet', 'Отправитель', 1, 'project-vopros-otvet-immya', 2, 'immya', NULL, 1),
('project-vopros-otvet-view-main-theme', 'project-vopros-otvet-view-main', 'project-vopros-otvet', 'Тема вопроса', 2, 'project-vopros-otvet-theme', 2, 'theme', NULL, 1),
('project-vopros-otvet-view-main-visible', 'project-vopros-otvet-view-main', 'project-vopros-otvet', 'Видимость', 3, 'project-vopros-otvet-visible', 9, 'visible', NULL, 1),
('ygin-faq-view-categoryQuestion', '2011', '512', 'Категория', 0, 'ygin-faq-category', 6, 'category', '', 1),
('ygin-invoice-view-main-amount', 'ygin-invoice-view-main', 'ygin-invoice', 'Сумма', 1, 'ygin-invoice-amount', 1, 'amount', NULL, 1),
('ygin-invoice-view-main-create-date', 'ygin-invoice-view-main', 'ygin-invoice', 'Дата создания', 1, 'ygin-invoice-create-date', 4, 'create_date', NULL, 1),
('ygin-invoice-view-main-id-offer', 'ygin-invoice-view-main', 'ygin-invoice', 'Заказ', 1, 'ygin-invoice-id-offer', 7, 'id_offer', NULL, 1),
('ygin-invoice-view-main-pay-date', 'ygin-invoice-view-main', 'ygin-invoice', 'Дата оплаты', 1, 'ygin-invoice-pay-date', 4, 'pay_date', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_php_script`
--

CREATE TABLE IF NOT EXISTS `da_php_script` (
  `id_php_script` int(8) NOT NULL AUTO_INCREMENT,
  `id_php_script_type` varchar(255) NOT NULL,
  `id_module` int(8) DEFAULT NULL,
  `params_value` longtext,
  PRIMARY KEY (`id_php_script`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1032 ;

--
-- Дамп данных таблицы `da_php_script`
--

INSERT INTO `da_php_script` (`id_php_script`, `id_php_script_type`, `id_module`, `params_value`) VALUES
(1016, '1032', 1007, NULL),
(1023, '1042', 1014, 'a:7:{s:11:"htmlOptions";s:32:"array(''class'' => ''nav nav-list'')";s:14:"activeCssClass";s:6:"active";s:12:"itemCssClass";s:4:"item";s:11:"encodeLabel";s:5:"false";s:18:"submenuHtmlOptions";s:46:"array(''class'' => ''nav nav-list sub-item-list'')";s:13:"maxChildLevel";s:1:"2";s:12:"baseTemplate";s:42:"<div class="b-menu-side-list">{menu}</div>";}'),
(1027, '1045', 1000, 'a:0:{}'),
(1029, '1005', NULL, 'a:1:{s:7:"maxNews";i:3;}'),
(1030, '1045', NULL, 'a:0:{}'),
(1031, '1045', NULL, 'a:0:{}');

-- --------------------------------------------------------

--
-- Структура таблицы `da_php_script_interface`
--

CREATE TABLE IF NOT EXISTS `da_php_script_interface` (
  `id_php_script_interface` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT 'Порядок',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `template` longtext COMMENT 'Шаблон файла',
  PRIMARY KEY (`id_php_script_interface`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Интерфейс php-скрипта' AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `da_php_script_interface`
--

INSERT INTO `da_php_script_interface` (`id_php_script_interface`, `sequence`, `name`, `template`) VALUES
(1, 1, 'Контент раздела сайта', 'class <<class_name>> extends ViewModule {\r\n  public function run() {\r\n\r\n  }\r\n}'),
(2, 2, 'Контент модуля сайта', 'class <<class_name>> extends ViewModule {\r\n  public function run() {\r\n\r\n  }\r\n}'),
(6, 6, 'Колонка представления', '<?php\r\n\r\nclass <<class_name>> implements ColumnType {\r\n\r\n  public function init($idObject, Array $arrayOfIdInstances, $idObjectParameter) {\r\n\r\n  }\r\n  \r\n  public function process($idInstance, $value, $idObjectParameter) {\r\n    return $value;\r\n  }\r\n  \r\n  public function getStyleClass() {\r\n    return null;\r\n  }\r\n}\r\n\r\n?>'),
(9, 9, 'Визуальный элемент', 'class <> extends VisualElement implements DaVisualElement {\r\n\r\n public function callBeforeProcessInstance() {\r\n\r\n }\r\n\r\n public function callAfterProcessInstance() {\r\n\r\n }\r\n\r\n protected function validate() {\r\n return true;\r\n }\r\n\r\n public function getValueFromClient() {\r\n return null;\r\n }\r\n}');

-- --------------------------------------------------------

--
-- Структура таблицы `da_php_script_type`
--

CREATE TABLE IF NOT EXISTS `da_php_script_type` (
  `id_php_script_type` varchar(255) NOT NULL COMMENT 'id',
  `file_path` varchar(255) NOT NULL COMMENT 'Путь к скрипту',
  `description` varchar(255) DEFAULT NULL COMMENT 'Название скрипта',
  `id_php_script_interface` int(8) NOT NULL COMMENT 'Интерфейс',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активен',
  PRIMARY KEY (`id_php_script_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='php-скрипты';

--
-- Дамп данных таблицы `da_php_script_type`
--

INSERT INTO `da_php_script_type` (`id_php_script_type`, `file_path`, `description`, `id_php_script_interface`, `active`) VALUES
('1000', 'project/plugin/photogallery/PhotogalleryView.php', 'Фотогалерея » Раздел сайта', 1, 1),
('1001', 'photogallery.backend.column.PhotoColumn', 'Фотогалерея » Колонка представления', 6, 1),
('1002', 'project/plugin/search/SearchView.php', 'Поиск по всему сайту', 1, 1),
('1003', 'project/plugin/news/NewsView.php', 'Новости » Список', 1, 1),
('1004', 'project/plugin/news/NewsCategoryView.php', 'Новости » Список в категории', 1, 1),
('1005', 'news.widgets.news.NewsWidget', 'Новости » Список последних на главной', 2, 1),
('1006', 'project/plugin/consultation/ConsultationView.php', 'Консультации', 1, 1),
('1007', 'project/plugin/internet_magazin/OfferFormView.php', 'Интернет-магазин » Форма оформления заказа', 1, 1),
('1008', 'project/plugin/internet_magazin/CatalogView.php', 'Интернет-магазин » Список товаров', 1, 1),
('1009', 'project/plugin/internet_magazin/CatalogModuleView.php', 'Интернет-магазин » Модуль категорий каталога', 2, 0),
('1010', 'shop.widgets.cart.CartWidget', 'Интернет-магазин » Корзина', 2, 0),
('1012', 'project/plugin/faq/FaqView.php', 'Вопрос-ответ', 1, 1),
('1027', 'project/plugin/internet_magazin/CatalogMainView.php', 'Интернет-магазин » Главная страница магазина', 1, 1),
('1029', 'project/plugin/comment/CommentModerationView.php', 'Модерирование комментариев', 1, 1),
('1032', 'user.widgets.login.LoginWidget', 'Авторизация', 2, 1),
('1037', 'feedback.widgets.FeedbackWidget', 'Кнопка обратной связи', 2, 1),
('1038', 'shop.widgets.category.CategoryWidget', 'Интернет-магазин » Категории', 2, 0),
('1039', 'ygin.widgets.vitrine.VitrineWidget', 'Витрина', 2, 0),
('1040', 'banners.widgets.specialOffer.SpecialOfferWidget', 'Спецпредложения', 2, 0),
('1041', 'vote.widgets.VoteWidget', 'Голосование', 2, 0),
('1042', 'menu.widgets.MenuWidget', 'Меню', 2, 1),
('1043', 'shop.widgets.brand.BrandWidget', 'Интернет-магазин » Брэнды', 2, 0),
('1044', 'ygin.widgets.cloudim.CloudimWidget', 'Онлайн-консультации Cloudim', 2, 0),
('1045', 'photogallery.widgets.randomPhoto.RandomPhotoWidget', 'Случайное фото', 2, 1),
('1046', 'backend.components.column.abstract.StrippedColumn', 'Текст без тегов и заменой nl на br', 6, 1),
('250', 'comments.backend.CommentsColumn', 'Комментарии » Комментарии экземпляра', 6, 1),
('3', 'menu.backend.column.InfoStatus', 'Статус раздела в объекте Меню', 6, 1),
('4', 'menu.backend.column.SiteModuleInfoStatus', 'Статус модуля сайта', 6, 1),
('443', 'comments.backend.CommentsViewLinkColumn', 'Комментарии » Ссылка на объект', 6, 1),
('5', 'engine/admin/column/abstract/PhpScriptTypeCacheClear.php', 'Кэш в базе', 6, 1),
('project-php-bloki---ssylki-na-glavnoi-vidghet', 'application.widgets.PromoBlockWidget', 'Блоки - ссылки на главной виджет', 2, 1),
('project-php-bystrye-novosti', 'application.widgets.FlashNewsWidget', 'Быстрые новости', 2, 1),
('project-php-forma-napisat-nam', 'application.widgets.formContactUsWidget', 'Форма написать нам', 2, 1),
('project-php-forma-podbora-tura', 'application.widgets.FormTourSelectionWidget', 'Форма подбора тура', 2, 1),
('project-php-forma-zadat-vopros', 'application.widgets.formAskQuestionWidget', 'Форма задать вопрос', 2, 1),
('project-php-forma-zakaza-suvenira', 'FormSouvenirOrderWidget', 'Форма заказа сувенира', 2, 1),
('project-php-forma-zakaza-tura', 'application.widgets.formTourOrderWidget', 'Форма заказа тура', 2, 1),
('project-php-forma-zakaza-zvonka', 'application.widgets.formCallbackWidget', 'Форма заказа звонка', 2, 1),
('project-php-novinki-turov', 'application.widgets.NewToursWidget', 'Новинки туров', 2, 1),
('project-php-otzyvy', 'application.widgets.ReviewWidget', 'Отзывы', 2, 1),
('project-php-pereklyuchatel-yazyka', 'application.widgets.ToggleLanguageWidget', 'Переключатель языка', 2, 1),
('project-php-randomnye-foto', 'application.widgets.FlashRandomPhotoWidget', 'Рандомные фото', 2, 1),
('project-php-tury-dlya-eto-interesno', 'application.widgets.TourInterestingAssignmentWidget', 'Туры для Это интересно', 2, 1),
('ygin-banner-widget-base', 'ygin.modules.banners.widgets.BannerWidget', 'Баннер', 2, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `da_plugin`
--

CREATE TABLE IF NOT EXISTS `da_plugin` (
  `id_plugin` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `status` int(8) NOT NULL DEFAULT '1' COMMENT 'Статус',
  `config` longtext COMMENT 'Сериализованные настройки',
  `class_name` varchar(255) NOT NULL COMMENT 'Класс плагина',
  `data` longtext COMMENT 'data',
  PRIMARY KEY (`id_plugin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Плагины системы' AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `da_plugin`
--

INSERT INTO `da_plugin` (`id_plugin`, `name`, `code`, `status`, `config`, `class_name`, `data`) VALUES
(5, 'Новости', 'ygin.news', 2, 'a:1:{s:7:"modules";a:1:{s:9:"ygin.news";a:1:{s:14:"showCategories";b:0;}}}', 'ygin.modules.news.NewsPlugin', 'a:7:{s:25:"id_php_script_type_module";i:1005;s:37:"id_php_script_type_module_param_count";i:1002;s:17:"id_sysytem_module";i:1002;s:9:"id_object";i:502;s:18:"id_object_category";i:503;s:7:"id_menu";s:3:"122";s:14:"id_site_module";s:3:"121";}'),
(6, 'Поиск по сайту', 'ygin.search', 3, 'a:1:{s:7:"modules";a:1:{s:11:"ygin.search";a:0:{}}}', 'ygin.modules.search.SearchPlugin', NULL),
(7, 'Обратная связь', 'ygin.feedback', 3, 'a:1:{s:7:"modules";a:1:{s:13:"ygin.feedback";a:1:{s:11:"idEventType";s:3:"108";}}}', 'ygin.modules.feedback.FeedbackPlugin', 'a:4:{s:25:"id_php_script_type_module";i:1037;s:17:"id_sysytem_module";i:1005;s:9:"id_object";i:517;s:17:"site_module_place";a:2:{i:0;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"1";s:18:"id_module_template";s:1:"2";}i:1;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"2";s:18:"id_module_template";s:1:"4";}}}'),
(8, 'Фотогалерея', 'ygin.photogallery', 2, 'a:1:{s:7:"modules";a:1:{s:17:"ygin.photogallery";a:1:{s:8:"urlRules";a:2:{s:12:"photogallery";N;s:28:"photogallery/<idGallery:\\d+>";N;}}}}', 'ygin.modules.photogallery.PhotogalleryPlugin', 'a:9:{s:17:"id_object_gallery";i:500;s:15:"id_object_photo";i:501;s:14:"handler_column";i:1001;s:10:"useGallery";b:1;s:23:"id_menu_module_template";s:1:"3";s:16:"id_system_module";i:1000;s:38:"id_php_script_type_widget_random_photo";i:1045;s:22:"id_widget_random_photo";s:3:"123";s:7:"id_menu";s:3:"123";}'),
(9, 'Интернет-магазин', 'ygin.shop', 3, 'a:1:{s:7:"modules";a:1:{s:9:"ygin.shop";a:7:{s:8:"pageSize";i:10;s:19:"displayTypeElements";s:3:"all";s:15:"viewProductList";s:19:"_product_list_table";s:9:"showPrice";b:1;s:17:"subCategoryOnMain";b:1;s:19:"imageCategoryOnMain";b:1;s:16:"makeOfferByOrder";b:0;}}}', 'ygin.modules.shop.ShopPlugin', 'a:14:{s:34:"id_php_script_type_module_category";i:1038;s:30:"id_php_script_type_module_cart";i:1010;s:50:"id_php_script_type_module_cart_param_visible_count";i:1028;s:31:"id_php_script_type_module_brand";i:1043;s:17:"id_sysytem_module";i:1004;s:17:"id_object_product";i:511;s:18:"id_object_category";i:509;s:15:"id_object_offer";i:519;s:15:"id_object_brand";i:525;s:19:"idEventTypeNewOffer";i:106;s:23:"id_menu_module_template";s:1:"5";s:26:"site_module_place_category";a:1:{i:0;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"3";s:18:"id_module_template";s:1:"5";}}s:22:"site_module_place_cart";a:1:{i:0;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"1";s:18:"id_module_template";s:1:"5";}}s:23:"site_module_place_brand";a:1:{i:0;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"2";s:18:"id_module_template";s:1:"5";}}}'),
(10, 'Вопрос-ответ', 'ygin.faq', 3, 'a:1:{s:7:"modules";a:1:{s:8:"ygin.faq";a:3:{s:8:"pageSize";i:15;s:8:"moderate";b:1;s:11:"idEventType";i:103;}}}', 'ygin.modules.faq.FaqPlugin', 'a:2:{s:9:"id_object";i:512;s:23:"id_menu_module_template";s:1:"3";}'),
(11, 'Опросы', 'ygin.vote', 3, 'a:1:{s:7:"modules";a:1:{s:9:"ygin.vote";a:4:{s:13:"expiredTimout";i:24;s:13:"checkByCookie";b:1;s:9:"checkByIp";b:1;s:9:"numVoteIp";i:1;}}}', 'ygin.modules.vote.VotePlugin', 'a:6:{s:25:"id_php_script_type_module";i:1041;s:17:"id_sysytem_module";i:1013;s:16:"id_object_voting";i:105;s:23:"id_object_voting_answer";i:106;s:23:"id_menu_module_template";N;s:17:"site_module_place";a:3:{i:0;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"2";s:18:"id_module_template";s:1:"1";}i:1;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"3";s:18:"id_module_template";s:1:"4";}i:2;a:3:{s:5:"place";s:1:"1";s:8:"sequence";s:1:"1";s:18:"id_module_template";s:1:"6";}}}'),
(12, 'Баннеры', 'ygin.banners', 3, 'a:1:{s:7:"modules";a:1:{s:12:"ygin.banners";a:0:{}}}', 'ygin.modules.banners.BannerPlugin', 'a:3:{s:17:"id_sysytem_module";i:300;s:22:"id_object_banner_place";i:261;s:16:"id_object_banner";i:260;}'),
(13, 'Спецпредложения', 'ygin.specoffers', 3, 'a:0:{}', 'ygin.modules.banners.widgets.specialOffer.SpecialOfferPlugin', 'a:4:{s:25:"id_php_script_type_module";i:1040;s:37:"id_php_script_type_module_param_place";i:1016;s:17:"id_sysytem_module";i:1012;s:17:"site_module_place";a:1:{i:0;a:3:{s:5:"place";s:1:"4";s:8:"sequence";s:1:"2";s:18:"id_module_template";s:1:"4";}}}'),
(14, 'Витрина', 'ygin.vitrine', 3, 'a:0:{}', 'ygin.widgets.vitrine.VitrinePlugin', 'a:4:{s:25:"id_php_script_type_module";i:1039;s:17:"id_sysytem_module";i:1011;s:9:"id_object";i:520;s:17:"site_module_place";a:1:{i:0;a:3:{s:5:"place";s:1:"4";s:8:"sequence";s:1:"1";s:18:"id_module_template";s:1:"4";}}}'),
(15, 'Онлайн-консультант', 'ygin.cloudim', 3, 'a:1:{s:10:"components";a:1:{s:13:"widgetFactory";a:1:{s:7:"widgets";a:1:{s:13:"CloudimWidget";a:1:{s:8:"htmlCode";s:0:"";}}}}}', 'ygin.widgets.cloudim.CloudimPlugin', 'a:3:{s:25:"id_php_script_type_module";i:1044;s:17:"id_sysytem_module";i:1015;s:17:"site_module_place";a:0:{}}'),
(16, 'Отзывы', 'ygin.review', 2, 'a:1:{s:7:"modules";a:1:{s:11:"ygin.review";a:3:{s:8:"pageSize";s:2:"15";s:8:"moderate";b:1;s:11:"idEventType";s:2:"56";}}}', 'ygin.modules.review.ReviewPlugin', 'a:3:{s:9:"id_object";i:530;s:13:"id_event_type";s:2:"56";s:7:"id_menu";s:3:"137";}'),
(17, 'Карта сайта', 'ygin.siteMap', 1, NULL, 'ygin.modules.siteMap.SiteMapPlugin', NULL),
(18, 'Личный кабинет', 'ygin.cabinet', 1, NULL, 'ygin.modules.user.CabinetPlugin', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_references`
--

CREATE TABLE IF NOT EXISTS `da_references` (
  `id_reference` varchar(255) NOT NULL COMMENT 'id',
  `name` varchar(100) NOT NULL COMMENT 'Название справочника',
  PRIMARY KEY (`id_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочники';

--
-- Дамп данных таблицы `da_references`
--

INSERT INTO `da_references` (`id_reference`, `name`) VALUES
('100', 'Тип вопроса в викторине'),
('101', 'Статус заказа пользователя'),
('2', 'Расположение данные в почтовом сообщении'),
('30', 'Типы порядка'),
('31', 'Тип объекта'),
('32', 'Положение модулей'),
('33', 'Дополнительные варианты обработки контента в разделе'),
('37', 'Место использования AJAX'),
('38', 'AJAX-движки'),
('50', 'Тип показа баннеров'),
('project-reference-vneschnii-vid-bloka-uslugi', 'Внешний вид блока услуги'),
('ygin-comment-reference-status', 'Статусы комментария'),
('ygin-faq-reference-categoryQuestion', 'Категории вопросов');

-- --------------------------------------------------------

--
-- Структура таблицы `da_reference_element`
--

CREATE TABLE IF NOT EXISTS `da_reference_element` (
  `id_reference` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Справочник',
  `id_reference_element` int(8) NOT NULL DEFAULT '1' COMMENT 'Значение элемента',
  `value` varchar(255) NOT NULL COMMENT 'Описание значения',
  `image_element` varchar(150) DEFAULT NULL COMMENT 'Картинка для значения',
  `id_reference_element_instance` varchar(255) NOT NULL COMMENT 'id',
  PRIMARY KEY (`id_reference_element_instance`),
  UNIQUE KEY `id_reference` (`id_reference`,`id_reference_element`),
  UNIQUE KEY `id_reference_2` (`id_reference`,`id_reference_element`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Значения справочника';

--
-- Дамп данных таблицы `da_reference_element`
--

INSERT INTO `da_reference_element` (`id_reference`, `id_reference_element`, `value`, `image_element`, `id_reference_element_instance`) VALUES
('50', 1, 'Баннеры меняются поочерёдно в заданном порядке', NULL, '100'),
('50', 2, 'Баннеры меняются в произвольном порядке', NULL, '101'),
('50', 3, 'Вывод всех баннеров', NULL, '102'),
('30', 1, 'ASC', NULL, '16'),
('30', 2, 'DESC', NULL, '17'),
('31', 1, 'Стандартный', NULL, '18'),
('31', 3, 'Контроллер', NULL, '19'),
('32', 1, 'Слева', NULL, '20'),
('32', 2, 'Справа', NULL, '21'),
('32', 3, 'Снизу', NULL, '22'),
('32', 4, 'Сверху', NULL, '23'),
('32', 5, 'В контенте', NULL, '24'),
('32', 7, 'Сверху в контенте', NULL, '25'),
('32', 95, 'В архиве', NULL, '26'),
('37', 1, 'Раздел', NULL, '38'),
('37', 2, 'Модуль', NULL, '39'),
('37', 3, 'Другое', NULL, '40'),
('38', 1, 'JsHttpRequest', NULL, '41'),
('38', 2, 'XAJAX', NULL, '42'),
('38', 3, 'jQuery', NULL, '43'),
('38', 4, 'ValumsFileUpload', NULL, '44'),
('2', 1, 'В тексте письма', NULL, '5'),
('33', 1, 'Отобразить список вложенных разделов при отсутствии контента', NULL, '50'),
('100', 1, 'один верный', NULL, '500'),
('100', 2, 'много верных', NULL, '501'),
('100', 3, 'произвольный', NULL, '502'),
('32', 6, 'Подвал', NULL, '503'),
('101', 1, 'Новый', NULL, '504'),
('101', 2, 'Согласован', NULL, '505'),
('101', 3, 'Оплачен', NULL, '506'),
('101', 4, 'Выполнен', NULL, '507'),
('101', 5, 'Отменен', NULL, '508'),
('33', 4, 'При отсутствии контента не выводить предупреждение, отобразить пустую страницу', NULL, '509'),
('33', 2, 'Переходить к первому вложенному разделу при отсутствии контента', NULL, '51'),
('33', 3, 'Открыть первый загруженный файл при отсутствии контента', NULL, '52'),
('2', 2, 'Во вложении', NULL, '6'),
('project-reference-vneschnii-vid-bloka-uslugi', 3, 'Белый', NULL, 'project-reference-vneschnii-vid-bloka-uslugi-belyi'),
('project-reference-vneschnii-vid-bloka-uslugi', 1, 'Синий', NULL, 'project-reference-vneschnii-vid-bloka-uslugi-sinii'),
('project-reference-vneschnii-vid-bloka-uslugi', 2, 'Зеленый', NULL, 'project-reference-vneschnii-vid-bloka-uslugi-zelenyi'),
('ygin-comment-reference-status', 1, 'Отмодерирован', NULL, 'ygin-comment-reference-status-approved'),
('ygin-comment-reference-status', 3, 'Удален', NULL, 'ygin-comment-reference-status-deleted'),
('ygin-comment-reference-status', 2, 'Ожидает модерации', NULL, 'ygin-comment-reference-status-pending'),
('ygin-faq-reference-categoryQuestion', 1, 'Общие', NULL, 'ygin-faq-reference-categoryQuestion-general'),
('ygin-faq-reference-categoryQuestion', 2, 'Личные', NULL, 'ygin-faq-reference-categoryQuestion-personal'),
('33', 5, 'Вывести список вложенных разделов после контента', NULL, 'ygin-menu-content-show-included-items-after-content'),
('33', 6, 'Вывести список вложенных разделов перед контентом', NULL, 'ygin-menu-content-show-included-items-before-content'),
('31', 5, 'Ссылка', NULL, 'ygin-object-reference-type-link');

-- --------------------------------------------------------

--
-- Структура таблицы `da_search_data`
--

CREATE TABLE IF NOT EXISTS `da_search_data` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(8) NOT NULL,
  `id_lang` int(8) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id_object`,`id_instance`,`id_lang`),
  KEY `id_object` (`id_object`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `da_search_data`
--

INSERT INTO `da_search_data` (`id_object`, `id_instance`, `id_lang`, `value`) VALUES
('ygin-menu', 122, 1, 'Новости'),
('ygin-menu', 100, 1, 'Главная'),
('ygin-menu', 117, 1, 'Главная'),
('ygin-menu', 124, 1, 'Фотоальбом'),
('ygin-menu', 125, 1, 'Фотоальбомы'),
('ygin-menu', 128, 1, 'Видеоальбом'),
('ygin-menu', 129, 1, 'Контакты'),
('ygin-menu', 132, 1, 'О центре История создания\r\nСовременные медицинские центры, как правило, предлагают своим пациентам все способы оздоровления и лечения. Клиники совмещают диагностику с назначением и проведением терапевтического, а далее реабилитационного курса. Это очень удобно, так как в значительной степени экономит полезное время человека, позволяя, во первых, не выпадать надолго из привычного ритма жизни, а, во вторых, не дать перейти заболеванию в хроническую форму.\r\nСовременное оборудование\r\nАппарат лазерный на парах меди  Яхрома  Мед ;\r\nАппарат ультразвуковой оториноларинологический  Тонзиллор   М ;\r\nАппарат УЗИ SA 8000 MEDISON;\r\nАппарат 3 MAX   инновационное оборудование, в котором сочетаются три новые эффективные технологии применяемые для омоложения тела и лица;\r\nСовременный аппарат прессотерапии BODY BEAUTY CLINIC 12\r\nCертификаты\r\n '),
('ygin-menu', 134, 1, 'Специалисты'),
('502', 1, 1, 'ВНИМАНИЕ! 31 ДЕКАБРЯ 31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10 00 часов. 31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10 00 часов. 31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10 00 часов. 31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10 00 часов.'),
('ygin-menu', 137, 1, 'Отзывы Отзывы'),
('502', 2, 1, 'Заголовк новости Краткое содержание Подробное содержание');

-- --------------------------------------------------------

--
-- Структура таблицы `da_search_history`
--

CREATE TABLE IF NOT EXISTS `da_search_history` (
  `id_search_history` int(8) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(255) DEFAULT NULL,
  `query` text,
  `info` varchar(255) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `ip` varchar(32) NOT NULL,
  PRIMARY KEY (`id_search_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `da_site_module`
--

CREATE TABLE IF NOT EXISTS `da_site_module` (
  `id_module` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_php_script` int(8) DEFAULT NULL COMMENT 'Обработчик',
  `name` varchar(255) NOT NULL COMMENT 'Имя',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `content` text COMMENT 'Простой текст',
  `html` longtext COMMENT 'Форматированный текст',
  PRIMARY KEY (`id_module`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Виджеты сайта' AUTO_INCREMENT=124 ;

--
-- Дамп данных таблицы `da_site_module`
--

INSERT INTO `da_site_module` (`id_module`, `id_php_script`, `name`, `is_visible`, `content`, `html`) VALUES
(108, 1016, 'Авторизация', 1, NULL, NULL),
(119, 1023, 'Левое меню', 1, NULL, NULL),
(121, 1029, 'Последние новости', 1, NULL, NULL),
(122, 1030, 'Случайное фото', 1, NULL, NULL),
(123, 1031, 'Случайное фото', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_site_module_rel`
--

CREATE TABLE IF NOT EXISTS `da_site_module_rel` (
  `id_module` int(8) NOT NULL DEFAULT '0',
  `place` int(3) NOT NULL DEFAULT '0',
  `sequence` int(3) NOT NULL DEFAULT '1',
  `id_module_template` int(8) NOT NULL,
  PRIMARY KEY (`id_module_template`,`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `da_site_module_template`
--

CREATE TABLE IF NOT EXISTS `da_site_module_template` (
  `id_module_template` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название шаблона',
  `is_default_template` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Использовать по умолчанию',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_module_template`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Наборы виджетов' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `da_site_module_template`
--

INSERT INTO `da_site_module_template` (`id_module_template`, `name`, `is_default_template`, `sequence`) VALUES
(1, 'Основной', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `da_stat_view`
--

CREATE TABLE IF NOT EXISTS `da_stat_view` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(11) NOT NULL,
  `last_date_process` int(11) NOT NULL,
  `view_type` tinyint(2) NOT NULL,
  `view_count` int(11) NOT NULL,
  PRIMARY KEY (`id_object`,`id_instance`,`view_type`,`last_date_process`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `da_system_parameter`
--

CREATE TABLE IF NOT EXISTS `da_system_parameter` (
  `id_system_parameter` varchar(255) NOT NULL COMMENT 'id',
  `id_group_system_parameter` int(8) NOT NULL DEFAULT '0' COMMENT 'Группа параметров',
  `name` varchar(60) NOT NULL COMMENT 'Имя для разработчика',
  `value` varchar(255) DEFAULT NULL COMMENT 'Значение параметра',
  `note` varchar(255) DEFAULT '-' COMMENT 'Описание',
  `id_parameter_type` int(8) DEFAULT '2' COMMENT 'Тип\r\nданных',
  `long_text_value` longtext COMMENT 'Значение для больших текстовых данных (longtext)',
  PRIMARY KEY (`id_system_parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Настройки сайта';

--
-- Дамп данных таблицы `da_system_parameter`
--

INSERT INTO `da_system_parameter` (`id_system_parameter`, `id_group_system_parameter`, `name`, `value`, `note`, `id_parameter_type`, `long_text_value`) VALUES
('101', 1, 'translit_uploaded_file_name', '1', 'Выполнять транслитерацию имен загружаемых файлов', 9, NULL),
('12', 1, 'count_day_for_delete_event', '30', 'Срок, который хранятся отосланные события (в днях)', 2, NULL),
('31', 1, 'upload_image_width', '1920', 'Автоматически уменьшать до заданного размера ширину загружаемых изображений', 1, ''),
('32', 1, 'upload_image_height', '1080', 'Автоматически уменьшать до заданного размера высоту загружаемых изображений', 1, ''),
('500', 2, 'phone', '22-13-14', 'Телефон', 2, NULL),
('6', 1, 'id_domain_default', '1', 'ИД основного домена', 1, NULL),
('7', 1, 'count_sent_mail_for_session', '3', 'Количество отсылаемых сообщений за сессию (0 - все)', 2, NULL),
('project-parameter-description-default', 2, 'description_default', NULL, 'Описание по умолчанию', 14, 'Описание по умолчанию'),
('project-parameter-hash-image-name-convert', 1, 'hash_image_name_convert', '1', 'Преобразовывать имена загружаемых (только картинок) картинок в хеш (ид юзера + микротайм + имя исходной картинки)', 9, ''),
('project-parameter-keywords-default', 2, 'keywords_default', NULL, 'Ключевые слова по умолчанию', 14, 'Ключевые слова по умолчанию'),
('project-parameter-moneta-merchant-login', 2, 'moneta_merchant_login', '123', 'Номер счета в мерчанте монеты', 2, ''),
('project-parameter-moneta-merchant-pass1', 2, 'moneta_merchant_pass1', '123456789', 'Пароль #1 в мерчанте монеты', 2, ''),
('project-parameter-robokassa-merchant-login', 2, 'robokassa_merchant_login', 'robokass-login', 'Логин в мерчанте робокассы', 2, ''),
('project-parameter-robokassa-merchant-pass1', 2, 'robokassa_merchant_pass1', 'robokass-pass1', 'Пароль #1 в мерчанте робокассы', 2, ''),
('project-parameter-robokassa-merchant-pass2', 2, 'robokassa_merchant_pass2', 'robokass-pass2', 'Пароль #2 в мерчанте робокассы', 2, ''),
('ygin-ext-main-lastChangeDate', 1, 'last_change_project_date', '20140615', 'Последняя дата проектных обновлений', 2, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `da_users`
--

CREATE TABLE IF NOT EXISTS `da_users` (
  `id_user` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Логин',
  `user_password` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'Пароль',
  `mail` varchar(255) NOT NULL COMMENT 'E-mail',
  `full_name` varchar(200) DEFAULT NULL COMMENT 'Имя пользователя',
  `rid` varchar(64) DEFAULT NULL COMMENT 'Регистрационный ИД',
  `create_date` int(10) NOT NULL DEFAULT '0' COMMENT 'Дата регистрации пользователя',
  `count_post` int(8) NOT NULL DEFAULT '0' COMMENT 'count_post',
  `active` tinyint(1) DEFAULT '1' COMMENT 'Активен',
  `requires_new_password` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Необходимо сменить пароль',
  `salt` varchar(255) DEFAULT NULL COMMENT 'Соль для пароля',
  `password_strategy` varchar(255) DEFAULT NULL COMMENT 'Стратегия для формирования пароля',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Пользователи' AUTO_INCREMENT=107 ;

--
-- Дамп данных таблицы `da_users`
--

INSERT INTO `da_users` (`id_user`, `name`, `user_password`, `mail`, `full_name`, `rid`, `create_date`, `count_post`, `active`, `requires_new_password`, `salt`, `password_strategy`) VALUES
(106, 'admin', '97e8e9cff004ca088ea9af6be4e7e7c9be1712e6', 'krvdns@mail.ru', 'Денис', NULL, 1408711608, 0, 1, 0, 'abce582623438e5204f5283d72197ac9a4cae4b2', 'sha1');

-- --------------------------------------------------------

--
-- Структура таблицы `da_visit_site`
--

CREATE TABLE IF NOT EXISTS `da_visit_site` (
  `id_instance` int(8) NOT NULL DEFAULT '0',
  `id_object` varchar(255) NOT NULL DEFAULT '0',
  `date` int(10) NOT NULL DEFAULT '0',
  `ip` int(10) unsigned NOT NULL,
  `type_visit` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_object`,`id_instance`,`type_visit`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_banner`
--

CREATE TABLE IF NOT EXISTS `pr_banner` (
  `id_banner` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `unique_name` varchar(100) NOT NULL COMMENT 'Уникальное название баннера (на английском языке)',
  `link` varchar(255) DEFAULT NULL COMMENT 'Ссылка на сайт',
  `alt` varchar(255) DEFAULT NULL COMMENT 'Текстовое описание',
  `file` int(8) NOT NULL COMMENT 'Файл',
  `id_banner_place` int(11) NOT NULL COMMENT 'Баннерное место',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `sequence` int(5) NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Баннеры' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_banner_place`
--

CREATE TABLE IF NOT EXISTS `pr_banner_place` (
  `id_banner_place` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'id_object',
  `id_instance` int(11) DEFAULT NULL COMMENT 'id_instance',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `showing` tinyint(2) NOT NULL COMMENT 'Тип показа баннеров',
  `sequence` smallint(11) DEFAULT '1' COMMENT 'Порядок',
  `id_parent` int(11) DEFAULT NULL COMMENT 'Родительский ключ',
  `width` int(8) DEFAULT NULL COMMENT 'Ширина',
  `height` int(8) DEFAULT NULL COMMENT 'Высота',
  PRIMARY KEY (`id_banner_place`),
  KEY `id_module` (`id_instance`,`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Баннерные места' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `pr_banner_place`
--

INSERT INTO `pr_banner_place` (`id_banner_place`, `id_object`, `id_instance`, `title`, `showing`, `sequence`, `id_parent`, `width`, `height`) VALUES
(3, '-1', NULL, 'Баннер слева', 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pr_client_review`
--

CREATE TABLE IF NOT EXISTS `pr_client_review` (
  `id_client_review` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'ФИО',
  `create_date` int(10) NOT NULL COMMENT 'Дата',
  `review` longtext NOT NULL COMMENT 'Текст отзыва',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  `visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость на сайте',
  `contact` varchar(255) DEFAULT NULL COMMENT 'Контакты клиента',
  PRIMARY KEY (`id_client_review`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Отзывы клиентов' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `pr_client_review`
--

INSERT INTO `pr_client_review` (`id_client_review`, `name`, `create_date`, `review`, `ip`, `visible`, `contact`) VALUES
(1, 'Денис', 1419488222, 'йукпцукп', '127.0.0.1', 1, 'кцуцук'),
(2, 'ифу', 1419488340, 'фуикф', '127.0.0.1', 1, 'укиыфу'),
(3, 'Серова Татьяна Андреевна', 1419491607, 'Хожу туда уже на повторные приёмы и могу сказать что всё всегда на высшем уровне. И главное - цены адекватные и персонал не навязывает ничего, не заставляют только у них лечиться, разговаривают с тобой вообще по-дружески, по-семейному так. Очень радует теплая атмосфера этой клиники.', '127.0.0.1', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pr_comment`
--

CREATE TABLE IF NOT EXISTS `pr_comment` (
  `id_comment` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `id_instance` int(10) NOT NULL COMMENT 'Экземпляр',
  `comment_name` varchar(255) DEFAULT NULL COMMENT 'Автор',
  `id_user` int(8) DEFAULT NULL COMMENT 'Пользователь',
  `comment_theme` varchar(255) DEFAULT NULL COMMENT 'Тема',
  `comment_date` int(10) NOT NULL COMMENT 'Дата',
  `comment_text` text NOT NULL COMMENT 'Комментарий',
  `moderation` int(8) NOT NULL COMMENT 'Отмодерировано',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP',
  `id_parent` int(11) DEFAULT NULL COMMENT 'id_parent',
  `token` varchar(255) NOT NULL COMMENT 'Токен',
  PRIMARY KEY (`id_comment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Комментарии' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_answer`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_answer` (
  `id_consultation_answer` int(8) NOT NULL COMMENT 'id',
  `id_consultation_ask` int(8) NOT NULL COMMENT 'На вопрос',
  `id_consultation_answerer` int(8) NOT NULL COMMENT 'Отвечающий',
  `answerer` varchar(255) DEFAULT NULL COMMENT 'Отвечающий (ручной ввод)',
  `answer_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата ответа',
  `answer` text NOT NULL COMMENT 'Ответ',
  `ip` varchar(255) DEFAULT NULL COMMENT 'IP отвечающего',
  PRIMARY KEY (`id_consultation_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответ';

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_answerer`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_answerer` (
  `id_consultation_answerer` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'ФИО отвечающего',
  `email` varchar(255) DEFAULT NULL COMMENT 'e-mail',
  `photo` int(8) DEFAULT NULL COMMENT 'Фото',
  `caption_before` varchar(255) DEFAULT NULL COMMENT 'Подпись перед ФИО',
  `caption_after` varchar(255) DEFAULT NULL COMMENT 'Подпись после ФИО',
  `short_info` text COMMENT 'Краткое описание',
  `full_info` text COMMENT 'Полное описание',
  PRIMARY KEY (`id_consultation_answerer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Отвечающий';

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_answerer_specialization`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_answerer_specialization` (
  `id_consultation_answerer` int(8) NOT NULL,
  `id_consultation_specialization` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_ask`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_ask` (
  `id_consultation_ask` int(8) NOT NULL COMMENT 'id',
  `user_fio` varchar(255) NOT NULL COMMENT 'ФИО спрашивающего',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail спрашивающего',
  `ask_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата вопроса',
  `ask` text NOT NULL COMMENT 'Вопрос',
  `ip` varchar(255) DEFAULT NULL COMMENT 'IP спрашивающего',
  `is_visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость',
  `attachment` int(8) DEFAULT NULL COMMENT 'Приложение',
  PRIMARY KEY (`id_consultation_ask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопрос';

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_ask_specialization`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_ask_specialization` (
  `id_consultation_ask` int(8) NOT NULL,
  `id_consultation_specialization` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_consultation_specialization`
--

CREATE TABLE IF NOT EXISTS `pr_consultation_specialization` (
  `id_consultation_specialization` int(8) NOT NULL COMMENT 'id',
  `specialization` varchar(255) NOT NULL COMMENT 'Специализация',
  `description` text COMMENT 'Описание',
  PRIMARY KEY (`id_consultation_specialization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Специализация отвечающего';

-- --------------------------------------------------------

--
-- Структура таблицы `pr_feedback`
--

CREATE TABLE IF NOT EXISTS `pr_feedback` (
  `id_feedback` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `fio` varchar(255) NOT NULL COMMENT 'ФИО',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `mail` varchar(255) DEFAULT NULL COMMENT 'e-mail',
  `message` longtext NOT NULL COMMENT 'Сообщение',
  `date` int(10) NOT NULL COMMENT 'Дата сообщения',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  PRIMARY KEY (`id_feedback`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Обратная связь' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_invoice`
--

CREATE TABLE IF NOT EXISTS `pr_invoice` (
  `id_invoice` int(8) NOT NULL AUTO_INCREMENT,
  `create_date` int(10) unsigned NOT NULL COMMENT 'Дата создания',
  `pay_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата оплаты',
  `amount` int(8) NOT NULL COMMENT 'Сумма',
  `id_offer` int(8) NOT NULL COMMENT 'Заказ',
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Счета' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_link_offer_product`
--

CREATE TABLE IF NOT EXISTS `pr_link_offer_product` (
  `id_offer` int(8) NOT NULL,
  `id_product` int(8) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id_offer`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_news`
--

CREATE TABLE IF NOT EXISTS `pr_news` (
  `id_news` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `date` int(10) unsigned NOT NULL COMMENT 'Дата',
  `id_news_category` int(8) DEFAULT NULL COMMENT 'Категория',
  `short` longtext COMMENT 'Краткое содержание',
  `content` longtext NOT NULL COMMENT 'Содержание',
  `photo` int(8) DEFAULT NULL COMMENT 'Картинка',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `page_title` varchar(255) DEFAULT NULL COMMENT 'Заголовок <title>',
  `meta_keywords` longtext COMMENT 'Ключевые слова <meta name="keywords">',
  `meta_description` longtext COMMENT 'Описание <meta name="description">',
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Новости' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `pr_news`
--

INSERT INTO `pr_news` (`id_news`, `title`, `date`, `id_news_category`, `short`, `content`, `photo`, `is_visible`, `page_title`, `meta_keywords`, `meta_description`) VALUES
(1, 'ВНИМАНИЕ! 31 ДЕКАБРЯ', 1419493446, NULL, '31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10-00 часов.', '<span class="text-main">31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10-00 часов.&nbsp;31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10-00 часов.&nbsp;31 декабря забор всех лабораторных исследований (кровь, моча, кал) производится до 10-00 часов.</span>', 35, 1, NULL, '', ''),
(2, 'Заголовк новости', 1419493680, NULL, 'Краткое содержание', 'Подробное содержание', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pr_news_category`
--

CREATE TABLE IF NOT EXISTS `pr_news_category` (
  `id_news_category` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `seq` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_news_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Категории новостей' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_offer`
--

CREATE TABLE IF NOT EXISTS `pr_offer` (
  `id_offer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `fio` varchar(255) NOT NULL COMMENT 'ФИО',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `mail` varchar(255) NOT NULL COMMENT 'e-mail',
  `comment` longtext COMMENT 'Пожелания',
  `offer_text` longtext NOT NULL COMMENT 'Заказ',
  `create_date` int(10) NOT NULL COMMENT 'Дата заказа',
  `is_process` tinyint(1) DEFAULT NULL COMMENT 'Обработано',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  `is_send` tinyint(1) DEFAULT NULL COMMENT 'Отправлено ли уведомление',
  `status` int(8) NOT NULL COMMENT 'Статус',
  `amount` int(8) NOT NULL COMMENT 'Сумма',
  `id_invoice` int(8) DEFAULT NULL COMMENT 'Счет',
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заказы пользователей' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_photogallery`
--

CREATE TABLE IF NOT EXISTS `pr_photogallery` (
  `id_photogallery` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `text_in_gallery` text COMMENT 'Текст в галерее',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родительский раздел',
  PRIMARY KEY (`id_photogallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Фотогалереи' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_photogallery_photo`
--

CREATE TABLE IF NOT EXISTS `pr_photogallery_photo` (
  `id_photogallery_photo` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `id_photogallery_object` varchar(255) NOT NULL COMMENT 'Объект',
  `id_photogallery_instance` int(8) NOT NULL COMMENT 'Экземпляр-фотогалерея',
  `file` int(8) NOT NULL COMMENT 'Файл',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_photogallery_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Фотографии' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_product`
--

CREATE TABLE IF NOT EXISTS `pr_product` (
  `id_product` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_product_category` int(8) NOT NULL COMMENT 'Каталог продукции',
  `code` varchar(255) DEFAULT NULL COMMENT 'Артикул',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `trade_price` decimal(8,2) NOT NULL COMMENT 'Оптовая цена',
  `sm_trade_price` decimal(8,2) NOT NULL COMMENT 'Мал. оптовая цена',
  `retail_price` decimal(8,2) NOT NULL COMMENT 'Розничная цена',
  `unit` varchar(255) DEFAULT NULL COMMENT 'Единица измерения',
  `quanList` varchar(255) DEFAULT NULL,
  `remain` varchar(255) DEFAULT NULL COMMENT 'Остаток',
  `description` longtext COMMENT 'Описание товара',
  `deleted` tinyint(1) NOT NULL COMMENT 'Удален',
  `image` int(8) DEFAULT NULL COMMENT 'Изображение',
  `properties` longtext COMMENT 'Характеристики',
  `additional_desc` longtext COMMENT 'Монтаж',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `id_brand` int(8) DEFAULT NULL COMMENT 'Брэнд',
  `video` longtext COMMENT 'Видео',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Продукция' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_product_brand`
--

CREATE TABLE IF NOT EXISTS `pr_product_brand` (
  `id_brand` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родительский брэнд',
  `image` int(8) DEFAULT NULL COMMENT 'Логотип брэнда',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Брэнды' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_product_category`
--

CREATE TABLE IF NOT EXISTS `pr_product_category` (
  `id_product_category` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родитель',
  `image` int(8) DEFAULT NULL COMMENT 'Изображение',
  `price_markup` int(8) NOT NULL DEFAULT '0' COMMENT 'Наценка',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_product_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Категории продукции' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_question`
--

CREATE TABLE IF NOT EXISTS `pr_question` (
  `id_question` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Спрашивающий',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `ask_date` int(10) NOT NULL COMMENT 'Дата',
  `question` longtext NOT NULL COMMENT 'Вопрос',
  `answer` text COMMENT 'Ответ',
  `visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость',
  `ip` varchar(100) NOT NULL,
  `category` int(8) NOT NULL DEFAULT '1' COMMENT 'Категория',
  `send` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отправить ответ на email',
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопрос-ответ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_quiz`
--

CREATE TABLE IF NOT EXISTS `pr_quiz` (
  `id_quiz` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'name',
  `description` text COMMENT 'description',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active',
  PRIMARY KEY (`id_quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Викторины' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_quiz_answer`
--

CREATE TABLE IF NOT EXISTS `pr_quiz_answer` (
  `id_quiz_answer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz_question` int(8) NOT NULL COMMENT 'Вопрос',
  `answer` varchar(255) NOT NULL COMMENT 'Текст ответа',
  `is_right` tinyint(1) DEFAULT NULL COMMENT 'Правильный ли ответ',
  `sequence` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_quiz_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Варианты ответов' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_quiz_answer_user`
--

CREATE TABLE IF NOT EXISTS `pr_quiz_answer_user` (
  `id_quiz_answer_user` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz` int(8) NOT NULL COMMENT 'Викторина',
  `name` varchar(255) NOT NULL COMMENT 'ФИО',
  `mail` varchar(255) NOT NULL COMMENT 'mail',
  `library_card` varchar(255) DEFAULT NULL COMMENT 'Читательский билет',
  `contact` varchar(255) DEFAULT NULL COMMENT 'Контактная информация',
  `answer` text NOT NULL COMMENT 'Ответ',
  `create_date` int(10) unsigned NOT NULL COMMENT 'Дата',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  PRIMARY KEY (`id_quiz_answer_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответ пользователя' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_quiz_question`
--

CREATE TABLE IF NOT EXISTS `pr_quiz_question` (
  `id_quiz_question` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz` int(8) NOT NULL COMMENT 'Викторина',
  `question` text NOT NULL COMMENT 'Текст вопроса',
  `type` int(8) NOT NULL DEFAULT '1' COMMENT 'Тип ответов',
  `sequence` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_quiz_question`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопросы викторины' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_remain_status`
--

CREATE TABLE IF NOT EXISTS `pr_remain_status` (
  `id_remain_status` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `min_value` int(8) NOT NULL COMMENT 'Мин. значение по-умолчанию',
  `max_value` int(8) NOT NULL COMMENT 'Макс. значение по-умолчанию',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Иконка',
  PRIMARY KEY (`id_remain_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Статусы остатка продукции' AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `pr_remain_status`
--

INSERT INTO `pr_remain_status` (`id_remain_status`, `name`, `min_value`, `max_value`, `icon`) VALUES
(1, 'под заказ', -999999999, 0, NULL),
(2, 'последняя штука', 0, 1, 'icon-red'),
(3, 'мало', 1, 5, 'icon-yellow'),
(4, 'средне', 5, 10, NULL),
(5, 'много', 10, 999999999, 'icon-green');

-- --------------------------------------------------------

--
-- Структура таблицы `pr_vitrine`
--

CREATE TABLE IF NOT EXISTS `pr_vitrine` (
  `id_vitrine` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `link` varchar(255) DEFAULT NULL COMMENT 'Ссылка на переход',
  `title` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `text` longtext COMMENT 'Дополнительный текст',
  `image` int(8) DEFAULT NULL COMMENT 'Фото',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_vitrine`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Витрина' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `pr_vitrine`
--

INSERT INTO `pr_vitrine` (`id_vitrine`, `link`, `title`, `text`, `image`, `sequence`) VALUES
(1, '/catalog/', 'Товары', '<h1>Лучшие товары в регионе!</h1>\r\n<p>Наши услуги настолько качественны, а товары дёшевы, что мы не могли не поделиться этим со всем миром и для этого создаём замечательный сайт.</p>', NULL, 1),
(2, '/news/', 'Услуги', '<h1>Лучшие услуги в регионе!</h1>\r\n<p>Наши услуги настолько качественны, а товары дёшевы, что мы не могли не поделиться этим со всем миром и для этого создаём замечательный сайт.</p>', NULL, 2),
(3, '/feedback/', 'Компания', '<h1>Лучшая компания в регионе!</h1>\r\n<p>Наши услуги настолько качественны, а товары дёшевы, что мы не могли не поделиться этим со всем миром и для этого создаём замечательный сайт.</p>', NULL, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `pr_voting`
--

CREATE TABLE IF NOT EXISTS `pr_voting` (
  `id_voting` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) NOT NULL COMMENT 'Название голосования',
  `create_date` int(10) NOT NULL DEFAULT '0' COMMENT 'Дата создания голосования',
  `is_active` int(1) NOT NULL DEFAULT '1' COMMENT 'Активное',
  `is_checkbox` int(1) NOT NULL DEFAULT '0' COMMENT 'Множество ответов',
  `in_module` int(1) NOT NULL DEFAULT '1' COMMENT 'Показать в модуле',
  PRIMARY KEY (`id_voting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Голосование' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pr_voting_answer`
--

CREATE TABLE IF NOT EXISTS `pr_voting_answer` (
  `id_voting_answer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_voting` int(8) NOT NULL DEFAULT '0' COMMENT 'Голосование',
  `name` varchar(60) NOT NULL COMMENT 'Вариант ответа',
  `count` int(8) NOT NULL DEFAULT '0' COMMENT 'Количество голосов',
  PRIMARY KEY (`id_voting_answer`,`id_voting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответы на голосование' AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `da_auth_assignment`
--
ALTER TABLE `da_auth_assignment`
  ADD CONSTRAINT `da_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `da_auth_item_child`
--
ALTER TABLE `da_auth_item_child`
  ADD CONSTRAINT `da_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `da_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
